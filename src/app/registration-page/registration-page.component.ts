import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';


@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit {

  constructor(private fb :FormBuilder) {}
	registrationForm : FormGroup;

  ngOnInit() {
  	this.formSignUp();
    window.scrollTo(0, 0);
  }		

  public formSignUp () {
  	this.registrationForm = this.fb.group ({
  		name : ['',Validators.required],
      email: ['', [Validators.required,Validators.pattern(/^\w+[\w-\.]\@\w+((-\w+)|(\w))\.[a-z]{2,3}$/)]],  		
  		phoneNumber: ['',Validators.required],
  		address1: ['',Validators.required],
  		address2: ['',Validators.required],
  		city: ['',Validators.required],
  		pinCode: ['',Validators.required]
  	})
  }
  public onSubmit() {
  	console.log(this.registrationForm.value);	
  }
}
