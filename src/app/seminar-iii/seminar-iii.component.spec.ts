import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarIIIComponent } from './seminar-iii.component';

describe('SeminarIIIComponent', () => {
  let component: SeminarIIIComponent;
  let fixture: ComponentFixture<SeminarIIIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarIIIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarIIIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
