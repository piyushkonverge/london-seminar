import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarVComponent } from './seminar-v.component';

describe('SeminarVComponent', () => {
  let component: SeminarVComponent;
  let fixture: ComponentFixture<SeminarVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
