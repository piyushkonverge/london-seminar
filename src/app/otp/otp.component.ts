import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {

  constructor(private otpFb :FormBuilder) { }
  otp : FormGroup;

  ngOnInit() {
  	this.getopt();
  }
  
  public getopt(){
  	this.otp = this.otpFb.group ({

  	})
  }
}
