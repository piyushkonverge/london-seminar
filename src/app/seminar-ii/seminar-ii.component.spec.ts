import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarIIComponent } from './seminar-ii.component';

describe('SeminarIIComponent', () => {
  let component: SeminarIIComponent;
  let fixture: ComponentFixture<SeminarIIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarIIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarIIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
