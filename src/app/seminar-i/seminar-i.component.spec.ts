import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarIComponent } from './seminar-i.component';

describe('SeminarIComponent', () => {
  let component: SeminarIComponent;
  let fixture: ComponentFixture<SeminarIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
