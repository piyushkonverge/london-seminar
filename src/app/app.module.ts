import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexPageComponent } from './index-page/index-page.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { SeminarIComponent } from './seminar-i/seminar-i.component';
import { SeminarIIComponent } from './seminar-ii/seminar-ii.component';
import { SeminarIIIComponent } from './seminar-iii/seminar-iii.component';
import { SeminarIVComponent } from './seminar-iv/seminar-iv.component';
import { SeminarVComponent } from './seminar-v/seminar-v.component';
import { SeminarVIComponent } from './seminar-vi/seminar-vi.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { OtpComponent } from './otp/otp.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexPageComponent,
    RegistrationPageComponent,
    SeminarIComponent,
    SeminarIIComponent,
    SeminarIIIComponent,
    SeminarIVComponent,
    SeminarVComponent,
    SeminarVIComponent,
    ContactUsComponent,
    MyAccountComponent,
    OtpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
