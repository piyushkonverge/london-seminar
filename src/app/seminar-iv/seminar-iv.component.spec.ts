import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarIVComponent } from './seminar-iv.component';

describe('SeminarIVComponent', () => {
  let component: SeminarIVComponent;
  let fixture: ComponentFixture<SeminarIVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarIVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarIVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
