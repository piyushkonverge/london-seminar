import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexPageComponent } from './index-page/index-page.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { SeminarIComponent } from './seminar-i/seminar-i.component';
import { SeminarIIComponent } from './seminar-ii/seminar-ii.component';
import { SeminarIIIComponent } from './seminar-iii/seminar-iii.component';
import { SeminarIVComponent } from './seminar-iv/seminar-iv.component';
import { SeminarVComponent } from './seminar-v/seminar-v.component';
import { SeminarVIComponent } from './seminar-vi/seminar-vi.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { OtpComponent } from './otp/otp.component';


const routes: Routes = [
{path:'', component:IndexPageComponent},
{path:'registration', component:RegistrationPageComponent},
{path:'seminarI', component:SeminarIComponent},
{path:'seminarII', component:SeminarIIComponent},
{path:'seminarIII', component:SeminarIIIComponent},
{path:'seminarIV', component:SeminarIVComponent},
{path:'seminarV', component:SeminarVComponent},
{path:'seminarVI', component:SeminarVIComponent},
{path:'contact_us', component:ContactUsComponent},
{path:'my_account', component:MyAccountComponent},
{path:'otp', component:OtpComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
