import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarVIComponent } from './seminar-vi.component';

describe('SeminarVIComponent', () => {
  let component: SeminarVIComponent;
  let fixture: ComponentFixture<SeminarVIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarVIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarVIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
