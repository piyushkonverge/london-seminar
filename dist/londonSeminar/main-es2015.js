(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--The content below is only a placeholder and can be replaced.-->\n\n\n<router-outlet></router-outlet>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n\t<div class=\"container\">\n\t\t<div class=\"contactus\">\n\t\t<div class=\"contactusHeader\">\n\t\t\t<h1>Contact Us</h1>\n\t\t</div>\n\t\t<div class=\"contactusContent\">\n\t\t\t<p>For more information,write to us on</p>\n\t\t\t<p class=\"emailId\">info@londonseminars.uk</p>\n\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/index-page/index-page.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/index-page/index-page.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n\t\t<!-- london seminar page -->\n\t\t<div class=\"londonSeminar\">\n\t\t\t<!-- header portion -->\n\t\t    <div class=\"header\">\n\t\t          \t<div class=\"heading \">\n\t\t\t            <h1>LONDON SEMINARS</h1>\n\t\t\t            <h4>Listen to the world’s best speakers</h4>\t\n\t\t          \t</div>\n\t\t          \t<!-- navigation start -->\n\t\t\t\t\t\t<nav class=\"navbar navbar-expand-sm justify-content-center londonNavbar\">\n\t\t\t\t\t        <ul class=\"navbar-nav\">\n\t\t\t\t\t          <li class=\"nav-item\">\n\t\t\t\t\t            <a class=\"nav-link\" href=\"#\">Home <span>|</span></a>\n\t\t\t\t\t          </li>\n\t\t\t\t\t          <li class=\"nav-item\">\n\t\t\t\t\t            <a class=\"nav-link\" routerLink=\"my_account\">My Account<span>|</span></a>\n\t\t\t\t\t          </li>\n\t\t\t\t\t          <li class=\"nav-item\">\n\t\t\t\t\t            <a routerLink=\"contact_us\" class=\"nav-link\" href=\"#\">Contact Us</a>\n\t\t\t\t\t          </li>\n\t\t\t\t\t        </ul>\n\t\t\t\t\t    </nav>\n\t\t\t\t\t<!-- navigation end -->\n\t\t    </div>\n\t\t\t<!-- header portion end --> \n\n\t\t\t<!--content main Section  -->\n\t\t\t<div class=\"contentSecton\">\n\t\t<div class=\"container\">\n\n\t\t\t\t<!-- paragraph start -->\n\t\t\t\t<div class=\"para\">\n\t\t\t        <div class=\"para_width\">\n\t\t\t            <!-- <img class=\"para_img\" src=\"assets/images/download.jpg\"> -->\n\t\t\t            <p>\n\t\t\t                Free exchange of thoughts based on scientific evidence is increasingly becoming a valuable tool for clinicians. As more & more scientific literature is published, analysing it & converting it into a meaningful information is a day to day challenge and an opportunity to improve patient care. Conferences across the globe are the most established platform of interaction & understanding of rapid developments in Cardiovascular Medicine.\n\t\t\t            </p>\n\t\t\t            <p>\n\t\t\t            \tWith the COVID-19 pandemic, a need has arisen for the exchange of  knowledge to evolve from conferences to digital platforms.\n\t\t\t\t\t\t\tLondon Seminars is a new platform which aims to bring the greatest scientific minds & best speakers directly to you. This will  enable you to make informed decisions about the issues that matter the most.\n\t\t\t\t\t\t\tEach series will be of three months. The first series will start in July 2020 & there will be two seminars every month.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t<li>July - Heart Failure.</li>\n\t\t\t\t\t\t\t<li>August - Management of Stable Ischemic Heart.</li>\n\t\t\t\t\t\t\t<li>September - Post PCI (Antiplatelet & Antithrombotic therapy)</li>\n\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t<!-- <span class=\"paraBold\">\n\t\t\t            \tJuly - Heart Failure.<br>\n\t\t\t\t\t\t\tAugust - Management of Stable Ischemic Heart.<br>  \n\t\t\t\t\t\t\tSeptember - Post PCI (Antiplatelet & Antithrombotic therapy)\n\t\t\t\t\t\t    </span> -->\n\t\t\t\t\t\t\t            \n\t\t\t            <p>\n\t\t\t            \tYou will be receiving an e-booklet on the topic covered within 2 weeks of the end of each series.  \n\t\t\t\t\t\t\tAt the end of the London Seminars  you will receive the collectors edition of the London Seminars in print format.\n\t\t\t\t\t\t</p>\t\t\t\t   \n\t\t\t            <div class=\"clearfix\"></div>\n\t\t\t        </div> \n\t\t\t\t</div>\n\t\t    \t<!-- paragraph end -->\n\t\t    \t<!-- seriesOne -->\n\t\t    \t<div class=\"seriesOne\">\n\t\t      \t<!-- <div class=\"seriesHeading\">\n\t\t      \t\t<div class=\"container\">\n\t\t      \t\t\t<img src=\"assets/images/data-21.jpg\">\n\t\t      \t    </div>\n\t\t      \t</div> -->\n\t\t      \t<!-- heatfailure -->\n\t\t      \t<div class=\"heartFailure\">\n\t\t\t      \t\t<h2 class=\"herthead\">Heart Failure</h2>\n\t\t\t      \t\t<hr>\n\t\t\t      \t\t<div class=\"row\">\n\t\t\t      \t\t\t<div class=\"col-sm-6 heartFailureimg\">\n\t\t\t      \t\t\t\t<h4>Seminar I</h4>\n\t\t\t      \t\t\t\t<img src=\"assets/images/john.png\">\n\t\t\t      \t\t\t\t<div class=\"episodePara\">\n\t\t\t\t\t\t\t\t\t<p class=\"espinfo\">Prof John McMurray on personalised Therapy of HFrEF</p>\n\t\t\t\t\t\t\t\t\t<p class=\"espTime\"> 11th July 2020 <span>|</span> 7.30-8.45pm\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<button routerLink=\"/seminarI\" class=\"btnMore\">Read More -></button>\n\t\t\t      \t\t\t\t</div>\n\t\t\t      \t\t\t</div>\n\t\t\t      \t\t\t<div class=\"col-sm-6 heartFailureimg\">\n\t\t\t      \t\t\t\t<h4>Seminar II</h4>\t      \t\t\t\t\n\t\t\t      \t\t\t\t<img src=\"assets/images/marc.png\">      \n\t\t\t      \t\t\t\t<div class=\"episodePara\">\n\t\t\t\t\t\t\t\t\t<p class=\"espinfo\">Prof Marc Pfeffer on Heart Failure With Preserved Ejection Fraction in Perspective</p>\n\t\t\t\t\t\t\t\t\t<p class=\"espTime2\"> 18th July 2020 <span>|</span> 7.30-8.45pm\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<button routerLink=\"/seminarII\" class=\"btnMore\">Read More -></button>\n\t\t\t      \t\t\t\t</div>\t\t\t\t\n\t\t\t      \t\t\t</div>      \t\t\t\n\t\t\t      \t\t</div>     \t\t\n\t\t      \t</div>\n\t\t      \t<!-- heartfailure end -->\n\n\t\t      \t<!-- Simplify PCI -->\n\t\t      \t<div class=\"heartFailure\">\n\t\t\t      \t\t<h2>Simplify PCI</h2>\n\t\t\t      \t\t<p>(with Enhanced imaging & Advanced Physiology)</p>\n\t\t\t      \t\t<hr>\n\t\t\t      \t\t<div class=\"row\">\n\t\t\t      \t\t\t<div class=\"col-sm-6 heartFailureimg\">\n\t\t\t      \t\t\t\t<h4>Seminar III</h4>\n\n\t\t\t      \t\t\t\t<img src=\"assets/images/benard.png\">\n\t\t\t      \t\t\t\t<div class=\"episodePara\">\n\t\t\t\t\t\t\t\t\t<p class=\"espinfo\">Prof Bernard Gersh</p>\n\t\t\t\t\t\t\t\t\t<p class=\"espTime3\"> 8th August 2020\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<button routerLink=\"/seminarIII\" class=\"btnMore\">Read More -> </button>\n\t\t\t      \t\t\t\t</div>\n\t\t\t      \t\t\t</div>\n\t\t\t      \t\t\t<div class=\"col-sm-6 heartFailureimg\">\n\t\t\t      \t\t\t\t<h4>Seminar IV</h4>\n\n\t\t\t      \t\t\t\t<img src=\"assets/images/william.png\">      \n\t\t\t      \t\t\t\t<div class=\"episodePara\">\n\t\t\t\t\t\t\t\t\t<p class=\"espinfo\">Prof William Fearon</p>\n\t\t\t\t\t\t\t\t\t<p class=\"espTime4\"> 22nd August 2020\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<button routerLink=\"/seminarIV\" class=\"btnMore\">Read More -></button>\n\t\t\t      \t\t\t\t</div>\t\t\t\t\n\t\t\t      \t\t\t</div>      \t\t\t\n\t\t\t      \t\t</div>     \t\t\n\t\t      \t</div>\n\t\t      \t<!-- Simplify PCI end -->\n\n\t\t      \t<!-- post PCI -->\n\t\t      \t<div class=\"heartFailure\">\n\t\t\t      \t\t<h2>Post PCI</h2>\n\t\t\t      \t\t<p>(Antiplatelet & Antithrombotic therapy)</p>\n\t\t\t      \t\t<hr>\n\t\t\t      \t\t<div class=\"row\">\n\t\t\t      \t\t\t<div class=\"col-sm-6 heartFailureimg\">\n\t\t\t      \t\t\t\t<h4>Seminar V</h4>\n\n\t\t\t      \t\t\t\t<img src=\"assets/images/phillippe.png\">\n\t\t\t      \t\t\t\t<div class=\"episodePara\">\n\t\t\t\t\t\t\t\t\t<p class=\"espinfo\">Prof Philippe Steg</p>\n\t\t\t\t\t\t\t\t\t<p class=\"espTime5\"> 12th September 2020\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<button routerLink=\"/seminarV\" class=\"btnMore\">Read More -></button>\n\t\t\t      \t\t\t\t</div>\n\t\t\t      \t\t\t</div>\n\t\t\t      \t\t\t<div class=\"col-sm-6 heartFailureimg\">\n\t\t\t      \t\t\t\t<h4>Seminar VI</h4>\n\n\t\t\t      \t\t\t\t<img src=\"assets/images/eike.png\">      \n\t\t\t      \t\t\t\t<div class=\"episodePara\">\n\t\t\t\t\t\t\t\t\t<p class=\"espinfo\">Prof John Eikelboom</p>\n\t\t\t\t\t\t\t\t\t<p class=\"espTime6\"> 19th Sepetember 2020\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<button routerLink=\"/seminarVI\" class=\"btnMore\">Read More -></button>\n\t\t\t      \t\t\t\t</div>\t\t\t\t\n\t\t\t      \t\t\t</div>      \t\t\t\n\t\t\t      \t\t</div>    \t\t\n\t\t      \t</div>\n\t\t      \t<!-- POST PCI end -->\n\t\t    \t</div>\n\t\t    \t<!-- seriesOne end -->\n\t\t    \t</div>\n\t\t    \t <!-- sponsor -->\n\t\t\t<div class=\"sponsoreHead\"> \n\t\t\t <div class=\"sponsore\">\n\t\t\t \t<h1>Our Sponsors</h1>\n\t\t\t </div>\n\t\t\t <div class=\"sponsorImage\">\n\t\t\t \t<img src=\"assets/images/zyduscadila.png\">\n\t\t\t \t<img src=\"assets/images/Lupinlogo.png\">\n\t\t\t \t<img src=\"assets/images/USV.png\">\n\t\t\t \t<img src=\"assets/images/Cipla.png\">\n\t\t\t \t<img src=\"assets/images/Glenmark.png\">\n\n\t\t\t </div>\n\t\t\t</div>\n\t\t    <!-- sponsor end -->\n\t\t\t</div>\n\n\t\t\t<!--content main Section end -->\n\n\t\t   \n\t\t<div>\n\t\t<!-- london seminar page end -->\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/my-account/my-account.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/my-account/my-account.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n\t<div class=\"container\">\n\t\t<div class=\"myaccount\">\n\t\t\t<div class=\"accountHeader\">\n\t\t\t\t<h1>My Account</h1>\n\t\t\t</div>\n\t\t\t<div class=\"accountContent\">\n\t\t\t\t<p>To watch the experts you need to register yourself. You can access the content on any device of your comfort at your convenience.</p>\n\t\t\t</div>\n      <div class=\"otpSection\">\n        <form [formGroup]=\"myAccount\" (ngSubmit)=\"onSubmit()\">\n          <div class=\"form-group\">\n            <input type=\"text\" class=\"form-control\" formControlName=\"mobileNumber\">\n          </div> \n          <p>Enter Your Mobile Number</p> \n\t\t\t<a routerLink=\"/otp\"> \n\t\t\t<button type=\"submit\" class=\"btn\">Get OTP</button>\n\t\t\t</a>\n        </form>\n      </div>\n\t\t</div>\n\t</div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/otp/otp.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/otp/otp.component.html ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n\t<div class=\"container\">\n\t\t<div class=\"myaccount\">\n\t\t\t<div class=\"accountHeader\">\n\t\t\t\t<h1>My Account</h1>\n\t\t\t</div>\n\t\t\t<div class=\"accountContent\">\n\t\t\t\t<p>To watch the experts you need to register yourself. You can access the content on any device of your comfort at your convenience.</p>\n\t\t\t</div>\n      <div class=\"otpSection\">\n        <form [formGroup]=\"otp\" (ngSubmit)=\"onSubmit()\">\n          <div class=\"form-group\">\n            <input type=\"text\" class=\"form-control\" formControlName=\"mobileNumber\">\n          </div> \n          <p>Enter OTP</p> \n          <button type=\"submit\" class=\"btn\">Submit</button>\n        </form>\n      </div>\n\t\t</div>\n\t</div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration-page/registration-page.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registration-page/registration-page.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" \n<div class=\"wrapper\">\n  <div class=\"header-register\">\n    <h3>\n      Register for an Account\n    </h3>\n    <p>\n      To watch the experts you need to register yourself. You can access the content\n      on any device of your comfort at your convenience.\n    </p>\n  </div>    \n  <div class=\"row\"> \n    <div class=\"col-md-5 col-sm-5\">\n      <div class=\"sign-in\">\n        <h4>Have an Account?</h4>\n        <button>Sign in</button>\n      </div>\n    </div>\n    <div class=\"col-md-7 col-sm-7 form-right\">\n      <form [formGroup]=\"registrationForm\" (ngSubmit)=\"onSubmit()\">\n        <div class=\"form-group\">\n          <input type=\"text\" class=\"form-control\" formControlName=\"name\" placeholder=\"Name\" required>\n          <span class=\"text-danger\" *ngIf=\"registrationForm.get('name').errors && \n                    (registrationForm.get('name').touched || registrationForm.get('name').dirty)\">\n                    Name is required\n          </span>\n        </div>\n        <div class=\"form-group\">\n          <input type=\"email\" class=\"form-control\" formControlName=\"email\" placeholder=\"Email\" required>\n          <span class=\"text-danger\" *ngIf=\"registrationForm.controls['email'].hasError('required') && \n                    registrationForm.get('email').touched\">\n                    Email is required\n          </span>\n          <span class=\"text-danger\"\n                    *ngIf=\"registrationForm.controls['email'].hasError('pattern') && (registrationForm.get('email').touched)\">\n                    Please enter valid email address\n          </span>\n        </div>\n        <div class=\"form-group\">\n          <input type=\"text\" class=\"form-control\" formControlName=\"phoneNumber\" placeholder=\"Phone no.\" required>\n           <span class=\"text-danger\" *ngIf=\"registrationForm.get('phoneNumber').errors && \n                    (registrationForm.get('phoneNumber').touched || registrationForm.get('phoneNumber').dirty)\">\n                    Phone number is required\n          </span>\n        </div>\n        <div class=\"form-group\">\n          <input type=\"text\" class=\"form-control\" formControlName=\"address1\" placeholder=\"Address 1\"required>\n           <span class=\"text-danger\" *ngIf=\"registrationForm.get('address1').errors && \n                    (registrationForm.get('address1').touched || registrationForm.get('address1').dirty)\">\n                    Address is required\n          </span>\n        </div>\n        <div class=\"form-group\">\n          <input type=\"text\" class=\"form-control\" formControlName=\"address2\" placeholder=\"Address 2\" required>\n           <span class=\"text-danger\" *ngIf=\"registrationForm.get('address2').errors && \n                    (registrationForm.get('address2').touched || registrationForm.get('address2').dirty)\">\n                    Address is required\n          </span>\n        </div>\n        <div class=\"form-group\">\n          <input type=\"text\" class=\"form-control\" formControlName=\"city\" placeholder=\"City\" required>\n           <span class=\"text-danger\" *ngIf=\"registrationForm.get('city').errors && \n                    (registrationForm.get('city').touched || registrationForm.get('city').dirty)\">\n                    City is required\n          </span>\n        </div>\n        <div class=\"form-group\">\n          <input type=\"text\" class=\"form-control\" formControlName=\"pinCode\" placeholder=\"Pin code\" required>\n          <span class=\"text-danger\" *ngIf=\"registrationForm.get('pinCode').errors && \n                    (registrationForm.get('pinCode').touched || registrationForm.get('pinCode').dirty)\">\n                    Pincode is required\n          </span>\n        </div>       \n        <button type=\"submit\" class=\"btn\">Submit</button>\n      </form>\n    </div>\n  </div>  \n</div> \n<!-- footer -->\n<footer class=\"londonSemi_footer\">\n  <div class=\"setFooter\">\n    <span>Follow us on :</span>\n    <img src=\"assets/images/facebook.png\">\n    <img src=\"assets/images/youtube.png\">\n    <img src=\"assets/images/twitter.png\">\n  </div>\n</footer>\n<!-- footer end -->\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-i/seminar-i.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-i/seminar-i.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n  <div class=\"seminarBackgrund\">\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-sm-6 col-md-6 col-xs-12 prof-div\">\n            <img src=\"assets/images/seminar1.png\">\n          </div>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 intro\">\n            <h2>Seminar I</h2>\n            <h1>John McMurray</h1>\n            <button>Heart Failure</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"senimarSpacing\">\n    <div class=\"container\">  \n      <div class=\"row middle-wraper\">\n          <div class=\"col-sm-9 col-sm-9 col-sm-9 col-xs-12 content\">\n            <p>\n                John McMurray, Professor of Medical Cardiology and Deputy Director of the Institute of Cardiovascular and Medical Sciences at the University of Glasgow, UK and honorary Consultant Cardiologist at the Queen Elizabeth University Hospital, Glasgow. He was Past-President of the Heart Failure Association of the European Society of Cardiology (ESC). \n            </p>\n            <p>\n                Professor McMurray’s primary research interest is in heart failure and his main research activity is clinical trials. He is, or was, the principal investigator of two tries which changed the dimensions of the treatment DAPA HP & CAROLINA.\n            </p>\n            <p>\n                He has published over 600 original papers, reviews, and book chapters and is the primary author or editor of 13 books. Professor McMurray was included in the 2015 listing of Highly Cited Researchers by Thomson-Reuters.\n            </p>\n            <p>\n                Professor McMurray was the lead author of the World Health Organization and first Scottish Intercollegiate Guidelines Network Guidelines on the Management of heart failure.\n            </p>\n          </div>\n          <div class=\"col-sm-3 col-sm-3 col-sm-3 col-xs-12 watch-area\">\n            <a routerLink=\"/my_account\"> \n              <button>\n                Watch\n              </button>\n            </a>\n\n            <div class=\"next-seminar\">\n              <p>Next</p>\n              <img src=\"assets/images/pic5.png\" alt=\"\">\n              <h3>\n                  Prof Marc Pfeffer on<br> Heart Failure With <br>Preserved Ejection <br>Fraction in Perspective\n              </h3>\n              <h4>18th July 2020</h4>\n              <hr>\n              <h4>7.30-8.45pm</h4>\n            </div>\n          </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"seminarBackgrund\">  \n    <div class=\"senimarSpacing\">    \n      <div class=\"container\">  \n        <div class=\"row chairperson-main\">\n          <h2 class=\"title\">\n              Chairperson\n          </h2>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n            <img src=\"assets/images/pic1.png\">\n            <h2>Jeroen Bax</h2>\n            <p>Director of non-invasive imaging and Director of the echo-lab at the Leiden University Medical Center</p>\n          </div>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n              <img src=\"assets/images/pic2.png\">\n              <h2>Martin Cowie</h2>\n              <p>Professor of Cardiology (Health Services Research) and is an Honorary Consultant Cardiologist at the Royal Brompton and Harefield NHS Foundation Trust</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"footer\">  \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-ii/seminar-ii.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-ii/seminar-ii.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n    <div class=\"seminarBackgrund\">\n      <div class=\"senimarSpacing\">\n        <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-sm-6 col-md-6 col-xs-12 prof-div\">\n              <img src=\"assets/images/seminar2.png\">\n            </div>\n            <div class=\"col-sm-6 col-md-6 col-xs-12 intro\">\n              <h2>Seminar II</h2>\n              <h1>Marc Preffer</h1>\n              <button>Heart Failure</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div> \n    <div class=\"senimarSpacing\">\n      <div class=\"container\">   \n        <div class=\"row middle-wraper\">\n          <div class=\"col-sm-9 col-sm-9 col-sm-9 col-xs-12 content\">\n            <p>\n                Dr. Marc Alan Pfeffer is a cardiovascular medicine specialist at Brigham and Women’s Hospital (BWH) and the Victor J. Dzau professor of medicine at Harvard Medical School.  \n            </p>\n            <p>\n                His clinical interests include cardiovascular disease, congestive heart failure, coronary artery disease and valvular heart disease. The author of over 450 peer-reviewed publications.\n            </p>\n            <p>\n                 Dr. Pfeffer has been listed as one of America’s Top Doctors by Castle Connolly and named a top cardiologist by Boston Magazine.\n            </p>\n            <p>\n               Dr. Pfeffer is the recipient of the Clinical Research Prize and the James Herrick Award, both from the American Heart Association.\n            </p>\n            <p>\n               Dr. Pfeffer - along with Dr. Janice Pfeffer and Dr. Eugene Braunwald - is credited with introducing the concept that angiotensin-converting enzyme inhibitors (ACEIs) could mitigate adverse ventricular remodeling following myocardial infarction and that ACEI use would result in increased survival and other clinical benefits. \n            </p>\n          </div>\n          <div class=\"col-sm-3 col-sm-3 col-sm-3 col-xs-12 watch-area\">\n            <a routerLink=\"/my_account\"> \n              <button>\n                Watch\n              </button>\n            </a>\n\n            <div class=\"next-seminar\">\n              <p>Next</p>\n              <img src=\"assets/images/pic6.png\" alt=\"\">\n              <h3>\n                  Prof Bernard Gersh\n              </h3>\n\n              <h4>8th August 2020</h4>\n              <!-- <hr> -->\n              <!-- <h4>7.30-8.45pm</h4> -->\n\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"seminarBackgrund\">\n      <div class=\"senimarSpacing\">   \n        <div class=\"container\">\n          <div class=\"row chairperson-main\">\n            <h2 class=\"title\">\n                Chairperson\n            </h2>\n            <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n              <img src=\"assets/images/pic1.png\">\n              <h2>Jeroen Bax</h2>\n              <p>Director of non-invasive imaging and Director of the echo-lab at the Leiden University Medical Center</p>\n            </div>\n\n            <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n                <img src=\"assets/images/pic2.png\">\n                <h2>Martin Cowie</h2>\n                <p>Professor of Cardiology (Health Services Research) and is an Honorary Consultant Cardiologist at the Royal Brompton and Harefield NHS Foundation Trust</p>\n            </div>\n          </div>\n        </div> \n      </div>\n    </div>\n</div>\n<div class=\"footer\">\n  \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-iii/seminar-iii.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-iii/seminar-iii.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n  <div class=\"seminarBackgrund\">\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-sm-6 col-md-6 col-xs-12 prof-div\">\n            <img src=\"assets/images/seminar3.png\">\n          </div>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 intro\">\n            <h2>Seminar III</h2>\n            <h1>Bernard Gersh</h1>\n            <button>Simplify PCI</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"senimarSpacing\">\n    <div class=\"container\">\n      <div class=\"row middle-wraper\">\n        <div class=\"col-sm-9 col-sm-9 col-sm-9 col-xs-12 content\">\n\n          <p>\n              Professor of Medicine at Mayo Clinic College of Medicine, Consultant in Cardiovascular Diseases and Internal Medicine. \n          </p>\n          <p>\n              His past positions include The W. Proctor Harvey Teaching Professor of Cardiology and Chief of the Division of Cardiology at Georgetown University Medical Center. Dr. Gersh received his MB, ChB, from the University of Cape Town in South Africa.\n          </p>\n          <p>\n               Dr. Gersh’s wide interests include the natural history and therapy of acute and chronic coronary artery disease, clinical electrophysiology and in particular atrial fibrillation, sudden cardiac death and syncope, cardiac stem cell therapy, and the epidemiology of cardiovascular disease in the developing world. \n          </p>\n          <p>\n             He has approximately 1105 publications (959 manuscripts and 146 book chapters). (h-index 119).\n          </p>\n        </div>\n        <div class=\"col-sm-3 col-sm-3 col-sm-3 col-xs-12 watch-area\">\n          <a routerLink=\"/my_account\"> \n            <button>\n              Watch\n            </button>\n          </a>\n\n          <div class=\"next-seminar\">\n            <p>Next</p>\n            <img src=\"assets/images/pic7.png\" alt=\"\">\n            <h3>\n                Prof William Fearon\n            </h3>\n\n            <h4>22nd August 2020</h4>\n            <!-- <hr> -->\n            <!-- <h4>7.30-8.45pm</h4> -->\n\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"seminarBackgrund\">\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row chairperson-main\">\n          <h2 class=\"title\">\n              Chairperson\n          </h2>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n            <img src=\"assets/images/pic1.png\">\n            <h2>Jeroen Bax</h2>\n            <p>Director of non-invasive imaging and Director of the echo-lab at the Leiden University Medical Center</p>\n          </div>\n\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n              <img src=\"assets/images/pic3.png\">\n              <h2>Anthony Gershlick</h2>\n              <p>Consultant Cardiologist at Leicester’s Hospitals, UK</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>  \n</div>\n<div class=\"footer\">  \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-iv/seminar-iv.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-iv/seminar-iv.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n  <div class=\"seminarBackgrund\">\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-sm-6 col-md-6 col-xs-12 prof-div\">\n            <img src=\"assets/images/seminar4.png\">\n          </div>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 intro\">\n            <h2>Seminar IV</h2>\n            <h1>William Fearon</h1>\n            <button>Simplify PCI</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"senimarSpacing\">\n    <div class=\"container\">\n      <div class=\"row middle-wraper\">\n        <div class=\"col-sm-9 col-sm-9 col-sm-9 col-xs-12 content\">\n\n          <p>\n              Dr. Fearon is Professor of Medicine (Cardiology) and Director of Interventional Cardiology at Stanford University.\n          </p>\n          <p>\n              Dr. Fearon’s primary area of research interest is in coronary physiology. He was the US principal investigator and senior author of the FAME trial, published in the New England Journal of Medicine and is a co-principal investigator and senior author of the FAME 2 trial, also published in the New England Journal of Medicine. He is the principal investigator of the FAME 3 trial comparing FFR-guided stenting with bypass surgery.\n          </p>\n          <p>\n              He has presented and published a number of abstracts and peer-reviewed papers in both coronary physiology and transcatheter aortic valve replacement, and serves on the editorial board of Circulation, the Journal of the American College of Cardiology, JACC Cardiovascular Interventions, and Circulation: Cardiovascular Interventions.\n          </p>\n          \n        </div>\n        <div class=\"col-sm-3 col-sm-3 col-sm-3 col-xs-12 watch-area\">\n          <a routerLink=\"/my_account\"> \n            <button>\n              Watch\n            </button>\n          </a>\n\n          <div class=\"next-seminar\">\n            <p>Next</p>\n            <img src=\"assets/images/pic8.png\" alt=\"\">\n            <h3>\n                Prof Philippe Steg\n            </h3>\n\n            <h4>12th September 2020</h4>\n            <!-- <hr> -->\n            <!-- <h4>7.30-8.45pm</h4> -->\n\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"seminarBackgrund\">\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row chairperson-main\">\n          <h2 class=\"title\">\n              Chairperson\n          </h2>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n            <img src=\"assets/images/pic1.png\">\n            <h2>Jeroen Bax</h2>\n            <p>Director of non-invasive imaging and Director of the echo-lab at the Leiden University Medical Center</p>\n          </div>\n\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n              <img src=\"assets/images/pic3.png\">\n              <h2>Anthony Gershlick</h2>\n              <p>Consultant Cardiologist at Leicester’s Hospitals, UK</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"footer\">\n  \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-v/seminar-v.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-v/seminar-v.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n  <div class=\"seminarBackgrund\">\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-sm-6 col-md-6 col-xs-12 prof-div\">\n            <img src=\"assets/images/seminar5.png\">\n          </div>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 intro\">\n            <h2>Seminar V</h2>\n            <h1>Philippe Steg</h1>\n            <button>Post PCI</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div> \n  <div class=\"senimarSpacing\">\n    <div class=\"container\">\n      <div class=\"row middle-wraper\">\n        <div class=\"col-sm-9 col-sm-9 col-sm-9 col-xs-12 content\">\n\n          <p>\n               Dr Steg has been a Professor of Cardiology at Université Paris - Diderot since 1994. He is also Professor at the National Heart and Lung Institute, Imperial College, London, UK. \n          </p>\n          <p>\n              Dr Steg’s research interests are in the field of coronary artery disease. He is a former member of the Board of the French Society of Cardiology, an honorary member of the Institut Universitaire de France, and received the silver medal of the European Society of Cardiology in 2011.\n          </p>\n          <p>\n              Dr Steg led the TAO and EUROMAX trials and is currently chairing or co-chairing several trials or registries, such as ODYSSEY-CV Outcomes, CLARIFY, THEMIS.\n          </p>\n          <p>\n               Dr Steg has authored more than 650 articles in peer-reviewed international journals. He is a member of the editorial boards for Circulation, Circulation: Cardiovascular Interventions, the European Heart Journal, and the European Heart Journal – Cardiovascular Pharmacotherapy.\n          </p>\n        </div>\n        <div class=\"col-sm-3 col-sm-3 col-sm-3 col-xs-12 watch-area\">\n          <a routerLink=\"/my_account\"> \n            <button>\n              Watch\n            </button>\n          </a>\n\n          <div class=\"next-seminar\">\n            <p>Next</p>\n            <img src=\"assets/images/pic9.png\" alt=\"\">\n            <h3>\n                Prof John Eikelboom\n            </h3>\n\n            <h4>19th Sepetember 2020</h4>\n            <!-- <hr> -->\n            <!-- <h4>7.30-8.45pm</h4> -->\n\n          </div>\n        </div>\n      </div>\n    </div>\n  </div> \n  <div class=\"seminarBackgrund\">\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row chairperson-main\">\n          <h2 class=\"title\">\n              Chairperson\n          </h2>\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n            <img src=\"assets/images/pic1.png\">\n            <h2>Jeroen Bax</h2>\n            <p>Director of non-invasive imaging and Director of the echo-lab at the Leiden University Medical Center</p>\n          </div>\n\n          <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n              <img src=\"assets/images/pic4.png\">\n              <h2>Keith Fox</h2>\n              <p>Professor of Cardiology of the University of Edinburgh, UK</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>  \n</div>\n\n<div class=\"footer\">\n  \n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-vi/seminar-vi.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-vi/seminar-vi.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"wraper\">\n    <div class=\"seminarBackgrund\">\n      <div class=\"senimarSpacing\">\n        <div class=\"container\">\n          <div class=\"row\">\n            <div class=\"col-sm-6 col-md-6 col-xs-12 prof-div\">\n              <img src=\"assets/images/john-eikelboom.png\">\n            </div>\n            <div class=\"col-sm-6 col-md-6 col-xs-12 intro\">\n              <h2>Seminar VI</h2>\n              <h1>John Eikelboom</h1>\n              <button>Post PCI</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"senimarSpacing\">\n      <div class=\"container\">\n        <div class=\"row middle-wraper\">\n          <div class=\"col-sm-9 col-sm-9 col-sm-9 col-xs-12 content\">\n\n            <p>\n                Dr. Eikelboom, Associate Professor in the Department of Medicine, McMaster University, and haematologist in the Thrombosis Service, Hamilton General Hospital, Ontario, Canada. \n            </p>\n            <p>\n                He originally trained in Internal Medicine and Haematology in Perth, Australia and subsequently moved to Hamilton to take up a Tier II Canada Research Chair in Cardiovascular Medicine.\n            </p>\n            <p>\n               Dr Eikelboom has co-authored more than 400 articles in peer-reviewed journals. His current research, supported by the Canadian Institutes for Health Research, the Heart and Stroke Foundation, and the National Health and Medical Research Council of Australia, focuses on the efficacy and safety of antithrombotic therapies, outcomes after blood transfusion and bleeding, and the mechanisms of variable response to antiplatelet drugs.\n            </p>\n            \n          </div>\n          <div class=\"col-sm-3 col-sm-3 col-sm-3 col-xs-12 watch-area\">\n            <a routerLink=\"/my_account\"> \n              <button>\n                Watch\n              </button>\n            </a>\n\n            <div class=\"next-seminar\">\n              <img src=\"assets/images/watch-next.png\" alt=\"\">\n       \n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"seminarBackgrund\">\n      <div class=\"senimarSpacing\">\n        <div class=\"container\">\n          <div class=\"row chairperson-main\">\n            <h2 class=\"title\">\n                Chairperson\n            </h2>\n            <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n              <img src=\"assets/images/pic1.png\">\n              <h2>Jeroen Bax</h2>\n              <p>Director of non-invasive imaging and Director of the echo-lab at the Leiden University Medical Center</p>\n            </div>\n\n            <div class=\"col-sm-6 col-md-6 col-xs-12 chairperson-info\">\n                <img src=\"assets/images/pic4.png\">\n                <h2>Keith Fox</h2>\n                <p>Professor of Cardiology of the University of Edinburgh, UK</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n</div>\n\n<div class=\"footer\">\n  \n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __createBinding, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__createBinding", function() { return __createBinding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __createBinding(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}

function __exportStar(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _index_page_index_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index-page/index-page.component */ "./src/app/index-page/index-page.component.ts");
/* harmony import */ var _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./registration-page/registration-page.component */ "./src/app/registration-page/registration-page.component.ts");
/* harmony import */ var _seminar_i_seminar_i_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./seminar-i/seminar-i.component */ "./src/app/seminar-i/seminar-i.component.ts");
/* harmony import */ var _seminar_ii_seminar_ii_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./seminar-ii/seminar-ii.component */ "./src/app/seminar-ii/seminar-ii.component.ts");
/* harmony import */ var _seminar_iii_seminar_iii_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./seminar-iii/seminar-iii.component */ "./src/app/seminar-iii/seminar-iii.component.ts");
/* harmony import */ var _seminar_iv_seminar_iv_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./seminar-iv/seminar-iv.component */ "./src/app/seminar-iv/seminar-iv.component.ts");
/* harmony import */ var _seminar_v_seminar_v_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./seminar-v/seminar-v.component */ "./src/app/seminar-v/seminar-v.component.ts");
/* harmony import */ var _seminar_vi_seminar_vi_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./seminar-vi/seminar-vi.component */ "./src/app/seminar-vi/seminar-vi.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _my_account_my_account_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./my-account/my-account.component */ "./src/app/my-account/my-account.component.ts");
/* harmony import */ var _otp_otp_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./otp/otp.component */ "./src/app/otp/otp.component.ts");














const routes = [
    { path: '', component: _index_page_index_page_component__WEBPACK_IMPORTED_MODULE_3__["IndexPageComponent"] },
    { path: 'registration', component: _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_4__["RegistrationPageComponent"] },
    { path: 'seminarI', component: _seminar_i_seminar_i_component__WEBPACK_IMPORTED_MODULE_5__["SeminarIComponent"] },
    { path: 'seminarII', component: _seminar_ii_seminar_ii_component__WEBPACK_IMPORTED_MODULE_6__["SeminarIIComponent"] },
    { path: 'seminarIII', component: _seminar_iii_seminar_iii_component__WEBPACK_IMPORTED_MODULE_7__["SeminarIIIComponent"] },
    { path: 'seminarIV', component: _seminar_iv_seminar_iv_component__WEBPACK_IMPORTED_MODULE_8__["SeminarIVComponent"] },
    { path: 'seminarV', component: _seminar_v_seminar_v_component__WEBPACK_IMPORTED_MODULE_9__["SeminarVComponent"] },
    { path: 'seminarVI', component: _seminar_vi_seminar_vi_component__WEBPACK_IMPORTED_MODULE_10__["SeminarVIComponent"] },
    { path: 'contact_us', component: _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_11__["ContactUsComponent"] },
    { path: 'my_account', component: _my_account_my_account_component__WEBPACK_IMPORTED_MODULE_12__["MyAccountComponent"] },
    { path: 'otp', component: _otp_otp_component__WEBPACK_IMPORTED_MODULE_13__["OtpComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'londonSeminar';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _index_page_index_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./index-page/index-page.component */ "./src/app/index-page/index-page.component.ts");
/* harmony import */ var _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./registration-page/registration-page.component */ "./src/app/registration-page/registration-page.component.ts");
/* harmony import */ var _seminar_i_seminar_i_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./seminar-i/seminar-i.component */ "./src/app/seminar-i/seminar-i.component.ts");
/* harmony import */ var _seminar_ii_seminar_ii_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./seminar-ii/seminar-ii.component */ "./src/app/seminar-ii/seminar-ii.component.ts");
/* harmony import */ var _seminar_iii_seminar_iii_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./seminar-iii/seminar-iii.component */ "./src/app/seminar-iii/seminar-iii.component.ts");
/* harmony import */ var _seminar_iv_seminar_iv_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./seminar-iv/seminar-iv.component */ "./src/app/seminar-iv/seminar-iv.component.ts");
/* harmony import */ var _seminar_v_seminar_v_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./seminar-v/seminar-v.component */ "./src/app/seminar-v/seminar-v.component.ts");
/* harmony import */ var _seminar_vi_seminar_vi_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./seminar-vi/seminar-vi.component */ "./src/app/seminar-vi/seminar-vi.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/contact-us/contact-us.component.ts");
/* harmony import */ var _my_account_my_account_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./my-account/my-account.component */ "./src/app/my-account/my-account.component.ts");
/* harmony import */ var _otp_otp_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./otp/otp.component */ "./src/app/otp/otp.component.ts");

















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _index_page_index_page_component__WEBPACK_IMPORTED_MODULE_6__["IndexPageComponent"],
            _registration_page_registration_page_component__WEBPACK_IMPORTED_MODULE_7__["RegistrationPageComponent"],
            _seminar_i_seminar_i_component__WEBPACK_IMPORTED_MODULE_8__["SeminarIComponent"],
            _seminar_ii_seminar_ii_component__WEBPACK_IMPORTED_MODULE_9__["SeminarIIComponent"],
            _seminar_iii_seminar_iii_component__WEBPACK_IMPORTED_MODULE_10__["SeminarIIIComponent"],
            _seminar_iv_seminar_iv_component__WEBPACK_IMPORTED_MODULE_11__["SeminarIVComponent"],
            _seminar_v_seminar_v_component__WEBPACK_IMPORTED_MODULE_12__["SeminarVComponent"],
            _seminar_vi_seminar_vi_component__WEBPACK_IMPORTED_MODULE_13__["SeminarVIComponent"],
            _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_14__["ContactUsComponent"],
            _my_account_my_account_component__WEBPACK_IMPORTED_MODULE_15__["MyAccountComponent"],
            _otp_otp_component__WEBPACK_IMPORTED_MODULE_16__["OtpComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/contact-us/contact-us.component.css":
/*!*****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif;\n    padding: 40px 70px 30px 70px;\n    background-image: url(/assets/images/contentSection.png);\n    background-size: contain;\n    min-height: 100vh;\n\n}\n.emailId{font-weight: bold !important;}\n.contactus{ width: 700px;\n    margin: auto;}\n.contactusHeader {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    text-align: center;\n    margin: auto;}\n.contactusHeader h1 {text-align: center;\n    letter-spacing: 1.27px;\n    color: #fff;\n    opacity: 1;\n    font-size: 42px;\n    padding: 50px 0;\n    font-weight: 600;\t}\n.contactusContent{padding: 20px 40px;}\n.contactusContent p {font-size: 20px; letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500; text-align: center;}\n@media screen and (max-width: 500px) {\n .wraper {\npadding: 10px 10px 10px 10px;\n}\n.contactus{\nwidth: 100%;\nmargin: auto;\n}\n.contactusHeader h1 {\nfont-size: 22px;\npadding: 10px 0;}\n.contactusContent{\npadding: 10px;\n}\n.contactusContent p {\nfont-size: 20px;\nletter-spacing: 1.25px;\ncolor: #040404;\nopacity: 1;\nline-height: 22px;}\n/*.otpSection {\nborder: 1px solid #707070;\nbackground: #EFEFEF;\npadding: 20px;\nwidth: 100%;}\n.otpSection button{font-size: 18px;margin: 0;width: 100%;} */\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxpQ0FBaUM7SUFDdEMsNEJBQTRCO0lBQzVCLHdEQUF3RDtJQUN4RCx3QkFBd0I7SUFDeEIsaUJBQWlCOztBQUVyQjtBQUNBLFNBQVMsNEJBQTRCLENBQUM7QUFDdEMsWUFBWSxZQUFZO0lBQ3BCLFlBQVksQ0FBQztBQUNqQixrQkFBa0Isd0pBQXdKO0lBQ3RLLGtCQUFrQjtJQUNsQixZQUFZLENBQUM7QUFDakIscUJBQXFCLGtCQUFrQjtJQUNuQyxzQkFBc0I7SUFDdEIsV0FBVztJQUNYLFVBQVU7SUFDVixlQUFlO0lBQ2YsZUFBZTtJQUNmLGdCQUFnQixFQUFFO0FBRXRCLGtCQUFrQixrQkFBa0IsQ0FBQztBQUNyQyxxQkFBcUIsZUFBZSxFQUFFLHNCQUFzQjtJQUN4RCxjQUFjO0lBQ2QsVUFBVSxDQUFDLGlCQUFpQjtJQUM1QixnQkFBZ0IsRUFBRSxrQkFBa0IsQ0FBQztBQUV6QztDQUNDO0FBQ0QsNEJBQTRCO0FBQzVCO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsZUFBZSxDQUFDO0FBQ2hCO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxlQUFlO0FBQ2Ysc0JBQXNCO0FBQ3RCLGNBQWM7QUFDZCxVQUFVO0FBQ1YsaUJBQWlCLENBQUM7QUFDbEI7Ozs7OzREQUs0RDtBQUM1RCIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3QtdXMvY29udGFjdC11cy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndyYXBlciB7Zm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xuICAgIHBhZGRpbmc6IDQwcHggNzBweCAzMHB4IDcwcHg7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvaW1hZ2VzL2NvbnRlbnRTZWN0aW9uLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xuXG59XG4uZW1haWxJZHtmb250LXdlaWdodDogYm9sZCAhaW1wb3J0YW50O31cbi5jb250YWN0dXN7IHdpZHRoOiA3MDBweDtcbiAgICBtYXJnaW46IGF1dG87fVxuLmNvbnRhY3R1c0hlYWRlciB7YmFja2dyb3VuZDogdHJhbnNwYXJlbnQgbGluZWFyLWdyYWRpZW50KDkwZGVnLCAjRkUzQjYxIDAlLCAjRkU0NjREIDE5JSwgI0ZGM0I1RSA0MCUsICNGRjRDMzYgNjAlLCAjRkY1RDFDIDgxJSwgI0ZFNkQwQyAxMDAlKSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogYXV0bzt9XG4uY29udGFjdHVzSGVhZGVyIGgxIHt0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDEuMjdweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtc2l6ZTogNDJweDtcbiAgICBwYWRkaW5nOiA1MHB4IDA7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcdH1cbiAgICBcbi5jb250YWN0dXNDb250ZW50e3BhZGRpbmc6IDIwcHggNDBweDt9XG4uY29udGFjdHVzQ29udGVudCBwIHtmb250LXNpemU6IDIwcHg7IGxldHRlci1zcGFjaW5nOiAxLjI1cHg7XG4gICAgY29sb3I6ICMwNDA0MDQ7XG4gICAgb3BhY2l0eTogMTtsaW5lLWhlaWdodDogMjhweDtcbiAgICBmb250LXdlaWdodDogNTAwOyB0ZXh0LWFsaWduOiBjZW50ZXI7fVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xuIC53cmFwZXIge1xucGFkZGluZzogMTBweCAxMHB4IDEwcHggMTBweDtcbn1cbi5jb250YWN0dXN7XG53aWR0aDogMTAwJTtcbm1hcmdpbjogYXV0bztcbn1cbi5jb250YWN0dXNIZWFkZXIgaDEge1xuZm9udC1zaXplOiAyMnB4O1xucGFkZGluZzogMTBweCAwO31cbi5jb250YWN0dXNDb250ZW50e1xucGFkZGluZzogMTBweDtcbn1cbi5jb250YWN0dXNDb250ZW50IHAge1xuZm9udC1zaXplOiAyMHB4O1xubGV0dGVyLXNwYWNpbmc6IDEuMjVweDtcbmNvbG9yOiAjMDQwNDA0O1xub3BhY2l0eTogMTtcbmxpbmUtaGVpZ2h0OiAyMnB4O31cbi8qLm90cFNlY3Rpb24ge1xuYm9yZGVyOiAxcHggc29saWQgIzcwNzA3MDtcbmJhY2tncm91bmQ6ICNFRkVGRUY7XG5wYWRkaW5nOiAyMHB4O1xud2lkdGg6IDEwMCU7fVxuLm90cFNlY3Rpb24gYnV0dG9ue2ZvbnQtc2l6ZTogMThweDttYXJnaW46IDA7d2lkdGg6IDEwMCU7fSAqL1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/contact-us/contact-us.component.ts":
/*!****************************************************!*\
  !*** ./src/app/contact-us/contact-us.component.ts ***!
  \****************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ContactUsComponent = class ContactUsComponent {
    constructor() { }
    ngOnInit() {
    }
};
ContactUsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contact-us',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contact-us.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contact-us.component.css */ "./src/app/contact-us/contact-us.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ContactUsComponent);



/***/ }),

/***/ "./src/app/index-page/index-page.component.css":
/*!*****************************************************!*\
  !*** ./src/app/index-page/index-page.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {\n    font-family: 'Roboto', sans-serif;\n}\n/*header*/\n.header {\nbackground: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\ntext-align: center;\ncolor: #fff;\n}\n.header .heading {\n    padding: 130px 0;\n}\n.header .heading h1 {    \n    font-size: 150px;\n    font-weight: bolder;\n    margin: 0;\n    font-family: auto;\n    line-height: 140px;\n}\n.header .heading h4 {    \n\tfont-size: 50px;\n    margin: 0;\n    color: #000;\n}\n/*header end*/\n/*navigation*/\n.londonNavbar {    \n    font-size: 18px;\n    margin-top: 10px;\n    padding:0;\n}\n.londonNavbar ul li a {\n    color: #000;\n    font-size: 28px;\n    font-weight: 500;\n}\n.londonNavbar ul li a span {\n    padding: 0 15px;\n     color: #f0dcdc;\n}\n/*navigation end*/\n.contentSecton {\n    background-image:url(/assets/images/contentSection.png);\n    background-size: contain;\n    padding: 30px 0;\n}\n/*paragraph*/\n.para img {\n\tfloat: left;\n    border: 2px solid red;\n    margin: 5px 35px 5px;\n}\n.para p {    \n    text-align: justify;\n    font-size: 18px;\n    letter-spacing: 1.5px;\n    color: #070707;\n}\n.para ul {   \n    font-size: 18px;\n    letter-spacing: 1.5px;\n    color: #070707;\n}\n.paraBold {\n    font-weight: bold;\n}\n.para_width {\n    max-width: 710px;\n     margin: auto;\n }\n/*paragraph end*/\n/*seriesOne*/\n.seriesOne .seriesHeading img{\n    width: 100%;\n}\n.heartFailure {\n    margin: 50px 0;\n}\n.heartFailure h2 {    \n    text-align: center;\n    font-size: 36px;\n    margin-bottom: 0px;\n    font-weight: bold;\n    letter-spacing: 3px;\n\n}\n.heartFailure p {    \n    text-align: center;\n    margin: 0;\n    font-size: 28px;\n}\n.heartFailure hr {\n    width: 280px;\n    border-color: #000;\n    border-width: 2px;\n    margin: auto;\n }\n/*heart Failure*/\n.heartFailureimg {\n    margin: 50px 0;\n}\n.heartFailureimg h4 {\n    color: #393737;\n    font-size: 20px;\n    letter-spacing: 2px;\n    text-align: center;\n}\n.heartFailureimg img {\n    width: 100%;\n}\n.heartFailureimg .episodePara{\n    background: #FFF;\n    border: 1px solid #707070; \n    padding: 15px 45px;\n    border-top: none;\n}\n.heartFailureimg .episodePara p{    \n    text-align: center;\n    letter-spacing: 1.27px;\n    color: #0A0A0A;\n    opacity: 1;\n    font-size: 18px;\n    padding-top: 5px;\n    text-align: center;\n}\n.espTime{\n    color:#D2AB2A !important;\n    font-weight: 600;\n    font-size: 20px;\n    letter-spacing: 1.25px;\n}\n.espinfo {    \n    color: #000;\n    font-weight: 600;\n    font-size: 20px;\n}\n.espsub {    \n    font-size: 14px;\n}\n.espprof {\n    color: #000;\n    font-weight: 600;\n    font-size: 16px;\n}\n.btnMore{    \n    border: none;\n    background: transparent;\n    font-size: 16px;\n    color: #393737;\n    font-weight: 600;\n    letter-spacing: 1.25px;\n    margin: 30px 160px 0;\n}\n.btnReg {border: none;\n    background: #96a4a5;\n    padding: 5px 10px;\n    width: 25%;\n    margin: 20px 50px;\n    font-size: 16px;}\n.espTime2{color: #8D44AD !important;\n     font-weight: 600;\n    font-size: 20px;\n    letter-spacing: 1.25px;}\n.espTime3 {color: #1CBB9B !important;\n font-weight: 600;\n    font-size: 20px;\n    letter-spacing: 1.25px ;}\n.espTime4 {color: #F39C11 !important;\n font-weight: 600;\n    font-size: 20px;\n    letter-spacing: 1.25px;}\n.espTime5 {color: #3598DB !important;\nfont-weight: 600;\n    font-size: 20px;\n    letter-spacing: 1.25px;}\n.espTime6 {color: #E84C3D !important;\n font-weight: 600;\n    font-size: 20px;\n    letter-spacing: 1.25px;}\n/*heart failure end*/\n/*footer*/\n.londonSemi_footer {\nbackground: #fe3b61; /* Old browsers */ /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */\nbackground: linear-gradient(45deg,  #fe3b61 0%,#fe464d 25%,#ff3b5e 48%,#ff4c36 71%,#ff5d1c 86%,#fe6d0c 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fe3b61', endColorstr='#fe6d0c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */\n\n}\n.setFooter {\n    text-align: right;\n    padding:7px 80px;\n}\n.setFooter img {    \n    width: 3%;\n    margin: 0 5px;\n}\n.setFooter span {    \n    color: #fff;\n    font-size: 24px;}\n/*seriesOne END*/\n.sponsore {    \n    background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    opacity: 1;\n    text-align: center;\n    color: #fff; \n    font-weight: bold;\n}\n.sponsore h1 {\n    margin: 0;    \n    padding: 10px 0;\n}\n.sponsorImage {\n    text-align: center;\n}\n.sponsorImage img {    \n    width: 200px;\n    padding: 40px 40px 0 0;\n}\n/*RESPONSIVE START*/\n@media and (min-width: 1030px) and (max-width: 1366px){}\n/*Tablet landscape (1024x768)*/\n@media (min-width:992px) and (max-width:1199px){\n.btnMore {margin: 20px 120px !important;}\n}\n/*Tablet portrait (768x1024)*/\n@media (min-width:758px) and (max-width:800px){\n.header .heading h1{font-size: 50px !important;}\n.header .heading h4 {font-size: 22px !important;}\n.londonNavbar ul{text-align: center;}\n.para_width {max-width: 450px;}\n.londonNavbar ul li a span {display: none;}\n.para img {margin: 10px 15px 3px 0px;width: 70%;}\n.para p {font-size: 16px;}\n.heartFailure h2{font-size: 20px;}\n.espinfo {font-size: 16px;}\n.btnMore {margin: 10px 0 !important; width: 100% !important;}\n.setFooter {padding: 10px !important;}\n}\n/*Small tablet landscape (800x600)*/\n@media (min-width:790px) and (max-width:900px){\n.header .heading h1{font-size: 50px !important;}\n.header .heading h4 {font-size: 22px !important;}\n.londonNavbar ul{text-align: center;}\n.para_width {max-width: 450px;}\n.para img {margin: 10px 15px 3px 0px;width: 70%;}\n.para p {font-size: 16px;}\n.heartFailure h2{font-size: 20px;}\n.espinfo {font-size: 16px;}\n.btnMore {margin: 10px 0 !important; width: 100% !important;}\n.setFooter {padding: 10px !important;}\n}\n/*Small tablet portrait (600x800)*/\n@media (min-width:590px) and (max-width:767px){\n.header .heading {padding: 50px 0;}    \n.header .heading h1{font-size: 50px !important;}\n.header .heading h4 {font-size: 22px !important;}\n.londonNavbar ul{text-align: center;}\n.para_width {max-width: 450px;}\n.para img {margin: 10px 15px 3px 0px;width: 70%;}\n.para p {font-size: 16px;}\n.heartFailure h2{font-size: 20px;}\n.episodePara .espinfo {font-size: 16px;}\n.episodePara {padding: 15px!important;}\n.espinfo {font-size: 14px;}\n.btnMore {margin: 10px 0 !important; width: 100% !important;}\n.setFooter {padding: 10px !important;}\n}\n@media screen and (max-width: 500px){\n.londonNavbar ul li a {\nfont-size: 16px;\nline-height: 12px;\n}\n.londonNavbar{padding-bottom: 20px;}\n.header .heading {padding: 30px 0;}\n.header .heading h1{font-size: 50px !important;    line-height: 46px;}\n.header .heading h4 {font-size: 22px !important;}\n.londonNavbar ul{text-align: center;}\n.londonNavbar ul li a span {display: none;}\n.para img {margin: 10px 15px 3px 0px;width: 70%;}\n.para p {font-size: 14px;}\n.heartFailure h2{font-size: 20px;}\n.espinfo {font-size: 16px;}\n.btnMore {margin: 10px 0 !important; width: 100% !important;}\n.setFooter {padding: 10px !important;}\n.sponsorImage img {\n    width: 110px;}\n.heartFailureimg {\nmargin: 10px 0;\n}\n}\n/*Mobile portrait (320x480)*/\n@media (min-width:310px) and (max-width:470px){\n.header .heading {padding: 30px 0;}\n.header .heading h1{font-size: 50px !important;}\n.header .heading h4 {font-size: 14px !important;}\n.londonNavbar ul{text-align: center;}\n.londonNavbar ul li a span {display: none;}\n.para img {margin: 10px 15px 3px 0px;}\n.para p {font-size: 18px;}\n.heartFailure h2{font-size: 20px;}\n.espinfo {font-size: 16px;}\n.btnMore {width: 30%; margin: 10px 20px 0 30px;font-size: 14px;}\n.setFooter {padding: 10px !important;}\n}\n/*Mobile  landscape (480x320)*/\n@media (min-width:470px) and (max-width:580px){\n.header .heading h1{font-size: 50px !important;}\n.header .heading h4 {font-size: 22px !important;}\n.londonNavbar ul{text-align: center;}\n.londonNavbar ul li a span {display: none;}\n.para img {margin: 10px 15px 3px 0px;width: 70%;}\n.para p {font-size: 14px;}\n.heartFailure h2{font-size: 20px;}\n.espinfo {font-size: 16px;}\n.btnMore {margin: 10px 0 !important; width: 100% !important;}\n.setFooter {padding: 10px !important;}\n}\n@media screen and (min-width: 1400px) {}\n@media screen and (min-width: 1600px) {}\n@media screen and (min-width: 1900px) {}\n@media (min-width: 992px) {}\n@media (min-width: 1200px){}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5kZXgtcGFnZS9pbmRleC1wYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQ0FBaUM7QUFDckM7QUFDQSxTQUFTO0FBQ1Q7QUFDQSx3SkFBd0o7QUFDeEosa0JBQWtCO0FBQ2xCLFdBQVc7QUFDWDtBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCO0FBQ0E7Q0FDQyxlQUFlO0lBQ1osU0FBUztJQUNULFdBQVc7QUFDZjtBQUNBLGFBQWE7QUFFYixhQUFhO0FBQ2I7SUFDSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLFNBQVM7QUFDYjtBQUNBO0lBQ0ksV0FBVztJQUNYLGVBQWU7SUFDZixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLGVBQWU7S0FDZCxjQUFjO0FBQ25CO0FBQ0EsaUJBQWlCO0FBR2pCO0lBQ0ksdURBQXVEO0lBQ3ZELHdCQUF3QjtJQUN4QixlQUFlO0FBQ25CO0FBQ0EsWUFBWTtBQUNaO0NBQ0MsV0FBVztJQUNSLHFCQUFxQjtJQUNyQixvQkFBb0I7QUFDeEI7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLGNBQWM7QUFDbEI7QUFDQTtJQUNJLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsY0FBYztBQUNsQjtBQUNBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxnQkFBZ0I7S0FDZixZQUFZO0NBQ2hCO0FBQ0QsZ0JBQWdCO0FBRWhCLFlBQVk7QUFDWjtJQUNJLFdBQVc7QUFDZjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsaUJBQWlCO0lBQ2pCLG1CQUFtQjs7QUFFdkI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsZUFBZTtBQUNuQjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsWUFBWTtDQUNmO0FBRUQsZ0JBQWdCO0FBQ2hCO0lBQ0ksY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxXQUFXO0FBQ2Y7QUFDQTtJQUNJLGdCQUFnQjtJQUNoQix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QixjQUFjO0lBQ2QsVUFBVTtJQUNWLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSx3QkFBd0I7SUFDeEIsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixzQkFBc0I7QUFDMUI7QUFFQTtJQUNJLFdBQVc7SUFDWCxnQkFBZ0I7SUFDaEIsZUFBZTtBQUNuQjtBQUNBO0lBQ0ksZUFBZTtBQUNuQjtBQUNBO0lBQ0ksV0FBVztJQUNYLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSxZQUFZO0lBQ1osdUJBQXVCO0lBQ3ZCLGVBQWU7SUFDZixjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLHNCQUFzQjtJQUN0QixvQkFBb0I7QUFDeEI7QUFDQSxTQUFTLFlBQVk7SUFDakIsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixVQUFVO0lBQ1YsaUJBQWlCO0lBQ2pCLGVBQWUsQ0FBQztBQUNwQixVQUFVLHlCQUF5QjtLQUM5QixnQkFBZ0I7SUFDakIsZUFBZTtJQUNmLHNCQUFzQixDQUFDO0FBQzNCLFdBQVcseUJBQXlCO0NBQ25DLGdCQUFnQjtJQUNiLGVBQWU7SUFDZix1QkFBdUIsQ0FBQztBQUM1QixXQUFXLHlCQUF5QjtDQUNuQyxnQkFBZ0I7SUFDYixlQUFlO0lBQ2Ysc0JBQXNCLENBQUM7QUFDM0IsV0FBVyx5QkFBeUI7QUFDcEMsZ0JBQWdCO0lBQ1osZUFBZTtJQUNmLHNCQUFzQixDQUFDO0FBQzNCLFdBQVcseUJBQXlCO0NBQ25DLGdCQUFnQjtJQUNiLGVBQWU7SUFDZixzQkFBc0IsQ0FBQztBQUMzQixvQkFBb0I7QUFHcEIsU0FBUztBQUNUO0FBQ0EsbUJBQW1CLEVBQUUsaUJBQWlCLEVBQ2tGLGFBQWEsRUFDZiw0QkFBNEI7QUFDbEosNEdBQTRHLEVBQUUscURBQXFEO0FBQ25LLG1IQUFtSCxFQUFFLDBDQUEwQzs7QUFFL0o7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFNBQVM7SUFDVCxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsZUFBZSxDQUFDO0FBQ3BCLGdCQUFnQjtBQUNoQjtJQUNJLHdKQUF3SjtJQUN4SixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxpQkFBaUI7QUFDckI7QUFDQTtJQUNJLFNBQVM7SUFDVCxlQUFlO0FBQ25CO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLFlBQVk7SUFDWixzQkFBc0I7QUFDMUI7QUFLQSxtQkFBbUI7QUFDbkIsdURBQXVEO0FBQ3ZELDhCQUE4QjtBQUM5QjtBQUNBLFVBQVUsNkJBQTZCLENBQUM7QUFDeEM7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSxvQkFBb0IsMEJBQTBCLENBQUM7QUFDL0MscUJBQXFCLDBCQUEwQixDQUFDO0FBQ2hELGlCQUFpQixrQkFBa0IsQ0FBQztBQUNwQyxhQUFhLGdCQUFnQixDQUFDO0FBQzlCLDRCQUE0QixhQUFhLENBQUM7QUFDMUMsV0FBVyx5QkFBeUIsQ0FBQyxVQUFVLENBQUM7QUFDaEQsU0FBUyxlQUFlLENBQUM7QUFDekIsaUJBQWlCLGVBQWUsQ0FBQztBQUNqQyxVQUFVLGVBQWUsQ0FBQztBQUMxQixVQUFVLHlCQUF5QixFQUFFLHNCQUFzQixDQUFDO0FBQzVELFlBQVksd0JBQXdCLENBQUM7QUFDckM7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQSxvQkFBb0IsMEJBQTBCLENBQUM7QUFDL0MscUJBQXFCLDBCQUEwQixDQUFDO0FBQ2hELGlCQUFpQixrQkFBa0IsQ0FBQztBQUNwQyxhQUFhLGdCQUFnQixDQUFDO0FBQzlCLFdBQVcseUJBQXlCLENBQUMsVUFBVSxDQUFDO0FBQ2hELFNBQVMsZUFBZSxDQUFDO0FBQ3pCLGlCQUFpQixlQUFlLENBQUM7QUFDakMsVUFBVSxlQUFlLENBQUM7QUFDMUIsVUFBVSx5QkFBeUIsRUFBRSxzQkFBc0IsQ0FBQztBQUM1RCxZQUFZLHdCQUF3QixDQUFDO0FBQ3JDO0FBQ0Esa0NBQWtDO0FBQ2xDO0FBQ0Esa0JBQWtCLGVBQWUsQ0FBQztBQUNsQyxvQkFBb0IsMEJBQTBCLENBQUM7QUFDL0MscUJBQXFCLDBCQUEwQixDQUFDO0FBQ2hELGlCQUFpQixrQkFBa0IsQ0FBQztBQUNwQyxhQUFhLGdCQUFnQixDQUFDO0FBQzlCLFdBQVcseUJBQXlCLENBQUMsVUFBVSxDQUFDO0FBQ2hELFNBQVMsZUFBZSxDQUFDO0FBQ3pCLGlCQUFpQixlQUFlLENBQUM7QUFDakMsdUJBQXVCLGVBQWUsQ0FBQztBQUN2QyxjQUFjLHVCQUF1QixDQUFDO0FBQ3RDLFVBQVUsZUFBZSxDQUFDO0FBQzFCLFVBQVUseUJBQXlCLEVBQUUsc0JBQXNCLENBQUM7QUFDNUQsWUFBWSx3QkFBd0IsQ0FBQztBQUNyQztBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsaUJBQWlCO0FBQ2pCO0FBQ0EsY0FBYyxvQkFBb0IsQ0FBQztBQUNuQyxrQkFBa0IsZUFBZSxDQUFDO0FBQ2xDLG9CQUFvQiwwQkFBMEIsS0FBSyxpQkFBaUIsQ0FBQztBQUNyRSxxQkFBcUIsMEJBQTBCLENBQUM7QUFDaEQsaUJBQWlCLGtCQUFrQixDQUFDO0FBQ3BDLDRCQUE0QixhQUFhLENBQUM7QUFDMUMsV0FBVyx5QkFBeUIsQ0FBQyxVQUFVLENBQUM7QUFDaEQsU0FBUyxlQUFlLENBQUM7QUFDekIsaUJBQWlCLGVBQWUsQ0FBQztBQUNqQyxVQUFVLGVBQWUsQ0FBQztBQUMxQixVQUFVLHlCQUF5QixFQUFFLHNCQUFzQixDQUFDO0FBQzVELFlBQVksd0JBQXdCLENBQUM7QUFDckM7SUFDSSxZQUFZLENBQUM7QUFDakI7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNDLDRCQUE0QjtBQUM3QjtBQUNBLGtCQUFrQixlQUFlLENBQUM7QUFDbEMsb0JBQW9CLDBCQUEwQixDQUFDO0FBQy9DLHFCQUFxQiwwQkFBMEIsQ0FBQztBQUNoRCxpQkFBaUIsa0JBQWtCLENBQUM7QUFDcEMsNEJBQTRCLGFBQWEsQ0FBQztBQUMxQyxXQUFXLHlCQUF5QixDQUFDO0FBQ3JDLFNBQVMsZUFBZSxDQUFDO0FBQ3pCLGlCQUFpQixlQUFlLENBQUM7QUFDakMsVUFBVSxlQUFlLENBQUM7QUFDMUIsVUFBVSxVQUFVLEVBQUUsd0JBQXdCLENBQUMsZUFBZSxDQUFDO0FBQy9ELFlBQVksd0JBQXdCLENBQUM7QUFDckM7QUFDQSw4QkFBOEI7QUFDOUI7QUFDQSxvQkFBb0IsMEJBQTBCLENBQUM7QUFDL0MscUJBQXFCLDBCQUEwQixDQUFDO0FBQ2hELGlCQUFpQixrQkFBa0IsQ0FBQztBQUNwQyw0QkFBNEIsYUFBYSxDQUFDO0FBQzFDLFdBQVcseUJBQXlCLENBQUMsVUFBVSxDQUFDO0FBQ2hELFNBQVMsZUFBZSxDQUFDO0FBQ3pCLGlCQUFpQixlQUFlLENBQUM7QUFDakMsVUFBVSxlQUFlLENBQUM7QUFDMUIsVUFBVSx5QkFBeUIsRUFBRSxzQkFBc0IsQ0FBQztBQUM1RCxZQUFZLHdCQUF3QixDQUFDO0FBQ3JDO0FBQ0EsdUNBQXVDO0FBQ3ZDLHVDQUF1QztBQUN2Qyx1Q0FBdUM7QUFDdkMsMkJBQTJCO0FBQzNCLDJCQUEyQiIsImZpbGUiOiJzcmMvYXBwL2luZGV4LXBhZ2UvaW5kZXgtcGFnZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndyYXBlciB7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xufVxuLypoZWFkZXIqL1xuLmhlYWRlciB7XG5iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICNGRTNCNjEgMCUsICNGRTQ2NEQgMTklLCAjRkYzQjVFIDQwJSwgI0ZGNEMzNiA2MCUsICNGRjVEMUMgODElLCAjRkU2RDBDIDEwMCUpIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbnRleHQtYWxpZ246IGNlbnRlcjtcbmNvbG9yOiAjZmZmO1xufVxuLmhlYWRlciAuaGVhZGluZyB7XG4gICAgcGFkZGluZzogMTMwcHggMDtcbn1cbi5oZWFkZXIgLmhlYWRpbmcgaDEgeyAgICBcbiAgICBmb250LXNpemU6IDE1MHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtZmFtaWx5OiBhdXRvO1xuICAgIGxpbmUtaGVpZ2h0OiAxNDBweDtcbn1cbi5oZWFkZXIgLmhlYWRpbmcgaDQgeyAgICBcblx0Zm9udC1zaXplOiA1MHB4O1xuICAgIG1hcmdpbjogMDtcbiAgICBjb2xvcjogIzAwMDtcbn1cbi8qaGVhZGVyIGVuZCovXG5cbi8qbmF2aWdhdGlvbiovXG4ubG9uZG9uTmF2YmFyIHsgICAgXG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgcGFkZGluZzowO1xufVxuLmxvbmRvbk5hdmJhciB1bCBsaSBhIHtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBmb250LXNpemU6IDI4cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi5sb25kb25OYXZiYXIgdWwgbGkgYSBzcGFuIHtcbiAgICBwYWRkaW5nOiAwIDE1cHg7XG4gICAgIGNvbG9yOiAjZjBkY2RjO1xufVxuLypuYXZpZ2F0aW9uIGVuZCovXG5cblxuLmNvbnRlbnRTZWN0b24ge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6dXJsKC9hc3NldHMvaW1hZ2VzL2NvbnRlbnRTZWN0aW9uLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIHBhZGRpbmc6IDMwcHggMDtcbn1cbi8qcGFyYWdyYXBoKi9cbi5wYXJhIGltZyB7XG5cdGZsb2F0OiBsZWZ0O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHJlZDtcbiAgICBtYXJnaW46IDVweCAzNXB4IDVweDtcbn1cbi5wYXJhIHAgeyAgICBcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMS41cHg7XG4gICAgY29sb3I6ICMwNzA3MDc7XG59XG4ucGFyYSB1bCB7ICAgXG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjVweDtcbiAgICBjb2xvcjogIzA3MDcwNztcbn1cbi5wYXJhQm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4ucGFyYV93aWR0aCB7XG4gICAgbWF4LXdpZHRoOiA3MTBweDtcbiAgICAgbWFyZ2luOiBhdXRvO1xuIH1cbi8qcGFyYWdyYXBoIGVuZCovXG5cbi8qc2VyaWVzT25lKi9cbi5zZXJpZXNPbmUgLnNlcmllc0hlYWRpbmcgaW1ne1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmhlYXJ0RmFpbHVyZSB7XG4gICAgbWFyZ2luOiA1MHB4IDA7XG59XG4uaGVhcnRGYWlsdXJlIGgyIHsgICAgXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMzZweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDNweDtcblxufVxuLmhlYXJ0RmFpbHVyZSBwIHsgICAgXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IDI4cHg7XG59XG4uaGVhcnRGYWlsdXJlIGhyIHtcbiAgICB3aWR0aDogMjgwcHg7XG4gICAgYm9yZGVyLWNvbG9yOiAjMDAwO1xuICAgIGJvcmRlci13aWR0aDogMnB4O1xuICAgIG1hcmdpbjogYXV0bztcbiB9XG5cbi8qaGVhcnQgRmFpbHVyZSovXG4uaGVhcnRGYWlsdXJlaW1nIHtcbiAgICBtYXJnaW46IDUwcHggMDtcbn1cbi5oZWFydEZhaWx1cmVpbWcgaDQge1xuICAgIGNvbG9yOiAjMzkzNzM3O1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5oZWFydEZhaWx1cmVpbWcgaW1nIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cbi5oZWFydEZhaWx1cmVpbWcgLmVwaXNvZGVQYXJhe1xuICAgIGJhY2tncm91bmQ6ICNGRkY7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzcwNzA3MDsgXG4gICAgcGFkZGluZzogMTVweCA0NXB4O1xuICAgIGJvcmRlci10b3A6IG5vbmU7XG59XG4uaGVhcnRGYWlsdXJlaW1nIC5lcGlzb2RlUGFyYSBweyAgICBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDEuMjdweDtcbiAgICBjb2xvcjogIzBBMEEwQTtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5lc3BUaW1le1xuICAgIGNvbG9yOiNEMkFCMkEgIWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMS4yNXB4O1xufVxuICBcbi5lc3BpbmZvIHsgICAgXG4gICAgY29sb3I6ICMwMDA7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDIwcHg7XG59XG4uZXNwc3ViIHsgICAgXG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuLmVzcHByb2Yge1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNnB4O1xufVxuLmJ0bk1vcmV7ICAgIFxuICAgIGJvcmRlcjogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6ICMzOTM3Mzc7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMS4yNXB4O1xuICAgIG1hcmdpbjogMzBweCAxNjBweCAwO1xufVxuLmJ0blJlZyB7Ym9yZGVyOiBub25lO1xuICAgIGJhY2tncm91bmQ6ICM5NmE0YTU7XG4gICAgcGFkZGluZzogNXB4IDEwcHg7XG4gICAgd2lkdGg6IDI1JTtcbiAgICBtYXJnaW46IDIwcHggNTBweDtcbiAgICBmb250LXNpemU6IDE2cHg7fVxuLmVzcFRpbWUye2NvbG9yOiAjOEQ0NEFEICFpbXBvcnRhbnQ7XG4gICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjI1cHg7fVxuLmVzcFRpbWUzIHtjb2xvcjogIzFDQkI5QiAhaW1wb3J0YW50O1xuIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjI1cHggO31cbi5lc3BUaW1lNCB7Y29sb3I6ICNGMzlDMTEgIWltcG9ydGFudDtcbiBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMS4yNXB4O31cbi5lc3BUaW1lNSB7Y29sb3I6ICMzNTk4REIgIWltcG9ydGFudDtcbmZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjI1cHg7fVxuLmVzcFRpbWU2IHtjb2xvcjogI0U4NEMzRCAhaW1wb3J0YW50O1xuIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjI1cHg7fVxuLypoZWFydCBmYWlsdXJlIGVuZCovXG5cblxuLypmb290ZXIqL1xuLmxvbmRvblNlbWlfZm9vdGVyIHtcbmJhY2tncm91bmQ6ICNmZTNiNjE7IC8qIE9sZCBicm93c2VycyAqL1xuYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoNDVkZWcsICAjZmUzYjYxIDAlLCAjZmU0NjRkIDI1JSwgI2ZmM2I1ZSA0OCUsICNmZjRjMzYgNzElLCAjZmY1ZDFjIDg2JSwgI2ZlNmQwYyAxMDAlKTsgLyogRkYzLjYtMTUgKi9cbmJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAgI2ZlM2I2MSAwJSwjZmU0NjRkIDI1JSwjZmYzYjVlIDQ4JSwjZmY0YzM2IDcxJSwjZmY1ZDFjIDg2JSwjZmU2ZDBjIDEwMCUpOyAvKiBDaHJvbWUxMC0yNSxTYWZhcmk1LjEtNiAqL1xuYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAgI2ZlM2I2MSAwJSwjZmU0NjRkIDI1JSwjZmYzYjVlIDQ4JSwjZmY0YzM2IDcxJSwjZmY1ZDFjIDg2JSwjZmU2ZDBjIDEwMCUpOyAvKiBXM0MsIElFMTArLCBGRjE2KywgQ2hyb21lMjYrLCBPcGVyYTEyKywgU2FmYXJpNysgKi9cbmZpbHRlcjogcHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LmdyYWRpZW50KCBzdGFydENvbG9yc3RyPScjZmUzYjYxJywgZW5kQ29sb3JzdHI9JyNmZTZkMGMnLEdyYWRpZW50VHlwZT0xICk7IC8qIElFNi05IGZhbGxiYWNrIG9uIGhvcml6b250YWwgZ3JhZGllbnQgKi9cblxufVxuLnNldEZvb3RlciB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgcGFkZGluZzo3cHggODBweDtcbn1cbi5zZXRGb290ZXIgaW1nIHsgICAgXG4gICAgd2lkdGg6IDMlO1xuICAgIG1hcmdpbjogMCA1cHg7XG59XG4uc2V0Rm9vdGVyIHNwYW4geyAgICBcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDI0cHg7fVxuLypzZXJpZXNPbmUgRU5EKi9cbi5zcG9uc29yZSB7ICAgIFxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50IGxpbmVhci1ncmFkaWVudCg5MGRlZywgI0ZFM0I2MSAwJSwgI0ZFNDY0RCAxOSUsICNGRjNCNUUgNDAlLCAjRkY0QzM2IDYwJSwgI0ZGNUQxQyA4MSUsICNGRTZEMEMgMTAwJSkgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmOyBcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn0gIFxuLnNwb25zb3JlIGgxIHtcbiAgICBtYXJnaW46IDA7ICAgIFxuICAgIHBhZGRpbmc6IDEwcHggMDtcbn1cbi5zcG9uc29ySW1hZ2Uge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5zcG9uc29ySW1hZ2UgaW1nIHsgICAgXG4gICAgd2lkdGg6IDIwMHB4O1xuICAgIHBhZGRpbmc6IDQwcHggNDBweCAwIDA7XG59XG5cblxuXG5cbi8qUkVTUE9OU0lWRSBTVEFSVCovXG5AbWVkaWEgYW5kIChtaW4td2lkdGg6IDEwMzBweCkgYW5kIChtYXgtd2lkdGg6IDEzNjZweCl7fVxuLypUYWJsZXQgbGFuZHNjYXBlICgxMDI0eDc2OCkqL1xuQG1lZGlhIChtaW4td2lkdGg6OTkycHgpIGFuZCAobWF4LXdpZHRoOjExOTlweCl7XG4uYnRuTW9yZSB7bWFyZ2luOiAyMHB4IDEyMHB4ICFpbXBvcnRhbnQ7fVxufVxuLypUYWJsZXQgcG9ydHJhaXQgKDc2OHgxMDI0KSovXG5AbWVkaWEgKG1pbi13aWR0aDo3NThweCkgYW5kIChtYXgtd2lkdGg6ODAwcHgpe1xuLmhlYWRlciAuaGVhZGluZyBoMXtmb250LXNpemU6IDUwcHggIWltcG9ydGFudDt9XG4uaGVhZGVyIC5oZWFkaW5nIGg0IHtmb250LXNpemU6IDIycHggIWltcG9ydGFudDt9XG4ubG9uZG9uTmF2YmFyIHVse3RleHQtYWxpZ246IGNlbnRlcjt9XG4ucGFyYV93aWR0aCB7bWF4LXdpZHRoOiA0NTBweDt9XG4ubG9uZG9uTmF2YmFyIHVsIGxpIGEgc3BhbiB7ZGlzcGxheTogbm9uZTt9XG4ucGFyYSBpbWcge21hcmdpbjogMTBweCAxNXB4IDNweCAwcHg7d2lkdGg6IDcwJTt9XG4ucGFyYSBwIHtmb250LXNpemU6IDE2cHg7fVxuLmhlYXJ0RmFpbHVyZSBoMntmb250LXNpemU6IDIwcHg7fVxuLmVzcGluZm8ge2ZvbnQtc2l6ZTogMTZweDt9XG4uYnRuTW9yZSB7bWFyZ2luOiAxMHB4IDAgIWltcG9ydGFudDsgd2lkdGg6IDEwMCUgIWltcG9ydGFudDt9XG4uc2V0Rm9vdGVyIHtwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7fVxufVxuLypTbWFsbCB0YWJsZXQgbGFuZHNjYXBlICg4MDB4NjAwKSovXG5AbWVkaWEgKG1pbi13aWR0aDo3OTBweCkgYW5kIChtYXgtd2lkdGg6OTAwcHgpe1xuLmhlYWRlciAuaGVhZGluZyBoMXtmb250LXNpemU6IDUwcHggIWltcG9ydGFudDt9XG4uaGVhZGVyIC5oZWFkaW5nIGg0IHtmb250LXNpemU6IDIycHggIWltcG9ydGFudDt9XG4ubG9uZG9uTmF2YmFyIHVse3RleHQtYWxpZ246IGNlbnRlcjt9XG4ucGFyYV93aWR0aCB7bWF4LXdpZHRoOiA0NTBweDt9XG4ucGFyYSBpbWcge21hcmdpbjogMTBweCAxNXB4IDNweCAwcHg7d2lkdGg6IDcwJTt9XG4ucGFyYSBwIHtmb250LXNpemU6IDE2cHg7fVxuLmhlYXJ0RmFpbHVyZSBoMntmb250LXNpemU6IDIwcHg7fVxuLmVzcGluZm8ge2ZvbnQtc2l6ZTogMTZweDt9XG4uYnRuTW9yZSB7bWFyZ2luOiAxMHB4IDAgIWltcG9ydGFudDsgd2lkdGg6IDEwMCUgIWltcG9ydGFudDt9XG4uc2V0Rm9vdGVyIHtwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7fVxufVxuLypTbWFsbCB0YWJsZXQgcG9ydHJhaXQgKDYwMHg4MDApKi9cbkBtZWRpYSAobWluLXdpZHRoOjU5MHB4KSBhbmQgKG1heC13aWR0aDo3NjdweCl7XG4uaGVhZGVyIC5oZWFkaW5nIHtwYWRkaW5nOiA1MHB4IDA7fSAgICBcbi5oZWFkZXIgLmhlYWRpbmcgaDF7Zm9udC1zaXplOiA1MHB4ICFpbXBvcnRhbnQ7fVxuLmhlYWRlciAuaGVhZGluZyBoNCB7Zm9udC1zaXplOiAyMnB4ICFpbXBvcnRhbnQ7fVxuLmxvbmRvbk5hdmJhciB1bHt0ZXh0LWFsaWduOiBjZW50ZXI7fVxuLnBhcmFfd2lkdGgge21heC13aWR0aDogNDUwcHg7fVxuLnBhcmEgaW1nIHttYXJnaW46IDEwcHggMTVweCAzcHggMHB4O3dpZHRoOiA3MCU7fVxuLnBhcmEgcCB7Zm9udC1zaXplOiAxNnB4O31cbi5oZWFydEZhaWx1cmUgaDJ7Zm9udC1zaXplOiAyMHB4O31cbi5lcGlzb2RlUGFyYSAuZXNwaW5mbyB7Zm9udC1zaXplOiAxNnB4O31cbi5lcGlzb2RlUGFyYSB7cGFkZGluZzogMTVweCFpbXBvcnRhbnQ7fVxuLmVzcGluZm8ge2ZvbnQtc2l6ZTogMTRweDt9XG4uYnRuTW9yZSB7bWFyZ2luOiAxMHB4IDAgIWltcG9ydGFudDsgd2lkdGg6IDEwMCUgIWltcG9ydGFudDt9XG4uc2V0Rm9vdGVyIHtwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7fVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpe1xuLmxvbmRvbk5hdmJhciB1bCBsaSBhIHtcbmZvbnQtc2l6ZTogMTZweDtcbmxpbmUtaGVpZ2h0OiAxMnB4O1xufVxuLmxvbmRvbk5hdmJhcntwYWRkaW5nLWJvdHRvbTogMjBweDt9XG4uaGVhZGVyIC5oZWFkaW5nIHtwYWRkaW5nOiAzMHB4IDA7fVxuLmhlYWRlciAuaGVhZGluZyBoMXtmb250LXNpemU6IDUwcHggIWltcG9ydGFudDsgICAgbGluZS1oZWlnaHQ6IDQ2cHg7fVxuLmhlYWRlciAuaGVhZGluZyBoNCB7Zm9udC1zaXplOiAyMnB4ICFpbXBvcnRhbnQ7fVxuLmxvbmRvbk5hdmJhciB1bHt0ZXh0LWFsaWduOiBjZW50ZXI7fVxuLmxvbmRvbk5hdmJhciB1bCBsaSBhIHNwYW4ge2Rpc3BsYXk6IG5vbmU7fVxuLnBhcmEgaW1nIHttYXJnaW46IDEwcHggMTVweCAzcHggMHB4O3dpZHRoOiA3MCU7fVxuLnBhcmEgcCB7Zm9udC1zaXplOiAxNHB4O31cbi5oZWFydEZhaWx1cmUgaDJ7Zm9udC1zaXplOiAyMHB4O31cbi5lc3BpbmZvIHtmb250LXNpemU6IDE2cHg7fVxuLmJ0bk1vcmUge21hcmdpbjogMTBweCAwICFpbXBvcnRhbnQ7IHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7fVxuLnNldEZvb3RlciB7cGFkZGluZzogMTBweCAhaW1wb3J0YW50O31cbi5zcG9uc29ySW1hZ2UgaW1nIHtcbiAgICB3aWR0aDogMTEwcHg7fVxuLmhlYXJ0RmFpbHVyZWltZyB7XG5tYXJnaW46IDEwcHggMDtcbn1cbn1cbiAvKk1vYmlsZSBwb3J0cmFpdCAoMzIweDQ4MCkqL1xuQG1lZGlhIChtaW4td2lkdGg6MzEwcHgpIGFuZCAobWF4LXdpZHRoOjQ3MHB4KXtcbi5oZWFkZXIgLmhlYWRpbmcge3BhZGRpbmc6IDMwcHggMDt9XG4uaGVhZGVyIC5oZWFkaW5nIGgxe2ZvbnQtc2l6ZTogNTBweCAhaW1wb3J0YW50O31cbi5oZWFkZXIgLmhlYWRpbmcgaDQge2ZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O31cbi5sb25kb25OYXZiYXIgdWx7dGV4dC1hbGlnbjogY2VudGVyO31cbi5sb25kb25OYXZiYXIgdWwgbGkgYSBzcGFuIHtkaXNwbGF5OiBub25lO31cbi5wYXJhIGltZyB7bWFyZ2luOiAxMHB4IDE1cHggM3B4IDBweDt9XG4ucGFyYSBwIHtmb250LXNpemU6IDE4cHg7fVxuLmhlYXJ0RmFpbHVyZSBoMntmb250LXNpemU6IDIwcHg7fVxuLmVzcGluZm8ge2ZvbnQtc2l6ZTogMTZweDt9XG4uYnRuTW9yZSB7d2lkdGg6IDMwJTsgbWFyZ2luOiAxMHB4IDIwcHggMCAzMHB4O2ZvbnQtc2l6ZTogMTRweDt9XG4uc2V0Rm9vdGVyIHtwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7fVxufVxuLypNb2JpbGUgIGxhbmRzY2FwZSAoNDgweDMyMCkqL1xuQG1lZGlhIChtaW4td2lkdGg6NDcwcHgpIGFuZCAobWF4LXdpZHRoOjU4MHB4KXtcbi5oZWFkZXIgLmhlYWRpbmcgaDF7Zm9udC1zaXplOiA1MHB4ICFpbXBvcnRhbnQ7fVxuLmhlYWRlciAuaGVhZGluZyBoNCB7Zm9udC1zaXplOiAyMnB4ICFpbXBvcnRhbnQ7fVxuLmxvbmRvbk5hdmJhciB1bHt0ZXh0LWFsaWduOiBjZW50ZXI7fVxuLmxvbmRvbk5hdmJhciB1bCBsaSBhIHNwYW4ge2Rpc3BsYXk6IG5vbmU7fVxuLnBhcmEgaW1nIHttYXJnaW46IDEwcHggMTVweCAzcHggMHB4O3dpZHRoOiA3MCU7fVxuLnBhcmEgcCB7Zm9udC1zaXplOiAxNHB4O31cbi5oZWFydEZhaWx1cmUgaDJ7Zm9udC1zaXplOiAyMHB4O31cbi5lc3BpbmZvIHtmb250LXNpemU6IDE2cHg7fVxuLmJ0bk1vcmUge21hcmdpbjogMTBweCAwICFpbXBvcnRhbnQ7IHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7fVxuLnNldEZvb3RlciB7cGFkZGluZzogMTBweCAhaW1wb3J0YW50O31cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDE0MDBweCkge31cbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDE2MDBweCkge31cbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDE5MDBweCkge31cbkBtZWRpYSAobWluLXdpZHRoOiA5OTJweCkge31cbkBtZWRpYSAobWluLXdpZHRoOiAxMjAwcHgpe30iXX0= */");

/***/ }),

/***/ "./src/app/index-page/index-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/index-page/index-page.component.ts ***!
  \****************************************************/
/*! exports provided: IndexPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexPageComponent", function() { return IndexPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let IndexPageComponent = class IndexPageComponent {
    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
    }
};
IndexPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-index-page',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./index-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/index-page/index-page.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./index-page.component.css */ "./src/app/index-page/index-page.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], IndexPageComponent);



/***/ }),

/***/ "./src/app/my-account/my-account.component.css":
/*!*****************************************************!*\
  !*** ./src/app/my-account/my-account.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif;\n    padding: 40px 70px 30px 70px;\n    background-image: url(/assets/images/contentSection.png);\n    background-size: contain;\n    min-height: 100vh;\n\n}\n.myaccount {\n        width: 700px;\n    margin: auto;\n}\n.accountHeader {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    text-align: center;\n    margin: auto;}\n.accountHeader h1 {text-align: center;\n    letter-spacing: 1.27px;\n    color: #fff;\n    opacity: 1;\n    font-size: 42px;\n    padding: 50px 0;\n    font-weight: 600;\t}\n.accountContent{padding: 20px 40px;}\n.accountContent p {font-size: 20px; letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500; text-align: center;}\n.otpSection {     border: 1px solid #707070;\n    background: #EFEFEF;\n    padding: 50px;\n    width: 70%;\n    margin: auto;}\n.otpSection p {\n            text-align: center;\n    font-size: 19px;\n    font-weight: 500;\n    }\n.otpSection button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    color: #fff;\n    font-weight: 500;\n    font-size: 20px;\n    text-align: center;\n    margin: 0 140px;}\n@media screen and (max-width: 500px) {\n.wraper {\npadding: 10px 10px 10px 10px;\n}\n.myaccount{\nwidth: 100%;\nmargin: auto;\n}\n.accountHeader h1 {\nfont-size: 22px;\npadding: 10px 0;}\n.accountContent{\npadding: 10px;\n}\n.accountContent p {\nfont-size: 16px;\nletter-spacing: 1.25px;\ncolor: #040404;\nopacity: 1;\nline-height: 22px;}\n.otpSection {\nborder: 1px solid #707070;\nbackground: #EFEFEF;\npadding: 20px;\nwidth: 100%;}\n.otpSection button{font-size: 18px;margin: 0;width: 100%;}\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktYWNjb3VudC9teS1hY2NvdW50LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxpQ0FBaUM7SUFDdEMsNEJBQTRCO0lBQzVCLHdEQUF3RDtJQUN4RCx3QkFBd0I7SUFDeEIsaUJBQWlCOztBQUVyQjtBQUNBO1FBQ1EsWUFBWTtJQUNoQixZQUFZO0FBQ2hCO0FBQ0EsZ0JBQWdCLHdKQUF3SjtJQUNwSyxrQkFBa0I7SUFDbEIsWUFBWSxDQUFDO0FBQ2pCLG1CQUFtQixrQkFBa0I7SUFDakMsc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxVQUFVO0lBQ1YsZUFBZTtJQUNmLGVBQWU7SUFDZixnQkFBZ0IsRUFBRTtBQUV0QixnQkFBZ0Isa0JBQWtCLENBQUM7QUFDbkMsbUJBQW1CLGVBQWUsRUFBRSxzQkFBc0I7SUFDdEQsY0FBYztJQUNkLFVBQVUsQ0FBQyxpQkFBaUI7SUFDNUIsZ0JBQWdCLEVBQUUsa0JBQWtCLENBQUM7QUFDekMsa0JBQWtCLHlCQUF5QjtJQUN2QyxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFVBQVU7SUFDVixZQUFZLENBQUM7QUFDYjtZQUNRLGtCQUFrQjtJQUMxQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCO0FBQ0Esb0JBQW9CLHdKQUF3SjtJQUM1SyxXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsZUFBZSxDQUFDO0FBR3BCO0FBQ0E7QUFDQSw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLGVBQWU7QUFDZixlQUFlLENBQUM7QUFDaEI7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGVBQWU7QUFDZixzQkFBc0I7QUFDdEIsY0FBYztBQUNkLFVBQVU7QUFDVixpQkFBaUIsQ0FBQztBQUNsQjtBQUNBLHlCQUF5QjtBQUN6QixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLFdBQVcsQ0FBQztBQUNaLG1CQUFtQixlQUFlLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQztBQUN6RCIsImZpbGUiOiJzcmMvYXBwL215LWFjY291bnQvbXktYWNjb3VudC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndyYXBlciB7Zm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xuICAgIHBhZGRpbmc6IDQwcHggNzBweCAzMHB4IDcwcHg7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvaW1hZ2VzL2NvbnRlbnRTZWN0aW9uLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xuXG59XG4ubXlhY2NvdW50IHtcbiAgICAgICAgd2lkdGg6IDcwMHB4O1xuICAgIG1hcmdpbjogYXV0bztcbn1cbi5hY2NvdW50SGVhZGVyIHtiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICNGRTNCNjEgMCUsICNGRTQ2NEQgMTklLCAjRkYzQjVFIDQwJSwgI0ZGNEMzNiA2MCUsICNGRjVEMUMgODElLCAjRkU2RDBDIDEwMCUpIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiBhdXRvO31cbi5hY2NvdW50SGVhZGVyIGgxIHt0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDEuMjdweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtc2l6ZTogNDJweDtcbiAgICBwYWRkaW5nOiA1MHB4IDA7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcdH1cbiAgICBcbi5hY2NvdW50Q29udGVudHtwYWRkaW5nOiAyMHB4IDQwcHg7fVxuLmFjY291bnRDb250ZW50IHAge2ZvbnQtc2l6ZTogMjBweDsgbGV0dGVyLXNwYWNpbmc6IDEuMjVweDtcbiAgICBjb2xvcjogIzA0MDQwNDtcbiAgICBvcGFjaXR5OiAxO2xpbmUtaGVpZ2h0OiAyOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7IHRleHQtYWxpZ246IGNlbnRlcjt9XG4ub3RwU2VjdGlvbiB7ICAgICBib3JkZXI6IDFweCBzb2xpZCAjNzA3MDcwO1xuICAgIGJhY2tncm91bmQ6ICNFRkVGRUY7XG4gICAgcGFkZGluZzogNTBweDtcbiAgICB3aWR0aDogNzAlO1xuICAgIG1hcmdpbjogYXV0bzt9XG4gICAgLm90cFNlY3Rpb24gcCB7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxOXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuICAgIC5vdHBTZWN0aW9uIGJ1dHRvbiB7YmFja2dyb3VuZDogdHJhbnNwYXJlbnQgbGluZWFyLWdyYWRpZW50KDkwZGVnLCAjRkUzQjYxIDAlLCAjRkU0NjREIDE5JSwgI0ZGM0I1RSA0MCUsICNGRjRDMzYgNjAlLCAjRkY1RDFDIDgxJSwgI0ZFNkQwQyAxMDAlKSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogMCAxNDBweDt9XG5cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcbi53cmFwZXIge1xucGFkZGluZzogMTBweCAxMHB4IDEwcHggMTBweDtcbn1cbi5teWFjY291bnR7XG53aWR0aDogMTAwJTtcbm1hcmdpbjogYXV0bztcbn1cbi5hY2NvdW50SGVhZGVyIGgxIHtcbmZvbnQtc2l6ZTogMjJweDtcbnBhZGRpbmc6IDEwcHggMDt9XG4uYWNjb3VudENvbnRlbnR7XG5wYWRkaW5nOiAxMHB4O1xufVxuLmFjY291bnRDb250ZW50IHAge1xuZm9udC1zaXplOiAxNnB4O1xubGV0dGVyLXNwYWNpbmc6IDEuMjVweDtcbmNvbG9yOiAjMDQwNDA0O1xub3BhY2l0eTogMTtcbmxpbmUtaGVpZ2h0OiAyMnB4O31cbi5vdHBTZWN0aW9uIHtcbmJvcmRlcjogMXB4IHNvbGlkICM3MDcwNzA7XG5iYWNrZ3JvdW5kOiAjRUZFRkVGO1xucGFkZGluZzogMjBweDtcbndpZHRoOiAxMDAlO31cbi5vdHBTZWN0aW9uIGJ1dHRvbntmb250LXNpemU6IDE4cHg7bWFyZ2luOiAwO3dpZHRoOiAxMDAlO31cbn0iXX0= */");

/***/ }),

/***/ "./src/app/my-account/my-account.component.ts":
/*!****************************************************!*\
  !*** ./src/app/my-account/my-account.component.ts ***!
  \****************************************************/
/*! exports provided: MyAccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyAccountComponent", function() { return MyAccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let MyAccountComponent = class MyAccountComponent {
    constructor(accountFb) {
        this.accountFb = accountFb;
    }
    ngOnInit() {
        this.getopt();
    }
    getopt() {
        this.myAccount = this.accountFb.group({});
    }
};
MyAccountComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
MyAccountComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-account',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./my-account.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/my-account/my-account.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./my-account.component.css */ "./src/app/my-account/my-account.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], MyAccountComponent);



/***/ }),

/***/ "./src/app/otp/otp.component.css":
/*!***************************************!*\
  !*** ./src/app/otp/otp.component.css ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif;\n    padding: 40px 70px 30px 70px;\n    background-image: url(/assets/images/contentSection.png);\n    background-size: contain;\n    min-height: 100vh;\n\n}\n.myaccount {\n        width: 700px;\n    margin: auto;\n}\n.accountHeader {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    text-align: center;\n    margin: auto;}\n.accountHeader h1 {text-align: center;\n    letter-spacing: 1.27px;\n    color: #fff;\n    opacity: 1;\n    font-size: 42px;\n    padding: 50px 0;\n    font-weight: 600;\t}\n.accountContent{padding: 20px 40px;}\n.accountContent p {font-size: 20px; letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500; text-align: center;}\n.otpSection {     border: 1px solid #707070;\n    background: #EFEFEF;\n    padding: 50px;\n    width: 70%;\n    margin: auto;}\n.otpSection p {\n            text-align: center;\n    font-size: 19px;\n    font-weight: 500;\n    }\n.otpSection button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    color: #fff;\n    font-weight: 500;\n    font-size: 20px;\n    text-align: center;\n    margin: 0 140px;}\n@media screen and (max-width: 500px) {\n.wraper {\npadding: 10px 10px 10px 10px;\n}\n.myaccount{\nwidth: 100%;\nmargin: auto;\n}\n.accountHeader h1 {\nfont-size: 22px;\npadding: 10px 0;}\n.accountContent{\npadding: 10px;\n}\n.accountContent p {\nfont-size: 16px;\nletter-spacing: 1.25px;\ncolor: #040404;\nopacity: 1;\nline-height: 22px;}\n.otpSection {\nborder: 1px solid #707070;\nbackground: #EFEFEF;\npadding: 20px;\nwidth: 100%;}\n.otpSection button{font-size: 18px;margin: 0;width: 100%;}\n}    \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3RwL290cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFNBQVMsaUNBQWlDO0lBQ3RDLDRCQUE0QjtJQUM1Qix3REFBd0Q7SUFDeEQsd0JBQXdCO0lBQ3hCLGlCQUFpQjs7QUFFckI7QUFDQTtRQUNRLFlBQVk7SUFDaEIsWUFBWTtBQUNoQjtBQUNBLGdCQUFnQix3SkFBd0o7SUFDcEssa0JBQWtCO0lBQ2xCLFlBQVksQ0FBQztBQUNqQixtQkFBbUIsa0JBQWtCO0lBQ2pDLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsVUFBVTtJQUNWLGVBQWU7SUFDZixlQUFlO0lBQ2YsZ0JBQWdCLEVBQUU7QUFFdEIsZ0JBQWdCLGtCQUFrQixDQUFDO0FBQ25DLG1CQUFtQixlQUFlLEVBQUUsc0JBQXNCO0lBQ3RELGNBQWM7SUFDZCxVQUFVLENBQUMsaUJBQWlCO0lBQzVCLGdCQUFnQixFQUFFLGtCQUFrQixDQUFDO0FBQ3pDLGtCQUFrQix5QkFBeUI7SUFDdkMsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixVQUFVO0lBQ1YsWUFBWSxDQUFDO0FBQ2I7WUFDUSxrQkFBa0I7SUFDMUIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQjtBQUNBLG9CQUFvQix3SkFBd0o7SUFDNUssV0FBVztJQUNYLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLGVBQWUsQ0FBQztBQUdwQjtBQUNBO0FBQ0EsNEJBQTRCO0FBQzVCO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsZUFBZSxDQUFDO0FBQ2hCO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxlQUFlO0FBQ2Ysc0JBQXNCO0FBQ3RCLGNBQWM7QUFDZCxVQUFVO0FBQ1YsaUJBQWlCLENBQUM7QUFDbEI7QUFDQSx5QkFBeUI7QUFDekIsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixXQUFXLENBQUM7QUFDWixtQkFBbUIsZUFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7QUFDekQiLCJmaWxlIjoic3JjL2FwcC9vdHAvb3RwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcGVyIHtmb250LWZhbWlseTogJ1JvYm90bycsIHNhbnMtc2VyaWY7XG4gICAgcGFkZGluZzogNDBweCA3MHB4IDMwcHggNzBweDtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoL2Fzc2V0cy9pbWFnZXMvY29udGVudFNlY3Rpb24ucG5nKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgbWluLWhlaWdodDogMTAwdmg7XG5cbn1cbi5teWFjY291bnQge1xuICAgICAgICB3aWR0aDogNzAwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xufVxuLmFjY291bnRIZWFkZXIge2JhY2tncm91bmQ6IHRyYW5zcGFyZW50IGxpbmVhci1ncmFkaWVudCg5MGRlZywgI0ZFM0I2MSAwJSwgI0ZFNDY0RCAxOSUsICNGRjNCNUUgNDAlLCAjRkY0QzM2IDYwJSwgI0ZGNUQxQyA4MSUsICNGRTZEMEMgMTAwJSkgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IGF1dG87fVxuLmFjY291bnRIZWFkZXIgaDEge3RleHQtYWxpZ246IGNlbnRlcjtcbiAgICBsZXR0ZXItc3BhY2luZzogMS4yN3B4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZm9udC1zaXplOiA0MnB4O1xuICAgIHBhZGRpbmc6IDUwcHggMDtcbiAgICBmb250LXdlaWdodDogNjAwO1x0fVxuICAgIFxuLmFjY291bnRDb250ZW50e3BhZGRpbmc6IDIwcHggNDBweDt9XG4uYWNjb3VudENvbnRlbnQgcCB7Zm9udC1zaXplOiAyMHB4OyBsZXR0ZXItc3BhY2luZzogMS4yNXB4O1xuICAgIGNvbG9yOiAjMDQwNDA0O1xuICAgIG9wYWNpdHk6IDE7bGluZS1oZWlnaHQ6IDI4cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDsgdGV4dC1hbGlnbjogY2VudGVyO31cbi5vdHBTZWN0aW9uIHsgICAgIGJvcmRlcjogMXB4IHNvbGlkICM3MDcwNzA7XG4gICAgYmFja2dyb3VuZDogI0VGRUZFRjtcbiAgICBwYWRkaW5nOiA1MHB4O1xuICAgIHdpZHRoOiA3MCU7XG4gICAgbWFyZ2luOiBhdXRvO31cbiAgICAub3RwU2VjdGlvbiBwIHtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDE5cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG4gICAgLm90cFNlY3Rpb24gYnV0dG9uIHtiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICNGRTNCNjEgMCUsICNGRTQ2NEQgMTklLCAjRkYzQjVFIDQwJSwgI0ZGNEMzNiA2MCUsICNGRjVEMUMgODElLCAjRkU2RDBDIDEwMCUpIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiAwIDE0MHB4O31cblxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xuLndyYXBlciB7XG5wYWRkaW5nOiAxMHB4IDEwcHggMTBweCAxMHB4O1xufVxuLm15YWNjb3VudHtcbndpZHRoOiAxMDAlO1xubWFyZ2luOiBhdXRvO1xufVxuLmFjY291bnRIZWFkZXIgaDEge1xuZm9udC1zaXplOiAyMnB4O1xucGFkZGluZzogMTBweCAwO31cbi5hY2NvdW50Q29udGVudHtcbnBhZGRpbmc6IDEwcHg7XG59XG4uYWNjb3VudENvbnRlbnQgcCB7XG5mb250LXNpemU6IDE2cHg7XG5sZXR0ZXItc3BhY2luZzogMS4yNXB4O1xuY29sb3I6ICMwNDA0MDQ7XG5vcGFjaXR5OiAxO1xubGluZS1oZWlnaHQ6IDIycHg7fVxuLm90cFNlY3Rpb24ge1xuYm9yZGVyOiAxcHggc29saWQgIzcwNzA3MDtcbmJhY2tncm91bmQ6ICNFRkVGRUY7XG5wYWRkaW5nOiAyMHB4O1xud2lkdGg6IDEwMCU7fVxuLm90cFNlY3Rpb24gYnV0dG9ue2ZvbnQtc2l6ZTogMThweDttYXJnaW46IDA7d2lkdGg6IDEwMCU7fVxufSAgICAiXX0= */");

/***/ }),

/***/ "./src/app/otp/otp.component.ts":
/*!**************************************!*\
  !*** ./src/app/otp/otp.component.ts ***!
  \**************************************/
/*! exports provided: OtpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpComponent", function() { return OtpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let OtpComponent = class OtpComponent {
    constructor(otpFb) {
        this.otpFb = otpFb;
    }
    ngOnInit() {
        this.getopt();
    }
    getopt() {
        this.otp = this.otpFb.group({});
    }
};
OtpComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
OtpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-otp',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./otp.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/otp/otp.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./otp.component.css */ "./src/app/otp/otp.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], OtpComponent);



/***/ }),

/***/ "./src/app/registration-page/registration-page.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/registration-page/registration-page.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wrapper {\n    width: 60%;\n    margin: auto;\n    margin-top: 10px;\n}\n.header-register {\nbackground: #fe3b61; /* Old browsers */ /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */\nbackground: linear-gradient(45deg,  #fe3b61 0%,#fe464d 25%,#ff3b5e 48%,#ff4c36 71%,#ff5d1c 86%,#fe6d0c 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fe3b61', endColorstr='#fe6d0c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */;\npadding: 20px;\n}\n.header-register h3 {\n  color: #fff;\n  font-size: 32px;\n  font-family: initial;\n  font-weight: bold;\n  padding-top: 30px;\n}\n.header-register p {\n    color: #fff;\n  }\n.sign-in {\n    background: #0000000f;\n    padding: 70% 0px;\n    min-height: 70vh;\n  }\n.sign-in h4 {    \n    font-size: 20px;\n    font-weight: 600;\n    text-align: center;\n  }\n.sign-in button {    \n    background: #ec544b;\n    border: none;\n    color: #fff;\n    font-size: 20px;\n    padding: 0 20px;\n    margin: auto;\n    display: block;\n  }\n.form-right form {    \n  padding: 20px;\n  border: 2px solid #000;\n  margin: 20px;\n  margin-left: 0;\n background: #fff;\n padding-top: 30px;\n}\n.form-right form  input {    \n  border: 2px solid #dc5e7d;\n  border-radius: 0;\n}\n.form-right form  input::-webkit-input-placeholder {color: #000;font-size: 18px;font-weight: bold;}\n.form-right form  input::-moz-placeholder {color: #000;font-size: 18px;font-weight: bold;}\n.form-right form  input::-ms-input-placeholder {color: #000;font-size: 18px;font-weight: bold;}\n.form-right form  input::placeholder {color: #000;font-size: 18px;font-weight: bold;}\n.form-right form  button { background: #ec544b;\n  border: none;\n  color: #fff;\n  font-size: 20px;\n  padding: 4px 80px;\n  margin: auto;\n  border-radius: 0;\n  display: block;}\n/*footer*/\n.londonSemi_footer {\nbackground: #fe3b61; /* Old browsers */ /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */\nbackground: linear-gradient(45deg,  #fe3b61 0%,#fe464d 25%,#ff3b5e 48%,#ff4c36 71%,#ff5d1c 86%,#fe6d0c 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fe3b61', endColorstr='#fe6d0c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */\n\n}\n.setFooter {text-align: right;\n    padding:7px 80px;}\n.setFooter img {    width: 3%;\n    margin: 0 5px;\n}\n.setFooter span {    color: #fff;\n    font-size: 24px;}\n/*seriesOne END*/\n/*RESPONSIVE START*/\n@media and (min-width: 1030px) and (max-width: 1366px){}\n/*Tablet landscape (1024x768)*/\n@media (min-width:992px) and (max-width:1199px){\n   .wrapper {width: 90%;margin-top: 20px;}\n.sign-in {\nbackground: #0000000f;\npadding: 214px 20PX;min-height: auto;}\n.form-right form {margin-left: 20px;}\n.setFooter span {font-size: 20px; }\n.setFooter img {width: 5%;}\n.setFooter {padding: 7px 20px;} \n}\n/*Tablet portrait (768x1024)*/\n@media (min-width:758px) and (max-width:800px){\n   .wrapper {width: 90%;margin-top: 20px;}\n.sign-in {\nbackground: #0000000f;\npadding: 214px 20PX;min-height: auto;}\n.form-right form {margin-left: 20px;}\n.setFooter span {font-size: 20px; }\n.setFooter img {width: 5%;}\n.setFooter {padding: 7px 20px;}\n}\n/*Small tablet landscape (800x600)*/\n@media (min-width:790px) and (max-width:900px){\n   .wrapper {width: 90%;margin-top: 20px;}\n.sign-in {\nbackground: #0000000f;\npadding: 214px 20PX;}\n.form-right form {margin-left: 20px;}\n.setFooter span {font-size: 20px; }\n.setFooter img {width: 5%;}\n.setFooter {padding: 7px 20px;}\n}\n/*Small tablet portrait (600x800)*/\n@media (min-width:590px) and (max-width:767px){\n  .wrapper {width: 90%;margin-top: 20px;}\n.sign-in {\nbackground: #0000000f;\npadding: 214px 20PX;}\n.form-right form {margin-left: 20px;}\n.setFooter span {font-size: 20px; }\n.setFooter img {width: 5%;}\n.setFooter {padding: 7px 20px;}\n}\n@media screen and (max-width: 500px){\n.wrapper {width: 90%;margin-top: 20px;}\n.sign-in {\nbackground: #0000000f;\npadding: 20px;\nmin-height: auto;}\n.form-right form {margin-left: 20px;}\n.setFooter span {font-size: 20px; }\n.setFooter img {width: 7%;}\n.setFooter {padding: 7px 20px;}\n}\n/*Mobile portrait (320x480)*/\n@media (min-width:310px) and (max-width:470px){\n  .form-right form button {padding: 5px 20px;}\n}\n/*Mobile  landscape (480x320)*/\n@media (min-width:470px) and (max-width:580px){}\n@media screen and (min-width: 1400px) {.sign-in{ padding:65% 0px;}}\n@media screen and (min-width: 1600px) {.sign-in{ padding:56% 0px;}}\n@media screen and (min-width: 1900px) {.sign-in{ padding:47% 0px;}}\n@media (min-width: 992px) {}\n@media (min-width: 1200px){.sign-in{ padding:76% 0px;}}\n/*RESPONSIVE END*/\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0cmF0aW9uLXBhZ2UvcmVnaXN0cmF0aW9uLXBhZ2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFVBQVU7SUFDVixZQUFZO0lBQ1osZ0JBQWdCO0FBQ3BCO0FBQ0E7QUFDQSxtQkFBbUIsRUFBRSxpQkFBaUIsRUFDa0YsYUFBYSxFQUNmLDRCQUE0QjtBQUNsSiw0R0FBNEcsRUFBRSxxREFBcUQ7QUFDbkssbUhBQW1ILEVBQUUsMENBQTBDO0FBQy9KLGFBQWE7QUFDYjtBQUNBO0VBQ0UsV0FBVztFQUNYLGVBQWU7RUFDZixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtBQUNuQjtBQUNFO0lBQ0UsV0FBVztFQUNiO0FBRUE7SUFDRSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtFQUNsQjtBQUNBO0lBQ0UsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0I7RUFDcEI7QUFDQTtJQUNFLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osV0FBVztJQUNYLGVBQWU7SUFDZixlQUFlO0lBQ2YsWUFBWTtJQUNaLGNBQWM7RUFDaEI7QUFFRjtFQUNFLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLGNBQWM7Q0FDZixnQkFBZ0I7Q0FDaEIsaUJBQWlCO0FBQ2xCO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsZ0JBQWdCO0FBQ2xCO0FBQ0Esb0RBQXNDLFdBQVcsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUM7QUFBcEYsMkNBQXNDLFdBQVcsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUM7QUFBcEYsZ0RBQXNDLFdBQVcsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUM7QUFBcEYsc0NBQXNDLFdBQVcsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUM7QUFDcEYsMkJBQTJCLG1CQUFtQjtFQUM1QyxZQUFZO0VBQ1osV0FBVztFQUNYLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixjQUFjLENBQUM7QUFFZixTQUFTO0FBQ1g7QUFDQSxtQkFBbUIsRUFBRSxpQkFBaUIsRUFDa0YsYUFBYSxFQUNmLDRCQUE0QjtBQUNsSiw0R0FBNEcsRUFBRSxxREFBcUQ7QUFDbkssbUhBQW1ILEVBQUUsMENBQTBDOztBQUUvSjtBQUNBLFlBQVksaUJBQWlCO0lBQ3pCLGdCQUFnQixDQUFDO0FBQ3JCLG9CQUFvQixTQUFTO0lBQ3pCLGFBQWE7QUFDakI7QUFDQSxxQkFBcUIsV0FBVztJQUM1QixlQUFlLENBQUM7QUFDcEIsZ0JBQWdCO0FBRWhCLG1CQUFtQjtBQUNuQix1REFBdUQ7QUFDdkQsOEJBQThCO0FBQzlCO0dBQ0csVUFBVSxVQUFVLENBQUMsZ0JBQWdCLENBQUM7QUFDekM7QUFDQSxxQkFBcUI7QUFDckIsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUM7QUFDckMsa0JBQWtCLGlCQUFpQixDQUFDO0FBQ3BDLGlCQUFpQixlQUFlLEVBQUU7QUFDbEMsZ0JBQWdCLFNBQVMsQ0FBQztBQUMxQixZQUFZLGlCQUFpQixDQUFDO0FBQzlCO0FBQ0EsNkJBQTZCO0FBQzdCO0dBQ0csVUFBVSxVQUFVLENBQUMsZ0JBQWdCLENBQUM7QUFDekM7QUFDQSxxQkFBcUI7QUFDckIsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUM7QUFDckMsa0JBQWtCLGlCQUFpQixDQUFDO0FBQ3BDLGlCQUFpQixlQUFlLEVBQUU7QUFDbEMsZ0JBQWdCLFNBQVMsQ0FBQztBQUMxQixZQUFZLGlCQUFpQixDQUFDO0FBQzlCO0FBQ0EsbUNBQW1DO0FBQ25DO0dBQ0csVUFBVSxVQUFVLENBQUMsZ0JBQWdCLENBQUM7QUFDekM7QUFDQSxxQkFBcUI7QUFDckIsbUJBQW1CLENBQUM7QUFDcEIsa0JBQWtCLGlCQUFpQixDQUFDO0FBQ3BDLGlCQUFpQixlQUFlLEVBQUU7QUFDbEMsZ0JBQWdCLFNBQVMsQ0FBQztBQUMxQixZQUFZLGlCQUFpQixDQUFDO0FBQzlCO0FBQ0Esa0NBQWtDO0FBQ2xDO0VBQ0UsVUFBVSxVQUFVLENBQUMsZ0JBQWdCLENBQUM7QUFDeEM7QUFDQSxxQkFBcUI7QUFDckIsbUJBQW1CLENBQUM7QUFDcEIsa0JBQWtCLGlCQUFpQixDQUFDO0FBQ3BDLGlCQUFpQixlQUFlLEVBQUU7QUFDbEMsZ0JBQWdCLFNBQVMsQ0FBQztBQUMxQixZQUFZLGlCQUFpQixDQUFDO0FBQzlCO0FBQ0E7QUFDQSxVQUFVLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQztBQUN0QztBQUNBLHFCQUFxQjtBQUNyQixhQUFhO0FBQ2IsZ0JBQWdCLENBQUM7QUFDakIsa0JBQWtCLGlCQUFpQixDQUFDO0FBQ3BDLGlCQUFpQixlQUFlLEVBQUU7QUFDbEMsZ0JBQWdCLFNBQVMsQ0FBQztBQUMxQixZQUFZLGlCQUFpQixDQUFDO0FBQzlCO0FBQ0MsNEJBQTRCO0FBQzdCO0VBQ0UseUJBQXlCLGlCQUFpQixDQUFDO0FBQzdDO0FBQ0EsOEJBQThCO0FBQzlCLCtDQUErQztBQUMvQyx1Q0FBdUMsVUFBVSxlQUFlLENBQUMsQ0FBQztBQUNsRSx1Q0FBdUMsVUFBVSxlQUFlLENBQUMsQ0FBQztBQUNsRSx1Q0FBdUMsVUFBVSxlQUFlLENBQUMsQ0FBQztBQUNsRSwyQkFBMkI7QUFDM0IsMkJBQTJCLFVBQVUsZUFBZSxDQUFDLENBQUM7QUFDdEQsaUJBQWlCIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0cmF0aW9uLXBhZ2UvcmVnaXN0cmF0aW9uLXBhZ2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwcGVyIHtcbiAgICB3aWR0aDogNjAlO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuLmhlYWRlci1yZWdpc3RlciB7XG5iYWNrZ3JvdW5kOiAjZmUzYjYxOyAvKiBPbGQgYnJvd3NlcnMgKi9cbmJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAgI2ZlM2I2MSAwJSwgI2ZlNDY0ZCAyNSUsICNmZjNiNWUgNDglLCAjZmY0YzM2IDcxJSwgI2ZmNWQxYyA4NiUsICNmZTZkMGMgMTAwJSk7IC8qIEZGMy42LTE1ICovXG5iYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCg0NWRlZywgICNmZTNiNjEgMCUsI2ZlNDY0ZCAyNSUsI2ZmM2I1ZSA0OCUsI2ZmNGMzNiA3MSUsI2ZmNWQxYyA4NiUsI2ZlNmQwYyAxMDAlKTsgLyogQ2hyb21lMTAtMjUsU2FmYXJpNS4xLTYgKi9cbmJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgICNmZTNiNjEgMCUsI2ZlNDY0ZCAyNSUsI2ZmM2I1ZSA0OCUsI2ZmNGMzNiA3MSUsI2ZmNWQxYyA4NiUsI2ZlNmQwYyAxMDAlKTsgLyogVzNDLCBJRTEwKywgRkYxNissIENocm9tZTI2KywgT3BlcmExMissIFNhZmFyaTcrICovXG5maWx0ZXI6IHByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5ncmFkaWVudCggc3RhcnRDb2xvcnN0cj0nI2ZlM2I2MScsIGVuZENvbG9yc3RyPScjZmU2ZDBjJyxHcmFkaWVudFR5cGU9MSApOyAvKiBJRTYtOSBmYWxsYmFjayBvbiBob3Jpem9udGFsIGdyYWRpZW50ICovO1xucGFkZGluZzogMjBweDtcbn1cbi5oZWFkZXItcmVnaXN0ZXIgaDMge1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAzMnB4O1xuICBmb250LWZhbWlseTogaW5pdGlhbDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmctdG9wOiAzMHB4O1xufVxuICAuaGVhZGVyLXJlZ2lzdGVyIHAge1xuICAgIGNvbG9yOiAjZmZmO1xuICB9XG5cbiAgLnNpZ24taW4ge1xuICAgIGJhY2tncm91bmQ6ICMwMDAwMDAwZjtcbiAgICBwYWRkaW5nOiA3MCUgMHB4O1xuICAgIG1pbi1oZWlnaHQ6IDcwdmg7XG4gIH1cbiAgLnNpZ24taW4gaDQgeyAgICBcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgLnNpZ24taW4gYnV0dG9uIHsgICAgXG4gICAgYmFja2dyb3VuZDogI2VjNTQ0YjtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIHBhZGRpbmc6IDAgMjBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuLmZvcm0tcmlnaHQgZm9ybSB7ICAgIFxuICBwYWRkaW5nOiAyMHB4O1xuICBib3JkZXI6IDJweCBzb2xpZCAjMDAwO1xuICBtYXJnaW46IDIwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwO1xuIGJhY2tncm91bmQ6ICNmZmY7XG4gcGFkZGluZy10b3A6IDMwcHg7XG59ICAgIFxuLmZvcm0tcmlnaHQgZm9ybSAgaW5wdXQgeyAgICBcbiAgYm9yZGVyOiAycHggc29saWQgI2RjNWU3ZDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbn1cbi5mb3JtLXJpZ2h0IGZvcm0gIGlucHV0OjpwbGFjZWhvbGRlciB7Y29sb3I6ICMwMDA7Zm9udC1zaXplOiAxOHB4O2ZvbnQtd2VpZ2h0OiBib2xkO31cbi5mb3JtLXJpZ2h0IGZvcm0gIGJ1dHRvbiB7IGJhY2tncm91bmQ6ICNlYzU0NGI7XG4gIGJvcmRlcjogbm9uZTtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcGFkZGluZzogNHB4IDgwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgZGlzcGxheTogYmxvY2s7fVxuXG4gIC8qZm9vdGVyKi9cbi5sb25kb25TZW1pX2Zvb3RlciB7XG5iYWNrZ3JvdW5kOiAjZmUzYjYxOyAvKiBPbGQgYnJvd3NlcnMgKi9cbmJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAgI2ZlM2I2MSAwJSwgI2ZlNDY0ZCAyNSUsICNmZjNiNWUgNDglLCAjZmY0YzM2IDcxJSwgI2ZmNWQxYyA4NiUsICNmZTZkMGMgMTAwJSk7IC8qIEZGMy42LTE1ICovXG5iYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCg0NWRlZywgICNmZTNiNjEgMCUsI2ZlNDY0ZCAyNSUsI2ZmM2I1ZSA0OCUsI2ZmNGMzNiA3MSUsI2ZmNWQxYyA4NiUsI2ZlNmQwYyAxMDAlKTsgLyogQ2hyb21lMTAtMjUsU2FmYXJpNS4xLTYgKi9cbmJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgICNmZTNiNjEgMCUsI2ZlNDY0ZCAyNSUsI2ZmM2I1ZSA0OCUsI2ZmNGMzNiA3MSUsI2ZmNWQxYyA4NiUsI2ZlNmQwYyAxMDAlKTsgLyogVzNDLCBJRTEwKywgRkYxNissIENocm9tZTI2KywgT3BlcmExMissIFNhZmFyaTcrICovXG5maWx0ZXI6IHByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5ncmFkaWVudCggc3RhcnRDb2xvcnN0cj0nI2ZlM2I2MScsIGVuZENvbG9yc3RyPScjZmU2ZDBjJyxHcmFkaWVudFR5cGU9MSApOyAvKiBJRTYtOSBmYWxsYmFjayBvbiBob3Jpem9udGFsIGdyYWRpZW50ICovXG5cbn1cbi5zZXRGb290ZXIge3RleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmc6N3B4IDgwcHg7fVxuLnNldEZvb3RlciBpbWcgeyAgICB3aWR0aDogMyU7XG4gICAgbWFyZ2luOiAwIDVweDtcbn1cbi5zZXRGb290ZXIgc3BhbiB7ICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMjRweDt9XG4vKnNlcmllc09uZSBFTkQqL1xuXG4vKlJFU1BPTlNJVkUgU1RBUlQqL1xuQG1lZGlhIGFuZCAobWluLXdpZHRoOiAxMDMwcHgpIGFuZCAobWF4LXdpZHRoOiAxMzY2cHgpe31cbi8qVGFibGV0IGxhbmRzY2FwZSAoMTAyNHg3NjgpKi9cbkBtZWRpYSAobWluLXdpZHRoOjk5MnB4KSBhbmQgKG1heC13aWR0aDoxMTk5cHgpe1xuICAgLndyYXBwZXIge3dpZHRoOiA5MCU7bWFyZ2luLXRvcDogMjBweDt9XG4uc2lnbi1pbiB7XG5iYWNrZ3JvdW5kOiAjMDAwMDAwMGY7XG5wYWRkaW5nOiAyMTRweCAyMFBYO21pbi1oZWlnaHQ6IGF1dG87fVxuLmZvcm0tcmlnaHQgZm9ybSB7bWFyZ2luLWxlZnQ6IDIwcHg7fVxuLnNldEZvb3RlciBzcGFuIHtmb250LXNpemU6IDIwcHg7IH1cbi5zZXRGb290ZXIgaW1nIHt3aWR0aDogNSU7fVxuLnNldEZvb3RlciB7cGFkZGluZzogN3B4IDIwcHg7fSBcbn1cbi8qVGFibGV0IHBvcnRyYWl0ICg3Njh4MTAyNCkqL1xuQG1lZGlhIChtaW4td2lkdGg6NzU4cHgpIGFuZCAobWF4LXdpZHRoOjgwMHB4KXtcbiAgIC53cmFwcGVyIHt3aWR0aDogOTAlO21hcmdpbi10b3A6IDIwcHg7fVxuLnNpZ24taW4ge1xuYmFja2dyb3VuZDogIzAwMDAwMDBmO1xucGFkZGluZzogMjE0cHggMjBQWDttaW4taGVpZ2h0OiBhdXRvO31cbi5mb3JtLXJpZ2h0IGZvcm0ge21hcmdpbi1sZWZ0OiAyMHB4O31cbi5zZXRGb290ZXIgc3BhbiB7Zm9udC1zaXplOiAyMHB4OyB9XG4uc2V0Rm9vdGVyIGltZyB7d2lkdGg6IDUlO31cbi5zZXRGb290ZXIge3BhZGRpbmc6IDdweCAyMHB4O31cbn1cbi8qU21hbGwgdGFibGV0IGxhbmRzY2FwZSAoODAweDYwMCkqL1xuQG1lZGlhIChtaW4td2lkdGg6NzkwcHgpIGFuZCAobWF4LXdpZHRoOjkwMHB4KXtcbiAgIC53cmFwcGVyIHt3aWR0aDogOTAlO21hcmdpbi10b3A6IDIwcHg7fVxuLnNpZ24taW4ge1xuYmFja2dyb3VuZDogIzAwMDAwMDBmO1xucGFkZGluZzogMjE0cHggMjBQWDt9XG4uZm9ybS1yaWdodCBmb3JtIHttYXJnaW4tbGVmdDogMjBweDt9XG4uc2V0Rm9vdGVyIHNwYW4ge2ZvbnQtc2l6ZTogMjBweDsgfVxuLnNldEZvb3RlciBpbWcge3dpZHRoOiA1JTt9XG4uc2V0Rm9vdGVyIHtwYWRkaW5nOiA3cHggMjBweDt9XG59XG4vKlNtYWxsIHRhYmxldCBwb3J0cmFpdCAoNjAweDgwMCkqL1xuQG1lZGlhIChtaW4td2lkdGg6NTkwcHgpIGFuZCAobWF4LXdpZHRoOjc2N3B4KXtcbiAgLndyYXBwZXIge3dpZHRoOiA5MCU7bWFyZ2luLXRvcDogMjBweDt9XG4uc2lnbi1pbiB7XG5iYWNrZ3JvdW5kOiAjMDAwMDAwMGY7XG5wYWRkaW5nOiAyMTRweCAyMFBYO31cbi5mb3JtLXJpZ2h0IGZvcm0ge21hcmdpbi1sZWZ0OiAyMHB4O31cbi5zZXRGb290ZXIgc3BhbiB7Zm9udC1zaXplOiAyMHB4OyB9XG4uc2V0Rm9vdGVyIGltZyB7d2lkdGg6IDUlO31cbi5zZXRGb290ZXIge3BhZGRpbmc6IDdweCAyMHB4O31cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KXtcbi53cmFwcGVyIHt3aWR0aDogOTAlO21hcmdpbi10b3A6IDIwcHg7fVxuLnNpZ24taW4ge1xuYmFja2dyb3VuZDogIzAwMDAwMDBmO1xucGFkZGluZzogMjBweDtcbm1pbi1oZWlnaHQ6IGF1dG87fVxuLmZvcm0tcmlnaHQgZm9ybSB7bWFyZ2luLWxlZnQ6IDIwcHg7fVxuLnNldEZvb3RlciBzcGFuIHtmb250LXNpemU6IDIwcHg7IH1cbi5zZXRGb290ZXIgaW1nIHt3aWR0aDogNyU7fVxuLnNldEZvb3RlciB7cGFkZGluZzogN3B4IDIwcHg7fVxufVxuIC8qTW9iaWxlIHBvcnRyYWl0ICgzMjB4NDgwKSovXG5AbWVkaWEgKG1pbi13aWR0aDozMTBweCkgYW5kIChtYXgtd2lkdGg6NDcwcHgpe1xuICAuZm9ybS1yaWdodCBmb3JtIGJ1dHRvbiB7cGFkZGluZzogNXB4IDIwcHg7fVxufVxuLypNb2JpbGUgIGxhbmRzY2FwZSAoNDgweDMyMCkqL1xuQG1lZGlhIChtaW4td2lkdGg6NDcwcHgpIGFuZCAobWF4LXdpZHRoOjU4MHB4KXt9XG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxNDAwcHgpIHsuc2lnbi1pbnsgcGFkZGluZzo2NSUgMHB4O319XG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxNjAwcHgpIHsuc2lnbi1pbnsgcGFkZGluZzo1NiUgMHB4O319XG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxOTAwcHgpIHsuc2lnbi1pbnsgcGFkZGluZzo0NyUgMHB4O319XG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHt9XG5AbWVkaWEgKG1pbi13aWR0aDogMTIwMHB4KXsuc2lnbi1pbnsgcGFkZGluZzo3NiUgMHB4O319XG4vKlJFU1BPTlNJVkUgRU5EKi9cblxuIl19 */");

/***/ }),

/***/ "./src/app/registration-page/registration-page.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/registration-page/registration-page.component.ts ***!
  \******************************************************************/
/*! exports provided: RegistrationPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationPageComponent", function() { return RegistrationPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let RegistrationPageComponent = class RegistrationPageComponent {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        this.formSignUp();
        window.scrollTo(0, 0);
    }
    formSignUp() {
        this.registrationForm = this.fb.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^\w+[\w-\.]\@\w+((-\w+)|(\w))\.[a-z]{2,3}$/)]],
            phoneNumber: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            address1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            address2: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            pinCode: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    onSubmit() {
        console.log(this.registrationForm.value);
    }
};
RegistrationPageComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
RegistrationPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-registration-page',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registration-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration-page/registration-page.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registration-page.component.css */ "./src/app/registration-page/registration-page.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], RegistrationPageComponent);



/***/ }),

/***/ "./src/app/seminar-i/seminar-i.component.css":
/*!***************************************************!*\
  !*** ./src/app/seminar-i/seminar-i.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif; }\n.senimarSpacing {padding: 40px 70px;}\n.prof-div img {width: 100%;}\n.intro {padding-top: 40px;}\n.intro h2 {    \n  letter-spacing: 1.72px;\n  color: #FE4644;\n  opacity: 1;\n}\n.intro h1 {    \n    text-align: left;\n    font: 278px;\n    letter-spacing: 1.63px;\n    color: #070707;\n    font-size: 42px;margin-top: 20px;\n    margin-bottom: 50px;\n  }\n.intro button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    border-radius: 54px;\n    padding: 10px 30px;\n    color: #fff;\n    border: none;\n    font-size: 24px;\n    font-weight: 500;}\n/*.middle-wraper {margin-top: 50px;}    */\n.content p {letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500;}\n.watch-area button {background: #D2AB2A 0% 0% no-repeat padding-box;\n    border: 1px solid #707070;\n    opacity: 1;\n    width: 100%;\n    padding: 8px 0px;\n    color: #fff;\n    font-size: 22px;}\n.next-seminar p{ color: #FE3B61;\nfont-weight: 500;\nfont-size: 22px;\ntext-align: center;\npadding-top: 20px;}\n.next-seminar img {width: 60%;\nmargin: auto;\ndisplay: block;}\n.next-seminar h3{text-align: center;\nletter-spacing: 1.27px;\ncolor: #0A0A0A;\nopacity: 1;    font-size: 16px;\npadding-top: 14px;}\n.next-seminar h4 {letter-spacing: 2.27px;\n    color: #FF4C36;\n    opacity: 1;\n    font-size: 16px;\n    text-align: center;\n    font-weight: 600;\n    margin-bottom: 0;}\n.next-seminar hr {background: #0000002e;\n    border: none;\n    margin-top: 5px;height: 2px;\n    width: 60%;\n    margin-bottom: 5px;}\n.title {letter-spacing: 2.63px;\n    color: #FE3B61;\n    opacity: 1;\n    text-align: center;\n    width: 100%;\n    font-size: 36px;}\n/*.chairperson-main {padding-top: 60px;}*/\n.chairperson-info img {width: 120px;\n    margin: auto;\n    display: block;\n    padding-bottom: 10px;}\n.chairperson-info h2 {    letter-spacing: 2.63px;\n    color: #070707;\n    opacity: 1;\n    font-size: 28px;\n    text-align: center;}\n.chairperson-info p {letter-spacing: 1.52px;\n    color: #393737;\n    opacity: 1;\n    font-weight: 600;\n    font-size: 18px;\n    width: 90%;\n    text-align: center;\n    margin: auto;}\n.footer {    \n    background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    opacity: 1;\n    height: 60px;\n}\n.seminarBackgrund {\n  background-image:url(/assets/images/contentSection.png);\n    background-size: contain;\n    padding: 0 !important;\n  }\n@media screen and (max-width: 500px) {\n    .wraper {\n      padding: 10px 10px 10px 10px;\n    }\n    .senimarSpacing {padding: 0;}\n    .intro {\n      padding-top: 10px;\n      text-align: center;\n    }\n  \n    .intro h1 {\n      font-size: 34px;\n      text-align: center;\n      margin-top: 0;\n      margin-bottom: 20px;\n    }\n  \n    .intro h1 {\n      font-size: 24px;\n    }\n  \n    .intro button {\n      padding: 5px 27px;\n      color: #fff;\n      border: none;\n      font-size: 20px;\n      font-weight: 500;\n      width: 100%;\n    }\n  \n    .middle-wraper {\n      margin-top: 20px;\n      text-align: -webkit-center;\n    }\n  \n    .chairperson-main {\n      padding-top: 20px;\n      background: beige;\n      margin-top: 20px;\n    }\n  \n    .title {\n      font-size: 26px;\n    }\n  \n    .chairperson-info h2 {\n      font-size: 22px;\n    }\n  \n    .chairperson-info p {\n      font-size: 15px;\n      width: 90%;\n    }\n  \n    .chairperson-info {\n      margin-bottom: 20px;\n    }\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VtaW5hci1pL3NlbWluYXItaS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFNBQVMsaUNBQWlDLEVBQUU7QUFDNUMsaUJBQWlCLGtCQUFrQixDQUFDO0FBQ3BDLGVBQWUsV0FBVyxDQUFDO0FBQzNCLFFBQVEsaUJBQWlCLENBQUM7QUFDMUI7RUFDRSxzQkFBc0I7RUFDdEIsY0FBYztFQUNkLFVBQVU7QUFDWjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxzQkFBc0I7SUFDdEIsY0FBYztJQUNkLGVBQWUsQ0FBQyxnQkFBZ0I7SUFDaEMsbUJBQW1CO0VBQ3JCO0FBRUYsZUFBZSx3SkFBd0o7SUFDbkssbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGVBQWU7SUFDZixnQkFBZ0IsQ0FBQztBQUdyQix5Q0FBeUM7QUFDekMsWUFBWSxzQkFBc0I7SUFDOUIsY0FBYztJQUNkLFVBQVUsQ0FBQyxpQkFBaUI7SUFDNUIsZ0JBQWdCLENBQUM7QUFDckIsb0JBQW9CLCtDQUErQztJQUMvRCx5QkFBeUI7SUFDekIsVUFBVTtJQUNWLFdBQVc7SUFDWCxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGVBQWUsQ0FBQztBQUVwQixpQkFBaUIsY0FBYztBQUMvQixnQkFBZ0I7QUFDaEIsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixpQkFBaUIsQ0FBQztBQUVsQixtQkFBbUIsVUFBVTtBQUM3QixZQUFZO0FBQ1osY0FBYyxDQUFDO0FBRWYsaUJBQWlCLGtCQUFrQjtBQUNuQyxzQkFBc0I7QUFDdEIsY0FBYztBQUNkLFVBQVUsS0FBSyxlQUFlO0FBQzlCLGlCQUFpQixDQUFDO0FBRWxCLGtCQUFrQixzQkFBc0I7SUFDcEMsY0FBYztJQUNkLFVBQVU7SUFDVixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixnQkFBZ0IsQ0FBQztBQUNyQixrQkFBa0IscUJBQXFCO0lBQ25DLFlBQVk7SUFDWixlQUFlLENBQUMsV0FBVztJQUMzQixVQUFVO0lBQ1Ysa0JBQWtCLENBQUM7QUFFdkIsUUFBUSxzQkFBc0I7SUFDMUIsY0FBYztJQUNkLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGVBQWUsQ0FBQztBQUVwQix5Q0FBeUM7QUFDekMsdUJBQXVCLFlBQVk7SUFDL0IsWUFBWTtJQUNaLGNBQWM7SUFDZCxvQkFBb0IsQ0FBQztBQUN6QiwwQkFBMEIsc0JBQXNCO0lBQzVDLGNBQWM7SUFDZCxVQUFVO0lBQ1YsZUFBZTtJQUNmLGtCQUFrQixDQUFDO0FBQ3ZCLHFCQUFxQixzQkFBc0I7SUFDdkMsY0FBYztJQUNkLFVBQVU7SUFDVixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsWUFBWSxDQUFDO0FBRWpCO0lBQ0ksd0pBQXdKO0lBQ3hKLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7RUFDRSx1REFBdUQ7SUFDckQsd0JBQXdCO0lBQ3hCLHFCQUFxQjtFQUN2QjtBQUdGO0lBQ0k7TUFDRSw0QkFBNEI7SUFDOUI7SUFDQSxpQkFBaUIsVUFBVSxDQUFDO0lBQzVCO01BQ0UsaUJBQWlCO01BQ2pCLGtCQUFrQjtJQUNwQjs7SUFFQTtNQUNFLGVBQWU7TUFDZixrQkFBa0I7TUFDbEIsYUFBYTtNQUNiLG1CQUFtQjtJQUNyQjs7SUFFQTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxpQkFBaUI7TUFDakIsV0FBVztNQUNYLFlBQVk7TUFDWixlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLFdBQVc7SUFDYjs7SUFFQTtNQUNFLGdCQUFnQjtNQUNoQiwwQkFBMEI7SUFDNUI7O0lBRUE7TUFDRSxpQkFBaUI7TUFDakIsaUJBQWlCO01BQ2pCLGdCQUFnQjtJQUNsQjs7SUFFQTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxlQUFlO0lBQ2pCOztJQUVBO01BQ0UsZUFBZTtNQUNmLFVBQVU7SUFDWjs7SUFFQTtNQUNFLG1CQUFtQjtJQUNyQjtFQUNGIiwiZmlsZSI6InNyYy9hcHAvc2VtaW5hci1pL3NlbWluYXItaS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndyYXBlciB7Zm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmOyB9XG4uc2VuaW1hclNwYWNpbmcge3BhZGRpbmc6IDQwcHggNzBweDt9XG4ucHJvZi1kaXYgaW1nIHt3aWR0aDogMTAwJTt9XG4uaW50cm8ge3BhZGRpbmctdG9wOiA0MHB4O31cbi5pbnRybyBoMiB7ICAgIFxuICBsZXR0ZXItc3BhY2luZzogMS43MnB4O1xuICBjb2xvcjogI0ZFNDY0NDtcbiAgb3BhY2l0eTogMTtcbn1cbi5pbnRybyBoMSB7ICAgIFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgZm9udDogMjc4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDEuNjNweDtcbiAgICBjb2xvcjogIzA3MDcwNztcbiAgICBmb250LXNpemU6IDQycHg7bWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xuICB9XG5cbi5pbnRybyBidXR0b24ge2JhY2tncm91bmQ6IHRyYW5zcGFyZW50IGxpbmVhci1ncmFkaWVudCg5MGRlZywgI0ZFM0I2MSAwJSwgI0ZFNDY0RCAxOSUsICNGRjNCNUUgNDAlLCAjRkY0QzM2IDYwJSwgI0ZGNUQxQyA4MSUsICNGRTZEMEMgMTAwJSkgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIGJvcmRlci1yYWRpdXM6IDU0cHg7XG4gICAgcGFkZGluZzogMTBweCAzMHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDt9XG5cblxuLyoubWlkZGxlLXdyYXBlciB7bWFyZ2luLXRvcDogNTBweDt9ICAgICovXG4uY29udGVudCBwIHtsZXR0ZXItc3BhY2luZzogMS4yNXB4O1xuICAgIGNvbG9yOiAjMDQwNDA0O1xuICAgIG9wYWNpdHk6IDE7bGluZS1oZWlnaHQ6IDI4cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDt9ICBcbi53YXRjaC1hcmVhIGJ1dHRvbiB7YmFja2dyb3VuZDogI0QyQUIyQSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzcwNzA3MDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDhweCAwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAyMnB4O30gICAgXG5cbi5uZXh0LXNlbWluYXIgcHsgY29sb3I6ICNGRTNCNjE7XG5mb250LXdlaWdodDogNTAwO1xuZm9udC1zaXplOiAyMnB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xucGFkZGluZy10b3A6IDIwcHg7fVxuXG4ubmV4dC1zZW1pbmFyIGltZyB7d2lkdGg6IDYwJTtcbm1hcmdpbjogYXV0bztcbmRpc3BsYXk6IGJsb2NrO31cblxuLm5leHQtc2VtaW5hciBoM3t0ZXh0LWFsaWduOiBjZW50ZXI7XG5sZXR0ZXItc3BhY2luZzogMS4yN3B4O1xuY29sb3I6ICMwQTBBMEE7XG5vcGFjaXR5OiAxOyAgICBmb250LXNpemU6IDE2cHg7XG5wYWRkaW5nLXRvcDogMTRweDt9ICAgIFxuXG4ubmV4dC1zZW1pbmFyIGg0IHtsZXR0ZXItc3BhY2luZzogMi4yN3B4O1xuICAgIGNvbG9yOiAjRkY0QzM2O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7fVxuLm5leHQtc2VtaW5hciBociB7YmFja2dyb3VuZDogIzAwMDAwMDJlO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBtYXJnaW4tdG9wOiA1cHg7aGVpZ2h0OiAycHg7XG4gICAgd2lkdGg6IDYwJTtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7fVxuXG4udGl0bGUge2xldHRlci1zcGFjaW5nOiAyLjYzcHg7XG4gICAgY29sb3I6ICNGRTNCNjE7XG4gICAgb3BhY2l0eTogMTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAzNnB4O31cblxuLyouY2hhaXJwZXJzb24tbWFpbiB7cGFkZGluZy10b3A6IDYwcHg7fSovXG4uY2hhaXJwZXJzb24taW5mbyBpbWcge3dpZHRoOiAxMjBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7fVxuLmNoYWlycGVyc29uLWluZm8gaDIgeyAgICBsZXR0ZXItc3BhY2luZzogMi42M3B4O1xuICAgIGNvbG9yOiAjMDcwNzA3O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZm9udC1zaXplOiAyOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjt9XG4uY2hhaXJwZXJzb24taW5mbyBwIHtsZXR0ZXItc3BhY2luZzogMS41MnB4O1xuICAgIGNvbG9yOiAjMzkzNzM3O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiBhdXRvO30gICAgXG5cbi5mb290ZXIgeyAgICBcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICNGRTNCNjEgMCUsICNGRTQ2NEQgMTklLCAjRkYzQjVFIDQwJSwgI0ZGNEMzNiA2MCUsICNGRjVEMUMgODElLCAjRkU2RDBDIDEwMCUpIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGhlaWdodDogNjBweDtcbn0gXG4uc2VtaW5hckJhY2tncnVuZCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6dXJsKC9hc3NldHMvaW1hZ2VzL2NvbnRlbnRTZWN0aW9uLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgfSAgXG5cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcbiAgICAud3JhcGVyIHtcbiAgICAgIHBhZGRpbmc6IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgfVxuICAgIC5zZW5pbWFyU3BhY2luZyB7cGFkZGluZzogMDt9XG4gICAgLmludHJvIHtcbiAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgXG4gICAgLmludHJvIGgxIHtcbiAgICAgIGZvbnQtc2l6ZTogMzRweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIG1hcmdpbi10b3A6IDA7XG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgIH1cbiAgXG4gICAgLmludHJvIGgxIHtcbiAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG4gIFxuICAgIC5pbnRybyBidXR0b24ge1xuICAgICAgcGFkZGluZzogNXB4IDI3cHg7XG4gICAgICBjb2xvcjogI2ZmZjtcbiAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG4gIFxuICAgIC5taWRkbGUtd3JhcGVyIHtcbiAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICB9XG4gIFxuICAgIC5jaGFpcnBlcnNvbi1tYWluIHtcbiAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xuICAgICAgYmFja2dyb3VuZDogYmVpZ2U7XG4gICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIH1cbiAgXG4gICAgLnRpdGxlIHtcbiAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICB9XG4gIFxuICAgIC5jaGFpcnBlcnNvbi1pbmZvIGgyIHtcbiAgICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICB9XG4gIFxuICAgIC5jaGFpcnBlcnNvbi1pbmZvIHAge1xuICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgd2lkdGg6IDkwJTtcbiAgICB9XG4gIFxuICAgIC5jaGFpcnBlcnNvbi1pbmZvIHtcbiAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgfVxuICB9Il19 */");

/***/ }),

/***/ "./src/app/seminar-i/seminar-i.component.ts":
/*!**************************************************!*\
  !*** ./src/app/seminar-i/seminar-i.component.ts ***!
  \**************************************************/
/*! exports provided: SeminarIComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeminarIComponent", function() { return SeminarIComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SeminarIComponent = class SeminarIComponent {
    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
    }
};
SeminarIComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-seminar-i',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./seminar-i.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-i/seminar-i.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./seminar-i.component.css */ "./src/app/seminar-i/seminar-i.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SeminarIComponent);



/***/ }),

/***/ "./src/app/seminar-ii/seminar-ii.component.css":
/*!*****************************************************!*\
  !*** ./src/app/seminar-ii/seminar-ii.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif; }\n.senimarSpacing {padding: 40px 70px;}\n.prof-div img {width: 100%;}\n.intro {padding-top: 40px;}\n.intro h2 {    \n  letter-spacing: 1.72px;\n  color: #FE4644;\n  opacity: 1;\n}\n.intro h1 {    \n    text-align: left;\n    font: 278px;\n    letter-spacing: 1.63px;\n    color: #070707;\n    font-size: 42px;margin-top: 20px;\n    margin-bottom: 50px;\n  }\n.intro button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    border-radius: 54px;\n    padding: 10px 30px;\n    color: #fff;\n    border: none;\n    font-size: 24px;\n    font-weight: 500;}\n/*.middle-wraper {margin-top: 50px;}    */\n.content p {letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500;}\n.watch-area button {background: #D2AB2A 0% 0% no-repeat padding-box;\n    border: 1px solid #707070;\n    opacity: 1;\n    width: 100%;\n    padding: 8px 0px;\n    color: #fff;\n    font-size: 22px;}\n.next-seminar p{ color: #FE3B61;\nfont-weight: 500;\nfont-size: 22px;\ntext-align: center;\npadding-top: 20px;}\n.next-seminar img {width: 60%;\nmargin: auto;\ndisplay: block;}\n.next-seminar h3{text-align: center;\nletter-spacing: 1.27px;\ncolor: #0A0A0A;\nopacity: 1;    font-size: 16px;\npadding-top: 14px;}\n.next-seminar h4 {letter-spacing: 2.27px;\n    color: #FF4C36;\n    opacity: 1;\n    font-size: 16px;\n    text-align: center;\n    font-weight: 600;\n    margin-bottom: 0;}\n.next-seminar hr {background: #0000002e;\n    border: none;\n    margin-top: 5px;height: 2px;\n    width: 60%;\n    margin-bottom: 5px;}\n.title {letter-spacing: 2.63px;\n    color: #FE3B61;\n    opacity: 1;\n    text-align: center;\n    width: 100%;\n    font-size: 36px;}\n/*.chairperson-main {padding-top: 60px;}*/\n.chairperson-info img {width: 120px;\n    margin: auto;\n    display: block;\n    padding-bottom: 10px;}\n.chairperson-info h2 {    letter-spacing: 2.63px;\n    color: #070707;\n    opacity: 1;\n    font-size: 28px;\n    text-align: center;}\n.chairperson-info p {letter-spacing: 1.52px;\n    color: #393737;\n    opacity: 1;\n    font-weight: 600;\n    font-size: 18px;\n    width: 90%;\n    text-align: center;\n    margin: auto;}\n.footer {    \n    background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    opacity: 1;\n    height: 60px;\n}\n.seminarBackgrund {\n  background-image:url(/assets/images/contentSection.png);\n    background-size: contain;\n    padding: 0 !important;\n  }\n@media screen and (max-width: 500px) {\n    .wraper {\n      padding: 10px 10px 10px 10px;\n    }\n    .senimarSpacing {padding: 0;}\n    .intro {\n      padding-top: 10px;\n      text-align: center;\n    }\n  \n    .intro h1 {\n      font-size: 34px;\n      text-align: center;\n      margin-top: 0;\n      margin-bottom: 20px;\n    }\n  \n    .intro h1 {\n      font-size: 24px;\n    }\n  \n    .intro button {\n      padding: 5px 27px;\n      color: #fff;\n      border: none;\n      font-size: 20px;\n      font-weight: 500;\n      width: 100%;\n    }\n  \n    .middle-wraper {\n      margin-top: 20px;\n      text-align: -webkit-center;\n    }\n  \n    .chairperson-main {\n      padding-top: 20px;\n      background: beige;\n      margin-top: 20px;\n    }\n  \n    .title {\n      font-size: 26px;\n    }\n  \n    .chairperson-info h2 {\n      font-size: 22px;\n    }\n  \n    .chairperson-info p {\n      font-size: 15px;\n      width: 90%;\n    }\n  \n    .chairperson-info {\n      margin-bottom: 20px;\n    }\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VtaW5hci1paS9zZW1pbmFyLWlpLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxpQ0FBaUMsRUFBRTtBQUM1QyxpQkFBaUIsa0JBQWtCLENBQUM7QUFDcEMsZUFBZSxXQUFXLENBQUM7QUFDM0IsUUFBUSxpQkFBaUIsQ0FBQztBQUMxQjtFQUNFLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsVUFBVTtBQUNaO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLHNCQUFzQjtJQUN0QixjQUFjO0lBQ2QsZUFBZSxDQUFDLGdCQUFnQjtJQUNoQyxtQkFBbUI7RUFDckI7QUFFRixlQUFlLHdKQUF3SjtJQUNuSyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGdCQUFnQixDQUFDO0FBR3JCLHlDQUF5QztBQUN6QyxZQUFZLHNCQUFzQjtJQUM5QixjQUFjO0lBQ2QsVUFBVSxDQUFDLGlCQUFpQjtJQUM1QixnQkFBZ0IsQ0FBQztBQUNyQixvQkFBb0IsK0NBQStDO0lBQy9ELHlCQUF5QjtJQUN6QixVQUFVO0lBQ1YsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsZUFBZSxDQUFDO0FBRXBCLGlCQUFpQixjQUFjO0FBQy9CLGdCQUFnQjtBQUNoQixlQUFlO0FBQ2Ysa0JBQWtCO0FBQ2xCLGlCQUFpQixDQUFDO0FBRWxCLG1CQUFtQixVQUFVO0FBQzdCLFlBQVk7QUFDWixjQUFjLENBQUM7QUFFZixpQkFBaUIsa0JBQWtCO0FBQ25DLHNCQUFzQjtBQUN0QixjQUFjO0FBQ2QsVUFBVSxLQUFLLGVBQWU7QUFDOUIsaUJBQWlCLENBQUM7QUFFbEIsa0JBQWtCLHNCQUFzQjtJQUNwQyxjQUFjO0lBQ2QsVUFBVTtJQUNWLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQixDQUFDO0FBQ3JCLGtCQUFrQixxQkFBcUI7SUFDbkMsWUFBWTtJQUNaLGVBQWUsQ0FBQyxXQUFXO0lBQzNCLFVBQVU7SUFDVixrQkFBa0IsQ0FBQztBQUV2QixRQUFRLHNCQUFzQjtJQUMxQixjQUFjO0lBQ2QsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsZUFBZSxDQUFDO0FBRXBCLHlDQUF5QztBQUN6Qyx1QkFBdUIsWUFBWTtJQUMvQixZQUFZO0lBQ1osY0FBYztJQUNkLG9CQUFvQixDQUFDO0FBQ3pCLDBCQUEwQixzQkFBc0I7SUFDNUMsY0FBYztJQUNkLFVBQVU7SUFDVixlQUFlO0lBQ2Ysa0JBQWtCLENBQUM7QUFDdkIscUJBQXFCLHNCQUFzQjtJQUN2QyxjQUFjO0lBQ2QsVUFBVTtJQUNWLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixZQUFZLENBQUM7QUFFakI7SUFDSSx3SkFBd0o7SUFDeEosVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtFQUNFLHVEQUF1RDtJQUNyRCx3QkFBd0I7SUFDeEIscUJBQXFCO0VBQ3ZCO0FBR0Y7SUFDSTtNQUNFLDRCQUE0QjtJQUM5QjtJQUNBLGlCQUFpQixVQUFVLENBQUM7SUFDNUI7TUFDRSxpQkFBaUI7TUFDakIsa0JBQWtCO0lBQ3BCOztJQUVBO01BQ0UsZUFBZTtNQUNmLGtCQUFrQjtNQUNsQixhQUFhO01BQ2IsbUJBQW1CO0lBQ3JCOztJQUVBO01BQ0UsZUFBZTtJQUNqQjs7SUFFQTtNQUNFLGlCQUFpQjtNQUNqQixXQUFXO01BQ1gsWUFBWTtNQUNaLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsV0FBVztJQUNiOztJQUVBO01BQ0UsZ0JBQWdCO01BQ2hCLDBCQUEwQjtJQUM1Qjs7SUFFQTtNQUNFLGlCQUFpQjtNQUNqQixpQkFBaUI7TUFDakIsZ0JBQWdCO0lBQ2xCOztJQUVBO01BQ0UsZUFBZTtJQUNqQjs7SUFFQTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxlQUFlO01BQ2YsVUFBVTtJQUNaOztJQUVBO01BQ0UsbUJBQW1CO0lBQ3JCO0VBQ0YiLCJmaWxlIjoic3JjL2FwcC9zZW1pbmFyLWlpL3NlbWluYXItaWkuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwZXIge2ZvbnQtZmFtaWx5OiAnUm9ib3RvJywgc2Fucy1zZXJpZjsgfVxuLnNlbmltYXJTcGFjaW5nIHtwYWRkaW5nOiA0MHB4IDcwcHg7fVxuLnByb2YtZGl2IGltZyB7d2lkdGg6IDEwMCU7fVxuLmludHJvIHtwYWRkaW5nLXRvcDogNDBweDt9XG4uaW50cm8gaDIgeyAgICBcbiAgbGV0dGVyLXNwYWNpbmc6IDEuNzJweDtcbiAgY29sb3I6ICNGRTQ2NDQ7XG4gIG9wYWNpdHk6IDE7XG59XG4uaW50cm8gaDEgeyAgICBcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGZvbnQ6IDI3OHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjYzcHg7XG4gICAgY29sb3I6ICMwNzA3MDc7XG4gICAgZm9udC1zaXplOiA0MnB4O21hcmdpbi10b3A6IDIwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuXG4uaW50cm8gYnV0dG9uIHtiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICNGRTNCNjEgMCUsICNGRTQ2NEQgMTklLCAjRkYzQjVFIDQwJSwgI0ZGNEMzNiA2MCUsICNGRjVEMUMgODElLCAjRkU2RDBDIDEwMCUpIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbiAgICBib3JkZXItcmFkaXVzOiA1NHB4O1xuICAgIHBhZGRpbmc6IDEwcHggMzBweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7fVxuXG5cbi8qLm1pZGRsZS13cmFwZXIge21hcmdpbi10b3A6IDUwcHg7fSAgICAqL1xuLmNvbnRlbnQgcCB7bGV0dGVyLXNwYWNpbmc6IDEuMjVweDtcbiAgICBjb2xvcjogIzA0MDQwNDtcbiAgICBvcGFjaXR5OiAxO2xpbmUtaGVpZ2h0OiAyOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7fSAgXG4ud2F0Y2gtYXJlYSBidXR0b24ge2JhY2tncm91bmQ6ICNEMkFCMkEgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM3MDcwNzA7XG4gICAgb3BhY2l0eTogMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiA4cHggMHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMjJweDt9ICAgIFxuXG4ubmV4dC1zZW1pbmFyIHB7IGNvbG9yOiAjRkUzQjYxO1xuZm9udC13ZWlnaHQ6IDUwMDtcbmZvbnQtc2l6ZTogMjJweDtcbnRleHQtYWxpZ246IGNlbnRlcjtcbnBhZGRpbmctdG9wOiAyMHB4O31cblxuLm5leHQtc2VtaW5hciBpbWcge3dpZHRoOiA2MCU7XG5tYXJnaW46IGF1dG87XG5kaXNwbGF5OiBibG9jazt9XG5cbi5uZXh0LXNlbWluYXIgaDN7dGV4dC1hbGlnbjogY2VudGVyO1xubGV0dGVyLXNwYWNpbmc6IDEuMjdweDtcbmNvbG9yOiAjMEEwQTBBO1xub3BhY2l0eTogMTsgICAgZm9udC1zaXplOiAxNnB4O1xucGFkZGluZy10b3A6IDE0cHg7fSAgICBcblxuLm5leHQtc2VtaW5hciBoNCB7bGV0dGVyLXNwYWNpbmc6IDIuMjdweDtcbiAgICBjb2xvcjogI0ZGNEMzNjtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO31cbi5uZXh0LXNlbWluYXIgaHIge2JhY2tncm91bmQ6ICMwMDAwMDAyZTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgbWFyZ2luLXRvcDogNXB4O2hlaWdodDogMnB4O1xuICAgIHdpZHRoOiA2MCU7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O31cblxuLnRpdGxlIHtsZXR0ZXItc3BhY2luZzogMi42M3B4O1xuICAgIGNvbG9yOiAjRkUzQjYxO1xuICAgIG9wYWNpdHk6IDE7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMzZweDt9XG5cbi8qLmNoYWlycGVyc29uLW1haW4ge3BhZGRpbmctdG9wOiA2MHB4O30qL1xuLmNoYWlycGVyc29uLWluZm8gaW1nIHt3aWR0aDogMTIwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O31cbi5jaGFpcnBlcnNvbi1pbmZvIGgyIHsgICAgbGV0dGVyLXNwYWNpbmc6IDIuNjNweDtcbiAgICBjb2xvcjogIzA3MDcwNztcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7fVxuLmNoYWlycGVyc29uLWluZm8gcCB7bGV0dGVyLXNwYWNpbmc6IDEuNTJweDtcbiAgICBjb2xvcjogIzM5MzczNztcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogYXV0bzt9ICAgIFxuXG4uZm9vdGVyIHsgICAgXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgbGluZWFyLWdyYWRpZW50KDkwZGVnLCAjRkUzQjYxIDAlLCAjRkU0NjREIDE5JSwgI0ZGM0I1RSA0MCUsICNGRjRDMzYgNjAlLCAjRkY1RDFDIDgxJSwgI0ZFNkQwQyAxMDAlKSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgb3BhY2l0eTogMTtcbiAgICBoZWlnaHQ6IDYwcHg7XG59IFxuLnNlbWluYXJCYWNrZ3J1bmQge1xuICBiYWNrZ3JvdW5kLWltYWdlOnVybCgvYXNzZXRzL2ltYWdlcy9jb250ZW50U2VjdGlvbi5wbmcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gIH0gIFxuXG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KSB7XG4gICAgLndyYXBlciB7XG4gICAgICBwYWRkaW5nOiAxMHB4IDEwcHggMTBweCAxMHB4O1xuICAgIH1cbiAgICAuc2VuaW1hclNwYWNpbmcge3BhZGRpbmc6IDA7fVxuICAgIC5pbnRybyB7XG4gICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIFxuICAgIC5pbnRybyBoMSB7XG4gICAgICBmb250LXNpemU6IDM0cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICB9XG4gIFxuICAgIC5pbnRybyBoMSB7XG4gICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgfVxuICBcbiAgICAuaW50cm8gYnV0dG9uIHtcbiAgICAgIHBhZGRpbmc6IDVweCAyN3B4O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICBib3JkZXI6IG5vbmU7XG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICBcbiAgICAubWlkZGxlLXdyYXBlciB7XG4gICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24tbWFpbiB7XG4gICAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgICAgIGJhY2tncm91bmQ6IGJlaWdlO1xuICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICB9XG4gIFxuICAgIC50aXRsZSB7XG4gICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyBoMiB7XG4gICAgICBmb250LXNpemU6IDIycHg7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyBwIHtcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgIHdpZHRoOiA5MCU7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgIH1cbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/seminar-ii/seminar-ii.component.ts":
/*!****************************************************!*\
  !*** ./src/app/seminar-ii/seminar-ii.component.ts ***!
  \****************************************************/
/*! exports provided: SeminarIIComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeminarIIComponent", function() { return SeminarIIComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SeminarIIComponent = class SeminarIIComponent {
    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
    }
};
SeminarIIComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-seminar-ii',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./seminar-ii.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-ii/seminar-ii.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./seminar-ii.component.css */ "./src/app/seminar-ii/seminar-ii.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SeminarIIComponent);



/***/ }),

/***/ "./src/app/seminar-iii/seminar-iii.component.css":
/*!*******************************************************!*\
  !*** ./src/app/seminar-iii/seminar-iii.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif; }\n.senimarSpacing {padding: 40px 70px;}\n.prof-div img {width: 100%;}\n.intro {padding-top: 40px;}\n.intro h2 {    \n  letter-spacing: 1.72px;\n  color: #FE4644;\n  opacity: 1;\n}\n.intro h1 {    \n    text-align: left;\n    font: 278px;\n    letter-spacing: 1.63px;\n    color: #070707;\n    font-size: 42px;margin-top: 20px;\n    margin-bottom: 50px;\n  }\n.intro button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    border-radius: 54px;\n    padding: 10px 30px;\n    color: #fff;\n    border: none;\n    font-size: 24px;\n    font-weight: 500;}\n/*.middle-wraper {margin-top: 50px;}    */\n.content p {letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500;}\n.watch-area button {background: #D2AB2A 0% 0% no-repeat padding-box;\n    border: 1px solid #707070;\n    opacity: 1;\n    width: 100%;\n    padding: 8px 0px;\n    color: #fff;\n    font-size: 22px;}\n.next-seminar p{ color: #FE3B61;\nfont-weight: 500;\nfont-size: 22px;\ntext-align: center;\npadding-top: 20px;}\n.next-seminar img {width: 60%;\nmargin: auto;\ndisplay: block;}\n.next-seminar h3{text-align: center;\nletter-spacing: 1.27px;\ncolor: #0A0A0A;\nopacity: 1;    font-size: 16px;\npadding-top: 14px;}\n.next-seminar h4 {letter-spacing: 2.27px;\n    color: #FF4C36;\n    opacity: 1;\n    font-size: 16px;\n    text-align: center;\n    font-weight: 600;\n    margin-bottom: 0;}\n.next-seminar hr {background: #0000002e;\n    border: none;\n    margin-top: 5px;height: 2px;\n    width: 60%;\n    margin-bottom: 5px;}\n.title {letter-spacing: 2.63px;\n    color: #FE3B61;\n    opacity: 1;\n    text-align: center;\n    width: 100%;\n    font-size: 36px;}\n/*.chairperson-main {padding-top: 60px;}*/\n.chairperson-info img {width: 120px;\n    margin: auto;\n    display: block;\n    padding-bottom: 10px;}\n.chairperson-info h2 {    letter-spacing: 2.63px;\n    color: #070707;\n    opacity: 1;\n    font-size: 28px;\n    text-align: center;}\n.chairperson-info p {letter-spacing: 1.52px;\n    color: #393737;\n    opacity: 1;\n    font-weight: 600;\n    font-size: 18px;\n    width: 90%;\n    text-align: center;\n    margin: auto;}\n.footer {    \n    background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    opacity: 1;\n    height: 60px;\n}\n.seminarBackgrund {\n  background-image:url(/assets/images/contentSection.png);\n    background-size: contain;\n    padding: 0 !important;\n  }\n@media screen and (max-width: 500px) {\n    .wraper {\n      padding: 10px 10px 10px 10px;\n    }\n    .senimarSpacing {padding: 0;}\n    .intro {\n      padding-top: 10px;\n      text-align: center;\n    }\n  \n    .intro h1 {\n      font-size: 34px;\n      text-align: center;\n      margin-top: 0;\n      margin-bottom: 20px;\n    }\n  \n    .intro h1 {\n      font-size: 24px;\n    }\n  \n    .intro button {\n      padding: 5px 27px;\n      color: #fff;\n      border: none;\n      font-size: 20px;\n      font-weight: 500;\n      width: 100%;\n    }\n  \n    .middle-wraper {\n      margin-top: 20px;\n      text-align: -webkit-center;\n    }\n  \n    .chairperson-main {\n      padding-top: 20px;\n      background: beige;\n      margin-top: 20px;\n    }\n  \n    .title {\n      font-size: 26px;\n    }\n  \n    .chairperson-info h2 {\n      font-size: 22px;\n    }\n  \n    .chairperson-info p {\n      font-size: 15px;\n      width: 90%;\n    }\n  \n    .chairperson-info {\n      margin-bottom: 20px;\n    }\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VtaW5hci1paWkvc2VtaW5hci1paWkuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxTQUFTLGlDQUFpQyxFQUFFO0FBQzVDLGlCQUFpQixrQkFBa0IsQ0FBQztBQUNwQyxlQUFlLFdBQVcsQ0FBQztBQUMzQixRQUFRLGlCQUFpQixDQUFDO0FBQzFCO0VBQ0Usc0JBQXNCO0VBQ3RCLGNBQWM7RUFDZCxVQUFVO0FBQ1o7QUFDQTtJQUNJLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsc0JBQXNCO0lBQ3RCLGNBQWM7SUFDZCxlQUFlLENBQUMsZ0JBQWdCO0lBQ2hDLG1CQUFtQjtFQUNyQjtBQUVGLGVBQWUsd0pBQXdKO0lBQ25LLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFlBQVk7SUFDWixlQUFlO0lBQ2YsZ0JBQWdCLENBQUM7QUFHckIseUNBQXlDO0FBQ3pDLFlBQVksc0JBQXNCO0lBQzlCLGNBQWM7SUFDZCxVQUFVLENBQUMsaUJBQWlCO0lBQzVCLGdCQUFnQixDQUFDO0FBQ3JCLG9CQUFvQiwrQ0FBK0M7SUFDL0QseUJBQXlCO0lBQ3pCLFVBQVU7SUFDVixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxlQUFlLENBQUM7QUFFcEIsaUJBQWlCLGNBQWM7QUFDL0IsZ0JBQWdCO0FBQ2hCLGVBQWU7QUFDZixrQkFBa0I7QUFDbEIsaUJBQWlCLENBQUM7QUFFbEIsbUJBQW1CLFVBQVU7QUFDN0IsWUFBWTtBQUNaLGNBQWMsQ0FBQztBQUVmLGlCQUFpQixrQkFBa0I7QUFDbkMsc0JBQXNCO0FBQ3RCLGNBQWM7QUFDZCxVQUFVLEtBQUssZUFBZTtBQUM5QixpQkFBaUIsQ0FBQztBQUVsQixrQkFBa0Isc0JBQXNCO0lBQ3BDLGNBQWM7SUFDZCxVQUFVO0lBQ1YsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZ0JBQWdCLENBQUM7QUFDckIsa0JBQWtCLHFCQUFxQjtJQUNuQyxZQUFZO0lBQ1osZUFBZSxDQUFDLFdBQVc7SUFDM0IsVUFBVTtJQUNWLGtCQUFrQixDQUFDO0FBRXZCLFFBQVEsc0JBQXNCO0lBQzFCLGNBQWM7SUFDZCxVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxlQUFlLENBQUM7QUFFcEIseUNBQXlDO0FBQ3pDLHVCQUF1QixZQUFZO0lBQy9CLFlBQVk7SUFDWixjQUFjO0lBQ2Qsb0JBQW9CLENBQUM7QUFDekIsMEJBQTBCLHNCQUFzQjtJQUM1QyxjQUFjO0lBQ2QsVUFBVTtJQUNWLGVBQWU7SUFDZixrQkFBa0IsQ0FBQztBQUN2QixxQkFBcUIsc0JBQXNCO0lBQ3ZDLGNBQWM7SUFDZCxVQUFVO0lBQ1YsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLFlBQVksQ0FBQztBQUVqQjtJQUNJLHdKQUF3SjtJQUN4SixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0VBQ0UsdURBQXVEO0lBQ3JELHdCQUF3QjtJQUN4QixxQkFBcUI7RUFDdkI7QUFHRjtJQUNJO01BQ0UsNEJBQTRCO0lBQzlCO0lBQ0EsaUJBQWlCLFVBQVUsQ0FBQztJQUM1QjtNQUNFLGlCQUFpQjtNQUNqQixrQkFBa0I7SUFDcEI7O0lBRUE7TUFDRSxlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLGFBQWE7TUFDYixtQkFBbUI7SUFDckI7O0lBRUE7TUFDRSxlQUFlO0lBQ2pCOztJQUVBO01BQ0UsaUJBQWlCO01BQ2pCLFdBQVc7TUFDWCxZQUFZO01BQ1osZUFBZTtNQUNmLGdCQUFnQjtNQUNoQixXQUFXO0lBQ2I7O0lBRUE7TUFDRSxnQkFBZ0I7TUFDaEIsMEJBQTBCO0lBQzVCOztJQUVBO01BQ0UsaUJBQWlCO01BQ2pCLGlCQUFpQjtNQUNqQixnQkFBZ0I7SUFDbEI7O0lBRUE7TUFDRSxlQUFlO0lBQ2pCOztJQUVBO01BQ0UsZUFBZTtJQUNqQjs7SUFFQTtNQUNFLGVBQWU7TUFDZixVQUFVO0lBQ1o7O0lBRUE7TUFDRSxtQkFBbUI7SUFDckI7RUFDRiIsImZpbGUiOiJzcmMvYXBwL3NlbWluYXItaWlpL3NlbWluYXItaWlpLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcGVyIHtmb250LWZhbWlseTogJ1JvYm90bycsIHNhbnMtc2VyaWY7IH1cbi5zZW5pbWFyU3BhY2luZyB7cGFkZGluZzogNDBweCA3MHB4O31cbi5wcm9mLWRpdiBpbWcge3dpZHRoOiAxMDAlO31cbi5pbnRybyB7cGFkZGluZy10b3A6IDQwcHg7fVxuLmludHJvIGgyIHsgICAgXG4gIGxldHRlci1zcGFjaW5nOiAxLjcycHg7XG4gIGNvbG9yOiAjRkU0NjQ0O1xuICBvcGFjaXR5OiAxO1xufVxuLmludHJvIGgxIHsgICAgXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBmb250OiAyNzhweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMS42M3B4O1xuICAgIGNvbG9yOiAjMDcwNzA3O1xuICAgIGZvbnQtc2l6ZTogNDJweDttYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gIH1cblxuLmludHJvIGJ1dHRvbiB7YmFja2dyb3VuZDogdHJhbnNwYXJlbnQgbGluZWFyLWdyYWRpZW50KDkwZGVnLCAjRkUzQjYxIDAlLCAjRkU0NjREIDE5JSwgI0ZGM0I1RSA0MCUsICNGRjRDMzYgNjAlLCAjRkY1RDFDIDgxJSwgI0ZFNkQwQyAxMDAlKSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgYm9yZGVyLXJhZGl1czogNTRweDtcbiAgICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICBmb250LXdlaWdodDogNTAwO31cblxuXG4vKi5taWRkbGUtd3JhcGVyIHttYXJnaW4tdG9wOiA1MHB4O30gICAgKi9cbi5jb250ZW50IHAge2xldHRlci1zcGFjaW5nOiAxLjI1cHg7XG4gICAgY29sb3I6ICMwNDA0MDQ7XG4gICAgb3BhY2l0eTogMTtsaW5lLWhlaWdodDogMjhweDtcbiAgICBmb250LXdlaWdodDogNTAwO30gIFxuLndhdGNoLWFyZWEgYnV0dG9uIHtiYWNrZ3JvdW5kOiAjRDJBQjJBIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNzA3MDcwO1xuICAgIG9wYWNpdHk6IDE7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogOHB4IDBweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDIycHg7fSAgICBcblxuLm5leHQtc2VtaW5hciBweyBjb2xvcjogI0ZFM0I2MTtcbmZvbnQtd2VpZ2h0OiA1MDA7XG5mb250LXNpemU6IDIycHg7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5wYWRkaW5nLXRvcDogMjBweDt9XG5cbi5uZXh0LXNlbWluYXIgaW1nIHt3aWR0aDogNjAlO1xubWFyZ2luOiBhdXRvO1xuZGlzcGxheTogYmxvY2s7fVxuXG4ubmV4dC1zZW1pbmFyIGgze3RleHQtYWxpZ246IGNlbnRlcjtcbmxldHRlci1zcGFjaW5nOiAxLjI3cHg7XG5jb2xvcjogIzBBMEEwQTtcbm9wYWNpdHk6IDE7ICAgIGZvbnQtc2l6ZTogMTZweDtcbnBhZGRpbmctdG9wOiAxNHB4O30gICAgXG5cbi5uZXh0LXNlbWluYXIgaDQge2xldHRlci1zcGFjaW5nOiAyLjI3cHg7XG4gICAgY29sb3I6ICNGRjRDMzY7XG4gICAgb3BhY2l0eTogMTtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDt9XG4ubmV4dC1zZW1pbmFyIGhyIHtiYWNrZ3JvdW5kOiAjMDAwMDAwMmU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIG1hcmdpbi10b3A6IDVweDtoZWlnaHQ6IDJweDtcbiAgICB3aWR0aDogNjAlO1xuICAgIG1hcmdpbi1ib3R0b206IDVweDt9XG5cbi50aXRsZSB7bGV0dGVyLXNwYWNpbmc6IDIuNjNweDtcbiAgICBjb2xvcjogI0ZFM0I2MTtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDM2cHg7fVxuXG4vKi5jaGFpcnBlcnNvbi1tYWluIHtwYWRkaW5nLXRvcDogNjBweDt9Ki9cbi5jaGFpcnBlcnNvbi1pbmZvIGltZyB7d2lkdGg6IDEyMHB4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDt9XG4uY2hhaXJwZXJzb24taW5mbyBoMiB7ICAgIGxldHRlci1zcGFjaW5nOiAyLjYzcHg7XG4gICAgY29sb3I6ICMwNzA3MDc7XG4gICAgb3BhY2l0eTogMTtcbiAgICBmb250LXNpemU6IDI4cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO31cbi5jaGFpcnBlcnNvbi1pbmZvIHAge2xldHRlci1zcGFjaW5nOiAxLjUycHg7XG4gICAgY29sb3I6ICMzOTM3Mzc7XG4gICAgb3BhY2l0eTogMTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB3aWR0aDogOTAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IGF1dG87fSAgICBcblxuLmZvb3RlciB7ICAgIFxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50IGxpbmVhci1ncmFkaWVudCg5MGRlZywgI0ZFM0I2MSAwJSwgI0ZFNDY0RCAxOSUsICNGRjNCNUUgNDAlLCAjRkY0QzM2IDYwJSwgI0ZGNUQxQyA4MSUsICNGRTZEMEMgMTAwJSkgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgaGVpZ2h0OiA2MHB4O1xufSBcbi5zZW1pbmFyQmFja2dydW5kIHtcbiAgYmFja2dyb3VuZC1pbWFnZTp1cmwoL2Fzc2V0cy9pbWFnZXMvY29udGVudFNlY3Rpb24ucG5nKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xuICB9ICBcblxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xuICAgIC53cmFwZXIge1xuICAgICAgcGFkZGluZzogMTBweCAxMHB4IDEwcHggMTBweDtcbiAgICB9XG4gICAgLnNlbmltYXJTcGFjaW5nIHtwYWRkaW5nOiAwO31cbiAgICAuaW50cm8ge1xuICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxuICBcbiAgICAuaW50cm8gaDEge1xuICAgICAgZm9udC1zaXplOiAzNHB4O1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgfVxuICBcbiAgICAuaW50cm8gaDEge1xuICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgIH1cbiAgXG4gICAgLmludHJvIGJ1dHRvbiB7XG4gICAgICBwYWRkaW5nOiA1cHggMjdweDtcbiAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgXG4gICAgLm1pZGRsZS13cmFwZXIge1xuICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICAgIH1cbiAgXG4gICAgLmNoYWlycGVyc29uLW1haW4ge1xuICAgICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgICBiYWNrZ3JvdW5kOiBiZWlnZTtcbiAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgfVxuICBcbiAgICAudGl0bGUge1xuICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgIH1cbiAgXG4gICAgLmNoYWlycGVyc29uLWluZm8gaDIge1xuICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgIH1cbiAgXG4gICAgLmNoYWlycGVyc29uLWluZm8gcCB7XG4gICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICB3aWR0aDogOTAlO1xuICAgIH1cbiAgXG4gICAgLmNoYWlycGVyc29uLWluZm8ge1xuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICB9XG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/seminar-iii/seminar-iii.component.ts":
/*!******************************************************!*\
  !*** ./src/app/seminar-iii/seminar-iii.component.ts ***!
  \******************************************************/
/*! exports provided: SeminarIIIComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeminarIIIComponent", function() { return SeminarIIIComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SeminarIIIComponent = class SeminarIIIComponent {
    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
    }
};
SeminarIIIComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-seminar-iii',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./seminar-iii.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-iii/seminar-iii.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./seminar-iii.component.css */ "./src/app/seminar-iii/seminar-iii.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SeminarIIIComponent);



/***/ }),

/***/ "./src/app/seminar-iv/seminar-iv.component.css":
/*!*****************************************************!*\
  !*** ./src/app/seminar-iv/seminar-iv.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif; }\n.senimarSpacing {padding: 40px 70px;}\n.prof-div img {width: 100%;}\n.intro {padding-top: 40px;}\n.intro h2 {    \n  letter-spacing: 1.72px;\n  color: #FE4644;\n  opacity: 1;\n}\n.intro h1 {    \n    text-align: left;\n    font: 278px;\n    letter-spacing: 1.63px;\n    color: #070707;\n    font-size: 42px;margin-top: 20px;\n    margin-bottom: 50px;\n  }\n.intro button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    border-radius: 54px;\n    padding: 10px 30px;\n    color: #fff;\n    border: none;\n    font-size: 24px;\n    font-weight: 500;}\n/*.middle-wraper {margin-top: 50px;}    */\n.content p {letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500;}\n.watch-area button {background: #D2AB2A 0% 0% no-repeat padding-box;\n    border: 1px solid #707070;\n    opacity: 1;\n    width: 100%;\n    padding: 8px 0px;\n    color: #fff;\n    font-size: 22px;}\n.next-seminar p{ color: #FE3B61;\nfont-weight: 500;\nfont-size: 22px;\ntext-align: center;\npadding-top: 20px;}\n.next-seminar img {width: 60%;\nmargin: auto;\ndisplay: block;}\n.next-seminar h3{text-align: center;\nletter-spacing: 1.27px;\ncolor: #0A0A0A;\nopacity: 1;    font-size: 16px;\npadding-top: 14px;}\n.next-seminar h4 {letter-spacing: 2.27px;\n    color: #FF4C36;\n    opacity: 1;\n    font-size: 16px;\n    text-align: center;\n    font-weight: 600;\n    margin-bottom: 0;}\n.next-seminar hr {background: #0000002e;\n    border: none;\n    margin-top: 5px;height: 2px;\n    width: 60%;\n    margin-bottom: 5px;}\n.title {letter-spacing: 2.63px;\n    color: #FE3B61;\n    opacity: 1;\n    text-align: center;\n    width: 100%;\n    font-size: 36px;}\n/*.chairperson-main {padding-top: 60px;}*/\n.chairperson-info img {width: 120px;\n    margin: auto;\n    display: block;\n    padding-bottom: 10px;}\n.chairperson-info h2 {    letter-spacing: 2.63px;\n    color: #070707;\n    opacity: 1;\n    font-size: 28px;\n    text-align: center;}\n.chairperson-info p {letter-spacing: 1.52px;\n    color: #393737;\n    opacity: 1;\n    font-weight: 600;\n    font-size: 18px;\n    width: 90%;\n    text-align: center;\n    margin: auto;}\n.footer {    \n    background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    opacity: 1;\n    height: 60px;\n}\n.seminarBackgrund {\n  background-image:url(/assets/images/contentSection.png);\n    background-size: contain;\n    padding: 0 !important;\n  }\n@media screen and (max-width: 500px) {\n    .wraper {\n      padding: 10px 10px 10px 10px;\n    }\n    .senimarSpacing {padding: 0;}\n    .intro {\n      padding-top: 10px;\n      text-align: center;\n    }\n  \n    .intro h1 {\n      font-size: 34px;\n      text-align: center;\n      margin-top: 0;\n      margin-bottom: 20px;\n    }\n  \n    .intro h1 {\n      font-size: 24px;\n    }\n  \n    .intro button {\n      padding: 5px 27px;\n      color: #fff;\n      border: none;\n      font-size: 20px;\n      font-weight: 500;\n      width: 100%;\n    }\n  \n    .middle-wraper {\n      margin-top: 20px;\n      text-align: -webkit-center;\n    }\n  \n    .chairperson-main {\n      padding-top: 20px;\n      background: beige;\n      margin-top: 20px;\n    }\n  \n    .title {\n      font-size: 26px;\n    }\n  \n    .chairperson-info h2 {\n      font-size: 22px;\n    }\n  \n    .chairperson-info p {\n      font-size: 15px;\n      width: 90%;\n    }\n  \n    .chairperson-info {\n      margin-bottom: 20px;\n    }\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VtaW5hci1pdi9zZW1pbmFyLWl2LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxpQ0FBaUMsRUFBRTtBQUM1QyxpQkFBaUIsa0JBQWtCLENBQUM7QUFDcEMsZUFBZSxXQUFXLENBQUM7QUFDM0IsUUFBUSxpQkFBaUIsQ0FBQztBQUMxQjtFQUNFLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsVUFBVTtBQUNaO0FBQ0E7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLHNCQUFzQjtJQUN0QixjQUFjO0lBQ2QsZUFBZSxDQUFDLGdCQUFnQjtJQUNoQyxtQkFBbUI7RUFDckI7QUFFRixlQUFlLHdKQUF3SjtJQUNuSyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGdCQUFnQixDQUFDO0FBR3JCLHlDQUF5QztBQUN6QyxZQUFZLHNCQUFzQjtJQUM5QixjQUFjO0lBQ2QsVUFBVSxDQUFDLGlCQUFpQjtJQUM1QixnQkFBZ0IsQ0FBQztBQUNyQixvQkFBb0IsK0NBQStDO0lBQy9ELHlCQUF5QjtJQUN6QixVQUFVO0lBQ1YsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsZUFBZSxDQUFDO0FBRXBCLGlCQUFpQixjQUFjO0FBQy9CLGdCQUFnQjtBQUNoQixlQUFlO0FBQ2Ysa0JBQWtCO0FBQ2xCLGlCQUFpQixDQUFDO0FBRWxCLG1CQUFtQixVQUFVO0FBQzdCLFlBQVk7QUFDWixjQUFjLENBQUM7QUFFZixpQkFBaUIsa0JBQWtCO0FBQ25DLHNCQUFzQjtBQUN0QixjQUFjO0FBQ2QsVUFBVSxLQUFLLGVBQWU7QUFDOUIsaUJBQWlCLENBQUM7QUFFbEIsa0JBQWtCLHNCQUFzQjtJQUNwQyxjQUFjO0lBQ2QsVUFBVTtJQUNWLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQixDQUFDO0FBQ3JCLGtCQUFrQixxQkFBcUI7SUFDbkMsWUFBWTtJQUNaLGVBQWUsQ0FBQyxXQUFXO0lBQzNCLFVBQVU7SUFDVixrQkFBa0IsQ0FBQztBQUV2QixRQUFRLHNCQUFzQjtJQUMxQixjQUFjO0lBQ2QsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsZUFBZSxDQUFDO0FBRXBCLHlDQUF5QztBQUN6Qyx1QkFBdUIsWUFBWTtJQUMvQixZQUFZO0lBQ1osY0FBYztJQUNkLG9CQUFvQixDQUFDO0FBQ3pCLDBCQUEwQixzQkFBc0I7SUFDNUMsY0FBYztJQUNkLFVBQVU7SUFDVixlQUFlO0lBQ2Ysa0JBQWtCLENBQUM7QUFDdkIscUJBQXFCLHNCQUFzQjtJQUN2QyxjQUFjO0lBQ2QsVUFBVTtJQUNWLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixZQUFZLENBQUM7QUFFakI7SUFDSSx3SkFBd0o7SUFDeEosVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtFQUNFLHVEQUF1RDtJQUNyRCx3QkFBd0I7SUFDeEIscUJBQXFCO0VBQ3ZCO0FBR0Y7SUFDSTtNQUNFLDRCQUE0QjtJQUM5QjtJQUNBLGlCQUFpQixVQUFVLENBQUM7SUFDNUI7TUFDRSxpQkFBaUI7TUFDakIsa0JBQWtCO0lBQ3BCOztJQUVBO01BQ0UsZUFBZTtNQUNmLGtCQUFrQjtNQUNsQixhQUFhO01BQ2IsbUJBQW1CO0lBQ3JCOztJQUVBO01BQ0UsZUFBZTtJQUNqQjs7SUFFQTtNQUNFLGlCQUFpQjtNQUNqQixXQUFXO01BQ1gsWUFBWTtNQUNaLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsV0FBVztJQUNiOztJQUVBO01BQ0UsZ0JBQWdCO01BQ2hCLDBCQUEwQjtJQUM1Qjs7SUFFQTtNQUNFLGlCQUFpQjtNQUNqQixpQkFBaUI7TUFDakIsZ0JBQWdCO0lBQ2xCOztJQUVBO01BQ0UsZUFBZTtJQUNqQjs7SUFFQTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxlQUFlO01BQ2YsVUFBVTtJQUNaOztJQUVBO01BQ0UsbUJBQW1CO0lBQ3JCO0VBQ0YiLCJmaWxlIjoic3JjL2FwcC9zZW1pbmFyLWl2L3NlbWluYXItaXYuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwZXIge2ZvbnQtZmFtaWx5OiAnUm9ib3RvJywgc2Fucy1zZXJpZjsgfVxuLnNlbmltYXJTcGFjaW5nIHtwYWRkaW5nOiA0MHB4IDcwcHg7fVxuLnByb2YtZGl2IGltZyB7d2lkdGg6IDEwMCU7fVxuLmludHJvIHtwYWRkaW5nLXRvcDogNDBweDt9XG4uaW50cm8gaDIgeyAgICBcbiAgbGV0dGVyLXNwYWNpbmc6IDEuNzJweDtcbiAgY29sb3I6ICNGRTQ2NDQ7XG4gIG9wYWNpdHk6IDE7XG59XG4uaW50cm8gaDEgeyAgICBcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGZvbnQ6IDI3OHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAxLjYzcHg7XG4gICAgY29sb3I6ICMwNzA3MDc7XG4gICAgZm9udC1zaXplOiA0MnB4O21hcmdpbi10b3A6IDIwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcbiAgfVxuXG4uaW50cm8gYnV0dG9uIHtiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICNGRTNCNjEgMCUsICNGRTQ2NEQgMTklLCAjRkYzQjVFIDQwJSwgI0ZGNEMzNiA2MCUsICNGRjVEMUMgODElLCAjRkU2RDBDIDEwMCUpIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbiAgICBib3JkZXItcmFkaXVzOiA1NHB4O1xuICAgIHBhZGRpbmc6IDEwcHggMzBweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7fVxuXG5cbi8qLm1pZGRsZS13cmFwZXIge21hcmdpbi10b3A6IDUwcHg7fSAgICAqL1xuLmNvbnRlbnQgcCB7bGV0dGVyLXNwYWNpbmc6IDEuMjVweDtcbiAgICBjb2xvcjogIzA0MDQwNDtcbiAgICBvcGFjaXR5OiAxO2xpbmUtaGVpZ2h0OiAyOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7fSAgXG4ud2F0Y2gtYXJlYSBidXR0b24ge2JhY2tncm91bmQ6ICNEMkFCMkEgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM3MDcwNzA7XG4gICAgb3BhY2l0eTogMTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiA4cHggMHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMjJweDt9ICAgIFxuXG4ubmV4dC1zZW1pbmFyIHB7IGNvbG9yOiAjRkUzQjYxO1xuZm9udC13ZWlnaHQ6IDUwMDtcbmZvbnQtc2l6ZTogMjJweDtcbnRleHQtYWxpZ246IGNlbnRlcjtcbnBhZGRpbmctdG9wOiAyMHB4O31cblxuLm5leHQtc2VtaW5hciBpbWcge3dpZHRoOiA2MCU7XG5tYXJnaW46IGF1dG87XG5kaXNwbGF5OiBibG9jazt9XG5cbi5uZXh0LXNlbWluYXIgaDN7dGV4dC1hbGlnbjogY2VudGVyO1xubGV0dGVyLXNwYWNpbmc6IDEuMjdweDtcbmNvbG9yOiAjMEEwQTBBO1xub3BhY2l0eTogMTsgICAgZm9udC1zaXplOiAxNnB4O1xucGFkZGluZy10b3A6IDE0cHg7fSAgICBcblxuLm5leHQtc2VtaW5hciBoNCB7bGV0dGVyLXNwYWNpbmc6IDIuMjdweDtcbiAgICBjb2xvcjogI0ZGNEMzNjtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO31cbi5uZXh0LXNlbWluYXIgaHIge2JhY2tncm91bmQ6ICMwMDAwMDAyZTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgbWFyZ2luLXRvcDogNXB4O2hlaWdodDogMnB4O1xuICAgIHdpZHRoOiA2MCU7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O31cblxuLnRpdGxlIHtsZXR0ZXItc3BhY2luZzogMi42M3B4O1xuICAgIGNvbG9yOiAjRkUzQjYxO1xuICAgIG9wYWNpdHk6IDE7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMzZweDt9XG5cbi8qLmNoYWlycGVyc29uLW1haW4ge3BhZGRpbmctdG9wOiA2MHB4O30qL1xuLmNoYWlycGVyc29uLWluZm8gaW1nIHt3aWR0aDogMTIwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O31cbi5jaGFpcnBlcnNvbi1pbmZvIGgyIHsgICAgbGV0dGVyLXNwYWNpbmc6IDIuNjNweDtcbiAgICBjb2xvcjogIzA3MDcwNztcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7fVxuLmNoYWlycGVyc29uLWluZm8gcCB7bGV0dGVyLXNwYWNpbmc6IDEuNTJweDtcbiAgICBjb2xvcjogIzM5MzczNztcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogYXV0bzt9ICAgIFxuXG4uZm9vdGVyIHsgICAgXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgbGluZWFyLWdyYWRpZW50KDkwZGVnLCAjRkUzQjYxIDAlLCAjRkU0NjREIDE5JSwgI0ZGM0I1RSA0MCUsICNGRjRDMzYgNjAlLCAjRkY1RDFDIDgxJSwgI0ZFNkQwQyAxMDAlKSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgb3BhY2l0eTogMTtcbiAgICBoZWlnaHQ6IDYwcHg7XG59IFxuLnNlbWluYXJCYWNrZ3J1bmQge1xuICBiYWNrZ3JvdW5kLWltYWdlOnVybCgvYXNzZXRzL2ltYWdlcy9jb250ZW50U2VjdGlvbi5wbmcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gIH0gIFxuXG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KSB7XG4gICAgLndyYXBlciB7XG4gICAgICBwYWRkaW5nOiAxMHB4IDEwcHggMTBweCAxMHB4O1xuICAgIH1cbiAgICAuc2VuaW1hclNwYWNpbmcge3BhZGRpbmc6IDA7fVxuICAgIC5pbnRybyB7XG4gICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIFxuICAgIC5pbnRybyBoMSB7XG4gICAgICBmb250LXNpemU6IDM0cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICB9XG4gIFxuICAgIC5pbnRybyBoMSB7XG4gICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgfVxuICBcbiAgICAuaW50cm8gYnV0dG9uIHtcbiAgICAgIHBhZGRpbmc6IDVweCAyN3B4O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICBib3JkZXI6IG5vbmU7XG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICBcbiAgICAubWlkZGxlLXdyYXBlciB7XG4gICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24tbWFpbiB7XG4gICAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgICAgIGJhY2tncm91bmQ6IGJlaWdlO1xuICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICB9XG4gIFxuICAgIC50aXRsZSB7XG4gICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyBoMiB7XG4gICAgICBmb250LXNpemU6IDIycHg7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyBwIHtcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgIHdpZHRoOiA5MCU7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgIH1cbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/seminar-iv/seminar-iv.component.ts":
/*!****************************************************!*\
  !*** ./src/app/seminar-iv/seminar-iv.component.ts ***!
  \****************************************************/
/*! exports provided: SeminarIVComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeminarIVComponent", function() { return SeminarIVComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SeminarIVComponent = class SeminarIVComponent {
    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
    }
};
SeminarIVComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-seminar-iv',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./seminar-iv.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-iv/seminar-iv.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./seminar-iv.component.css */ "./src/app/seminar-iv/seminar-iv.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SeminarIVComponent);



/***/ }),

/***/ "./src/app/seminar-v/seminar-v.component.css":
/*!***************************************************!*\
  !*** ./src/app/seminar-v/seminar-v.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif; }\n.senimarSpacing {padding: 40px 70px;}\n.prof-div img {width: 100%;}\n.intro {padding-top: 40px;}\n.intro h2 {    \n  letter-spacing: 1.72px;\n  color: #FE4644;\n  opacity: 1;\n}\n.intro h1 {    \n    text-align: left;\n    font: 278px;\n    letter-spacing: 1.63px;\n    color: #070707;\n    font-size: 42px;margin-top: 20px;\n    margin-bottom: 50px;\n  }\n.intro button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    border-radius: 54px;\n    padding: 10px 30px;\n    color: #fff;\n    border: none;\n    font-size: 24px;\n    font-weight: 500;}\n/*.middle-wraper {margin-top: 50px;}    */\n.content p {letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500;}\n.watch-area button {background: #D2AB2A 0% 0% no-repeat padding-box;\n    border: 1px solid #707070;\n    opacity: 1;\n    width: 100%;\n    padding: 8px 0px;\n    color: #fff;\n    font-size: 22px;}\n.next-seminar p{ color: #FE3B61;\nfont-weight: 500;\nfont-size: 22px;\ntext-align: center;\npadding-top: 20px;}\n.next-seminar img {width: 60%;\nmargin: auto;\ndisplay: block;}\n.next-seminar h3{text-align: center;\nletter-spacing: 1.27px;\ncolor: #0A0A0A;\nopacity: 1;    font-size: 16px;\npadding-top: 14px;}\n.next-seminar h4 {letter-spacing: 2.27px;\n    color: #FF4C36;\n    opacity: 1;\n    font-size: 16px;\n    text-align: center;\n    font-weight: 600;\n    margin-bottom: 0;}\n.next-seminar hr {background: #0000002e;\n    border: none;\n    margin-top: 5px;height: 2px;\n    width: 60%;\n    margin-bottom: 5px;}\n.title {letter-spacing: 2.63px;\n    color: #FE3B61;\n    opacity: 1;\n    text-align: center;\n    width: 100%;\n    font-size: 36px;}\n/*.chairperson-main {padding-top: 60px;}*/\n.chairperson-info img {width: 120px;\n    margin: auto;\n    display: block;\n    padding-bottom: 10px;}\n.chairperson-info h2 {    letter-spacing: 2.63px;\n    color: #070707;\n    opacity: 1;\n    font-size: 28px;\n    text-align: center;}\n.chairperson-info p {letter-spacing: 1.52px;\n    color: #393737;\n    opacity: 1;\n    font-weight: 600;\n    font-size: 18px;\n    width: 90%;\n    text-align: center;\n    margin: auto;}\n.footer {    \n    background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    opacity: 1;\n    height: 60px;\n}\n.seminarBackgrund {\n  background-image:url(/assets/images/contentSection.png);\n    background-size: contain;\n    padding: 0 !important;\n  }\n@media screen and (max-width: 500px) {\n    .wraper {\n      padding: 10px 10px 10px 10px;\n    }\n  .senimarSpacing {padding: 0;}\n    .intro {\n      padding-top: 10px;\n      text-align: center;\n    }\n  \n    .intro h1 {\n      font-size: 34px;\n      text-align: center;\n      margin-top: 0;\n      margin-bottom: 20px;\n    }\n  \n    .intro h1 {\n      font-size: 24px;\n    }\n  \n    .intro button {\n      padding: 5px 27px;\n      color: #fff;\n      border: none;\n      font-size: 20px;\n      font-weight: 500;\n      width: 100%;\n    }\n  \n    .middle-wraper {\n      margin-top: 20px;\n      text-align: -webkit-center;\n    }\n  \n    .chairperson-main {\n      padding-top: 20px;\n      background: beige;\n      margin-top: 20px;\n    }\n  \n    .title {\n      font-size: 26px;\n    }\n  \n    .chairperson-info h2 {\n      font-size: 22px;\n    }\n  \n    .chairperson-info p {\n      font-size: 15px;\n      width: 90%;\n    }\n  \n    .chairperson-info {\n      margin-bottom: 20px;\n    }\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VtaW5hci12L3NlbWluYXItdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFNBQVMsaUNBQWlDLEVBQUU7QUFDNUMsaUJBQWlCLGtCQUFrQixDQUFDO0FBQ3BDLGVBQWUsV0FBVyxDQUFDO0FBQzNCLFFBQVEsaUJBQWlCLENBQUM7QUFDMUI7RUFDRSxzQkFBc0I7RUFDdEIsY0FBYztFQUNkLFVBQVU7QUFDWjtBQUNBO0lBQ0ksZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxzQkFBc0I7SUFDdEIsY0FBYztJQUNkLGVBQWUsQ0FBQyxnQkFBZ0I7SUFDaEMsbUJBQW1CO0VBQ3JCO0FBRUYsZUFBZSx3SkFBd0o7SUFDbkssbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGVBQWU7SUFDZixnQkFBZ0IsQ0FBQztBQUdyQix5Q0FBeUM7QUFDekMsWUFBWSxzQkFBc0I7SUFDOUIsY0FBYztJQUNkLFVBQVUsQ0FBQyxpQkFBaUI7SUFDNUIsZ0JBQWdCLENBQUM7QUFDckIsb0JBQW9CLCtDQUErQztJQUMvRCx5QkFBeUI7SUFDekIsVUFBVTtJQUNWLFdBQVc7SUFDWCxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGVBQWUsQ0FBQztBQUVwQixpQkFBaUIsY0FBYztBQUMvQixnQkFBZ0I7QUFDaEIsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQixpQkFBaUIsQ0FBQztBQUVsQixtQkFBbUIsVUFBVTtBQUM3QixZQUFZO0FBQ1osY0FBYyxDQUFDO0FBRWYsaUJBQWlCLGtCQUFrQjtBQUNuQyxzQkFBc0I7QUFDdEIsY0FBYztBQUNkLFVBQVUsS0FBSyxlQUFlO0FBQzlCLGlCQUFpQixDQUFDO0FBRWxCLGtCQUFrQixzQkFBc0I7SUFDcEMsY0FBYztJQUNkLFVBQVU7SUFDVixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixnQkFBZ0IsQ0FBQztBQUNyQixrQkFBa0IscUJBQXFCO0lBQ25DLFlBQVk7SUFDWixlQUFlLENBQUMsV0FBVztJQUMzQixVQUFVO0lBQ1Ysa0JBQWtCLENBQUM7QUFFdkIsUUFBUSxzQkFBc0I7SUFDMUIsY0FBYztJQUNkLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGVBQWUsQ0FBQztBQUVwQix5Q0FBeUM7QUFDekMsdUJBQXVCLFlBQVk7SUFDL0IsWUFBWTtJQUNaLGNBQWM7SUFDZCxvQkFBb0IsQ0FBQztBQUN6QiwwQkFBMEIsc0JBQXNCO0lBQzVDLGNBQWM7SUFDZCxVQUFVO0lBQ1YsZUFBZTtJQUNmLGtCQUFrQixDQUFDO0FBQ3ZCLHFCQUFxQixzQkFBc0I7SUFDdkMsY0FBYztJQUNkLFVBQVU7SUFDVixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsWUFBWSxDQUFDO0FBRWpCO0lBQ0ksd0pBQXdKO0lBQ3hKLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7RUFDRSx1REFBdUQ7SUFDckQsd0JBQXdCO0lBQ3hCLHFCQUFxQjtFQUN2QjtBQUdGO0lBQ0k7TUFDRSw0QkFBNEI7SUFDOUI7RUFDRixpQkFBaUIsVUFBVSxDQUFDO0lBQzFCO01BQ0UsaUJBQWlCO01BQ2pCLGtCQUFrQjtJQUNwQjs7SUFFQTtNQUNFLGVBQWU7TUFDZixrQkFBa0I7TUFDbEIsYUFBYTtNQUNiLG1CQUFtQjtJQUNyQjs7SUFFQTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxpQkFBaUI7TUFDakIsV0FBVztNQUNYLFlBQVk7TUFDWixlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLFdBQVc7SUFDYjs7SUFFQTtNQUNFLGdCQUFnQjtNQUNoQiwwQkFBMEI7SUFDNUI7O0lBRUE7TUFDRSxpQkFBaUI7TUFDakIsaUJBQWlCO01BQ2pCLGdCQUFnQjtJQUNsQjs7SUFFQTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxlQUFlO0lBQ2pCOztJQUVBO01BQ0UsZUFBZTtNQUNmLFVBQVU7SUFDWjs7SUFFQTtNQUNFLG1CQUFtQjtJQUNyQjtFQUNGIiwiZmlsZSI6InNyYy9hcHAvc2VtaW5hci12L3NlbWluYXItdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndyYXBlciB7Zm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmOyB9XG4uc2VuaW1hclNwYWNpbmcge3BhZGRpbmc6IDQwcHggNzBweDt9XG4ucHJvZi1kaXYgaW1nIHt3aWR0aDogMTAwJTt9XG4uaW50cm8ge3BhZGRpbmctdG9wOiA0MHB4O31cbi5pbnRybyBoMiB7ICAgIFxuICBsZXR0ZXItc3BhY2luZzogMS43MnB4O1xuICBjb2xvcjogI0ZFNDY0NDtcbiAgb3BhY2l0eTogMTtcbn1cbi5pbnRybyBoMSB7ICAgIFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgZm9udDogMjc4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDEuNjNweDtcbiAgICBjb2xvcjogIzA3MDcwNztcbiAgICBmb250LXNpemU6IDQycHg7bWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xuICB9XG5cbi5pbnRybyBidXR0b24ge2JhY2tncm91bmQ6IHRyYW5zcGFyZW50IGxpbmVhci1ncmFkaWVudCg5MGRlZywgI0ZFM0I2MSAwJSwgI0ZFNDY0RCAxOSUsICNGRjNCNUUgNDAlLCAjRkY0QzM2IDYwJSwgI0ZGNUQxQyA4MSUsICNGRTZEMEMgMTAwJSkgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIGJvcmRlci1yYWRpdXM6IDU0cHg7XG4gICAgcGFkZGluZzogMTBweCAzMHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDt9XG5cblxuLyoubWlkZGxlLXdyYXBlciB7bWFyZ2luLXRvcDogNTBweDt9ICAgICovXG4uY29udGVudCBwIHtsZXR0ZXItc3BhY2luZzogMS4yNXB4O1xuICAgIGNvbG9yOiAjMDQwNDA0O1xuICAgIG9wYWNpdHk6IDE7bGluZS1oZWlnaHQ6IDI4cHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDt9ICBcbi53YXRjaC1hcmVhIGJ1dHRvbiB7YmFja2dyb3VuZDogI0QyQUIyQSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzcwNzA3MDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDhweCAwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAyMnB4O30gICAgXG5cbi5uZXh0LXNlbWluYXIgcHsgY29sb3I6ICNGRTNCNjE7XG5mb250LXdlaWdodDogNTAwO1xuZm9udC1zaXplOiAyMnB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xucGFkZGluZy10b3A6IDIwcHg7fVxuXG4ubmV4dC1zZW1pbmFyIGltZyB7d2lkdGg6IDYwJTtcbm1hcmdpbjogYXV0bztcbmRpc3BsYXk6IGJsb2NrO31cblxuLm5leHQtc2VtaW5hciBoM3t0ZXh0LWFsaWduOiBjZW50ZXI7XG5sZXR0ZXItc3BhY2luZzogMS4yN3B4O1xuY29sb3I6ICMwQTBBMEE7XG5vcGFjaXR5OiAxOyAgICBmb250LXNpemU6IDE2cHg7XG5wYWRkaW5nLXRvcDogMTRweDt9ICAgIFxuXG4ubmV4dC1zZW1pbmFyIGg0IHtsZXR0ZXItc3BhY2luZzogMi4yN3B4O1xuICAgIGNvbG9yOiAjRkY0QzM2O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7fVxuLm5leHQtc2VtaW5hciBociB7YmFja2dyb3VuZDogIzAwMDAwMDJlO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBtYXJnaW4tdG9wOiA1cHg7aGVpZ2h0OiAycHg7XG4gICAgd2lkdGg6IDYwJTtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7fVxuXG4udGl0bGUge2xldHRlci1zcGFjaW5nOiAyLjYzcHg7XG4gICAgY29sb3I6ICNGRTNCNjE7XG4gICAgb3BhY2l0eTogMTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZm9udC1zaXplOiAzNnB4O31cblxuLyouY2hhaXJwZXJzb24tbWFpbiB7cGFkZGluZy10b3A6IDYwcHg7fSovXG4uY2hhaXJwZXJzb24taW5mbyBpbWcge3dpZHRoOiAxMjBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7fVxuLmNoYWlycGVyc29uLWluZm8gaDIgeyAgICBsZXR0ZXItc3BhY2luZzogMi42M3B4O1xuICAgIGNvbG9yOiAjMDcwNzA3O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZm9udC1zaXplOiAyOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjt9XG4uY2hhaXJwZXJzb24taW5mbyBwIHtsZXR0ZXItc3BhY2luZzogMS41MnB4O1xuICAgIGNvbG9yOiAjMzkzNzM3O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiBhdXRvO30gICAgXG5cbi5mb290ZXIgeyAgICBcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICNGRTNCNjEgMCUsICNGRTQ2NEQgMTklLCAjRkYzQjVFIDQwJSwgI0ZGNEMzNiA2MCUsICNGRjVEMUMgODElLCAjRkU2RDBDIDEwMCUpIDAlIDAlIG5vLXJlcGVhdCBwYWRkaW5nLWJveDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGhlaWdodDogNjBweDtcbn0gXG4uc2VtaW5hckJhY2tncnVuZCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6dXJsKC9hc3NldHMvaW1hZ2VzL2NvbnRlbnRTZWN0aW9uLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgfSAgXG5cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcbiAgICAud3JhcGVyIHtcbiAgICAgIHBhZGRpbmc6IDEwcHggMTBweCAxMHB4IDEwcHg7XG4gICAgfVxuICAuc2VuaW1hclNwYWNpbmcge3BhZGRpbmc6IDA7fVxuICAgIC5pbnRybyB7XG4gICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIFxuICAgIC5pbnRybyBoMSB7XG4gICAgICBmb250LXNpemU6IDM0cHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICB9XG4gIFxuICAgIC5pbnRybyBoMSB7XG4gICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgfVxuICBcbiAgICAuaW50cm8gYnV0dG9uIHtcbiAgICAgIHBhZGRpbmc6IDVweCAyN3B4O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICBib3JkZXI6IG5vbmU7XG4gICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICBcbiAgICAubWlkZGxlLXdyYXBlciB7XG4gICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24tbWFpbiB7XG4gICAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgICAgIGJhY2tncm91bmQ6IGJlaWdlO1xuICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICB9XG4gIFxuICAgIC50aXRsZSB7XG4gICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyBoMiB7XG4gICAgICBmb250LXNpemU6IDIycHg7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyBwIHtcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgIHdpZHRoOiA5MCU7XG4gICAgfVxuICBcbiAgICAuY2hhaXJwZXJzb24taW5mbyB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgIH1cbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/seminar-v/seminar-v.component.ts":
/*!**************************************************!*\
  !*** ./src/app/seminar-v/seminar-v.component.ts ***!
  \**************************************************/
/*! exports provided: SeminarVComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeminarVComponent", function() { return SeminarVComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SeminarVComponent = class SeminarVComponent {
    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
    }
};
SeminarVComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-seminar-v',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./seminar-v.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-v/seminar-v.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./seminar-v.component.css */ "./src/app/seminar-v/seminar-v.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SeminarVComponent);



/***/ }),

/***/ "./src/app/seminar-vi/seminar-vi.component.css":
/*!*****************************************************!*\
  !*** ./src/app/seminar-vi/seminar-vi.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".wraper {font-family: 'Roboto', sans-serif;}\n.prof-div img {width: 100%;}\n.senimarSpacing {padding: 40px 70px;}\n.intro {padding-top: 40px;}\n.intro h2 {    letter-spacing: 1.72px;\n    color: #FE4644;\n    opacity: 1;}\n.intro h1 {    text-align: left;\n    font: 278px;\n    letter-spacing: 1.63px;\n    color: #070707;\n    font-size: 42px;margin-top: 20px;\n    margin-bottom: 50px;}\n.intro button {background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    border-radius: 54px;\n    padding: 10px 30px;\n    color: #fff;\n    border: none;\n    font-size: 24px;\n    font-weight: 500;}\n/*.middle-wraper {    margin-top: 50px;}    */\n.content p {    letter-spacing: 1.25px;\n    color: #040404;\n    opacity: 1;line-height: 28px;\n    font-weight: 500;}\n.watch-area button {    background: #D2AB2A 0% 0% no-repeat padding-box;\n    border: 1px solid #707070;\n    opacity: 1;\n    width: 100%;\n    padding: 8px 0px;\n    color: #fff;\n    font-size: 22px;}\n.next-seminar p{ color: #FE3B61;\nfont-weight: 500;\nfont-size: 22px;\ntext-align: center;\npadding-top: 20px;}\n.next-seminar img { width: 100%;\n  margin: auto;\n  display: block;\n  margin-top: 40px;\n}\n.next-seminar h3{text-align: center;\nletter-spacing: 1.27px;\ncolor: #0A0A0A;\nopacity: 1;    font-size: 16px;\npadding-top: 14px;}\n.next-seminar h4 {letter-spacing: 2.27px;\n    color: #FF4C36;\n    opacity: 1;\n    font-size: 16px;\n    text-align: center;\n    font-weight: 600;\n    margin-bottom: 0;}\n.next-seminar hr {background: #0000002e;\n    border: none;\n    margin-top: 5px;height: 2px;\n    width: 60%;\n    margin-bottom: 5px;}\n.title {letter-spacing: 2.63px;\n    color: #FE3B61;\n    opacity: 1;\n    text-align: center;\n    width: 100%;\n    font-size: 36px;}\n/*.chairperson-main {padding-top: 60px;}*/\n.chairperson-info img {width: 120px;\n    margin: auto;\n    display: block;\n    padding-bottom: 10px;}\n.chairperson-info h2 {    letter-spacing: 2.63px;\n    color: #070707;\n    opacity: 1;\n    font-size: 28px;\n    text-align: center;}\n.chairperson-info p {letter-spacing: 1.52px;\n    color: #393737;\n    opacity: 1;\n    font-weight: 600;\n    font-size: 18px;\n    width: 90%;\n    text-align: center;\n    margin: auto;}\n.footer {    background: transparent linear-gradient(90deg, #FE3B61 0%, #FE464D 19%, #FF3B5E 40%, #FF4C36 60%, #FF5D1C 81%, #FE6D0C 100%) 0% 0% no-repeat padding-box;\n    opacity: 1;\n    height: 60px;}\n.seminarBackgrund {\n  background-image:url(/assets/images/contentSection.png);\n    background-size: contain;\n    padding: 0 !important;\n  }\n@media screen and (max-width: 500px) {\n        .wraper {\n          padding: 10px 10px 10px 10px;\n        }\n      .senimarSpacing {padding: 0;}\n        .intro {\n          padding-top: 10px;\n          text-align: center;\n        }\n      \n        .intro h1 {\n          font-size: 34px;\n          text-align: center;\n          margin-top: 0;\n          margin-bottom: 20px;\n        }\n      \n        .intro h1 {\n          font-size: 24px;\n        }\n      \n        .intro button {\n          padding: 5px 27px;\n          color: #fff;\n          border: none;\n          font-size: 20px;\n          font-weight: 500;\n          width: 100%;\n        }\n      \n        .middle-wraper {\n          margin-top: 20px;\n          text-align: -webkit-center;\n        }\n      \n        .chairperson-main {\n          padding-top: 20px;\n          background: beige;\n          margin-top: 20px;\n        }\n      \n        .title {\n          font-size: 26px;\n        }\n      \n        .chairperson-info h2 {\n          font-size: 22px;\n        }\n      \n        .chairperson-info p {\n          font-size: 15px;\n          width: 90%;\n        }\n      \n        .chairperson-info {\n          margin-bottom: 20px;\n        }\n      }    \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VtaW5hci12aS9zZW1pbmFyLXZpLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsU0FBUyxpQ0FBaUMsQ0FBQztBQUMzQyxlQUFlLFdBQVcsQ0FBQztBQUMzQixpQkFBaUIsa0JBQWtCLENBQUM7QUFFcEMsUUFBUSxpQkFBaUIsQ0FBQztBQUMxQixlQUFlLHNCQUFzQjtJQUNqQyxjQUFjO0lBQ2QsVUFBVSxDQUFDO0FBQ2YsZUFBZSxnQkFBZ0I7SUFDM0IsV0FBVztJQUNYLHNCQUFzQjtJQUN0QixjQUFjO0lBQ2QsZUFBZSxDQUFDLGdCQUFnQjtJQUNoQyxtQkFBbUIsQ0FBQztBQUV4QixlQUFlLHdKQUF3SjtJQUNuSyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGdCQUFnQixDQUFDO0FBR3JCLDZDQUE2QztBQUM3QyxnQkFBZ0Isc0JBQXNCO0lBQ2xDLGNBQWM7SUFDZCxVQUFVLENBQUMsaUJBQWlCO0lBQzVCLGdCQUFnQixDQUFDO0FBRXJCLHdCQUF3QiwrQ0FBK0M7SUFDbkUseUJBQXlCO0lBQ3pCLFVBQVU7SUFDVixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxlQUFlLENBQUM7QUFFcEIsaUJBQWlCLGNBQWM7QUFDL0IsZ0JBQWdCO0FBQ2hCLGVBQWU7QUFDZixrQkFBa0I7QUFDbEIsaUJBQWlCLENBQUM7QUFFbEIsb0JBQW9CLFdBQVc7RUFDN0IsWUFBWTtFQUNaLGNBQWM7RUFDZCxnQkFBZ0I7QUFDbEI7QUFFQSxpQkFBaUIsa0JBQWtCO0FBQ25DLHNCQUFzQjtBQUN0QixjQUFjO0FBQ2QsVUFBVSxLQUFLLGVBQWU7QUFDOUIsaUJBQWlCLENBQUM7QUFFbEIsa0JBQWtCLHNCQUFzQjtJQUNwQyxjQUFjO0lBQ2QsVUFBVTtJQUNWLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQixDQUFDO0FBQ3JCLGtCQUFrQixxQkFBcUI7SUFDbkMsWUFBWTtJQUNaLGVBQWUsQ0FBQyxXQUFXO0lBQzNCLFVBQVU7SUFDVixrQkFBa0IsQ0FBQztBQUV2QixRQUFRLHNCQUFzQjtJQUMxQixjQUFjO0lBQ2QsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsZUFBZSxDQUFDO0FBRXBCLHlDQUF5QztBQUN6Qyx1QkFBdUIsWUFBWTtJQUMvQixZQUFZO0lBQ1osY0FBYztJQUNkLG9CQUFvQixDQUFDO0FBQ3pCLDBCQUEwQixzQkFBc0I7SUFDNUMsY0FBYztJQUNkLFVBQVU7SUFDVixlQUFlO0lBQ2Ysa0JBQWtCLENBQUM7QUFDdkIscUJBQXFCLHNCQUFzQjtJQUN2QyxjQUFjO0lBQ2QsVUFBVTtJQUNWLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixZQUFZLENBQUM7QUFFakIsYUFBYSx3SkFBd0o7SUFDakssVUFBVTtJQUNWLFlBQVksQ0FBQztBQUNqQjtFQUNFLHVEQUF1RDtJQUNyRCx3QkFBd0I7SUFDeEIscUJBQXFCO0VBQ3ZCO0FBR0U7UUFDSTtVQUNFLDRCQUE0QjtRQUM5QjtNQUNGLGlCQUFpQixVQUFVLENBQUM7UUFDMUI7VUFDRSxpQkFBaUI7VUFDakIsa0JBQWtCO1FBQ3BCOztRQUVBO1VBQ0UsZUFBZTtVQUNmLGtCQUFrQjtVQUNsQixhQUFhO1VBQ2IsbUJBQW1CO1FBQ3JCOztRQUVBO1VBQ0UsZUFBZTtRQUNqQjs7UUFFQTtVQUNFLGlCQUFpQjtVQUNqQixXQUFXO1VBQ1gsWUFBWTtVQUNaLGVBQWU7VUFDZixnQkFBZ0I7VUFDaEIsV0FBVztRQUNiOztRQUVBO1VBQ0UsZ0JBQWdCO1VBQ2hCLDBCQUEwQjtRQUM1Qjs7UUFFQTtVQUNFLGlCQUFpQjtVQUNqQixpQkFBaUI7VUFDakIsZ0JBQWdCO1FBQ2xCOztRQUVBO1VBQ0UsZUFBZTtRQUNqQjs7UUFFQTtVQUNFLGVBQWU7UUFDakI7O1FBRUE7VUFDRSxlQUFlO1VBQ2YsVUFBVTtRQUNaOztRQUVBO1VBQ0UsbUJBQW1CO1FBQ3JCO01BQ0YiLCJmaWxlIjoic3JjL2FwcC9zZW1pbmFyLXZpL3NlbWluYXItdmkuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi53cmFwZXIge2ZvbnQtZmFtaWx5OiAnUm9ib3RvJywgc2Fucy1zZXJpZjt9XG4ucHJvZi1kaXYgaW1nIHt3aWR0aDogMTAwJTt9XG4uc2VuaW1hclNwYWNpbmcge3BhZGRpbmc6IDQwcHggNzBweDt9XG5cbi5pbnRybyB7cGFkZGluZy10b3A6IDQwcHg7fVxuLmludHJvIGgyIHsgICAgbGV0dGVyLXNwYWNpbmc6IDEuNzJweDtcbiAgICBjb2xvcjogI0ZFNDY0NDtcbiAgICBvcGFjaXR5OiAxO31cbi5pbnRybyBoMSB7ICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgZm9udDogMjc4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDEuNjNweDtcbiAgICBjb2xvcjogIzA3MDcwNztcbiAgICBmb250LXNpemU6IDQycHg7bWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O31cblxuLmludHJvIGJ1dHRvbiB7YmFja2dyb3VuZDogdHJhbnNwYXJlbnQgbGluZWFyLWdyYWRpZW50KDkwZGVnLCAjRkUzQjYxIDAlLCAjRkU0NjREIDE5JSwgI0ZGM0I1RSA0MCUsICNGRjRDMzYgNjAlLCAjRkY1RDFDIDgxJSwgI0ZFNkQwQyAxMDAlKSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgYm9yZGVyLXJhZGl1czogNTRweDtcbiAgICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICBmb250LXdlaWdodDogNTAwO31cblxuXG4vKi5taWRkbGUtd3JhcGVyIHsgICAgbWFyZ2luLXRvcDogNTBweDt9ICAgICovXG4uY29udGVudCBwIHsgICAgbGV0dGVyLXNwYWNpbmc6IDEuMjVweDtcbiAgICBjb2xvcjogIzA0MDQwNDtcbiAgICBvcGFjaXR5OiAxO2xpbmUtaGVpZ2h0OiAyOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7fSAgICBcblxuLndhdGNoLWFyZWEgYnV0dG9uIHsgICAgYmFja2dyb3VuZDogI0QyQUIyQSAwJSAwJSBuby1yZXBlYXQgcGFkZGluZy1ib3g7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzcwNzA3MDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDhweCAwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAyMnB4O30gICAgXG5cbi5uZXh0LXNlbWluYXIgcHsgY29sb3I6ICNGRTNCNjE7XG5mb250LXdlaWdodDogNTAwO1xuZm9udC1zaXplOiAyMnB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xucGFkZGluZy10b3A6IDIwcHg7fVxuXG4ubmV4dC1zZW1pbmFyIGltZyB7IHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IGF1dG87XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuXG4ubmV4dC1zZW1pbmFyIGgze3RleHQtYWxpZ246IGNlbnRlcjtcbmxldHRlci1zcGFjaW5nOiAxLjI3cHg7XG5jb2xvcjogIzBBMEEwQTtcbm9wYWNpdHk6IDE7ICAgIGZvbnQtc2l6ZTogMTZweDtcbnBhZGRpbmctdG9wOiAxNHB4O30gICAgXG5cbi5uZXh0LXNlbWluYXIgaDQge2xldHRlci1zcGFjaW5nOiAyLjI3cHg7XG4gICAgY29sb3I6ICNGRjRDMzY7XG4gICAgb3BhY2l0eTogMTtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDt9XG4ubmV4dC1zZW1pbmFyIGhyIHtiYWNrZ3JvdW5kOiAjMDAwMDAwMmU7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIG1hcmdpbi10b3A6IDVweDtoZWlnaHQ6IDJweDtcbiAgICB3aWR0aDogNjAlO1xuICAgIG1hcmdpbi1ib3R0b206IDVweDt9XG5cbi50aXRsZSB7bGV0dGVyLXNwYWNpbmc6IDIuNjNweDtcbiAgICBjb2xvcjogI0ZFM0I2MTtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDM2cHg7fVxuXG4vKi5jaGFpcnBlcnNvbi1tYWluIHtwYWRkaW5nLXRvcDogNjBweDt9Ki9cbi5jaGFpcnBlcnNvbi1pbmZvIGltZyB7d2lkdGg6IDEyMHB4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDt9XG4uY2hhaXJwZXJzb24taW5mbyBoMiB7ICAgIGxldHRlci1zcGFjaW5nOiAyLjYzcHg7XG4gICAgY29sb3I6ICMwNzA3MDc7XG4gICAgb3BhY2l0eTogMTtcbiAgICBmb250LXNpemU6IDI4cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO31cbi5jaGFpcnBlcnNvbi1pbmZvIHAge2xldHRlci1zcGFjaW5nOiAxLjUycHg7XG4gICAgY29sb3I6ICMzOTM3Mzc7XG4gICAgb3BhY2l0eTogMTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB3aWR0aDogOTAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IGF1dG87fSAgICBcblxuLmZvb3RlciB7ICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50IGxpbmVhci1ncmFkaWVudCg5MGRlZywgI0ZFM0I2MSAwJSwgI0ZFNDY0RCAxOSUsICNGRjNCNUUgNDAlLCAjRkY0QzM2IDYwJSwgI0ZGNUQxQyA4MSUsICNGRTZEMEMgMTAwJSkgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgaGVpZ2h0OiA2MHB4O30gXG4uc2VtaW5hckJhY2tncnVuZCB7XG4gIGJhY2tncm91bmQtaW1hZ2U6dXJsKC9hc3NldHMvaW1hZ2VzL2NvbnRlbnRTZWN0aW9uLnBuZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgfSAgICBcblxuXG4gICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTAwcHgpIHtcbiAgICAgICAgLndyYXBlciB7XG4gICAgICAgICAgcGFkZGluZzogMTBweCAxMHB4IDEwcHggMTBweDtcbiAgICAgICAgfVxuICAgICAgLnNlbmltYXJTcGFjaW5nIHtwYWRkaW5nOiAwO31cbiAgICAgICAgLmludHJvIHtcbiAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgIFxuICAgICAgICAuaW50cm8gaDEge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMzRweDtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICB9XG4gICAgICBcbiAgICAgICAgLmludHJvIGgxIHtcbiAgICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIH1cbiAgICAgIFxuICAgICAgICAuaW50cm8gYnV0dG9uIHtcbiAgICAgICAgICBwYWRkaW5nOiA1cHggMjdweDtcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIH1cbiAgICAgIFxuICAgICAgICAubWlkZGxlLXdyYXBlciB7XG4gICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAgICAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICAgICAgfVxuICAgICAgXG4gICAgICAgIC5jaGFpcnBlcnNvbi1tYWluIHtcbiAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBiZWlnZTtcbiAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICB9XG4gICAgICBcbiAgICAgICAgLnRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICAgIH1cbiAgICAgIFxuICAgICAgICAuY2hhaXJwZXJzb24taW5mbyBoMiB7XG4gICAgICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgICAgICB9XG4gICAgICBcbiAgICAgICAgLmNoYWlycGVyc29uLWluZm8gcCB7XG4gICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICAgIHdpZHRoOiA5MCU7XG4gICAgICAgIH1cbiAgICAgIFxuICAgICAgICAuY2hhaXJwZXJzb24taW5mbyB7XG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgICAgfVxuICAgICAgfSAgICAiXX0= */");

/***/ }),

/***/ "./src/app/seminar-vi/seminar-vi.component.ts":
/*!****************************************************!*\
  !*** ./src/app/seminar-vi/seminar-vi.component.ts ***!
  \****************************************************/
/*! exports provided: SeminarVIComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SeminarVIComponent", function() { return SeminarVIComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SeminarVIComponent = class SeminarVIComponent {
    constructor() { }
    ngOnInit() {
        window.scrollTo(0, 0);
    }
};
SeminarVIComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-seminar-vi',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./seminar-vi.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/seminar-vi/seminar-vi.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./seminar-vi.component.css */ "./src/app/seminar-vi/seminar-vi.component.css")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], SeminarVIComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /usr/share/nginx/html/vrushali-LS/london-seminar/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map